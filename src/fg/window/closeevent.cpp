﻿#include "fg/core/state/event.h"
#include "fg/core/state/eventbackground.h"
#include "fg/core/state/eventmanager.h"
#include "fg/core/state/eventregistermanager.h"

#include "fg/util/exportdummy.h"
#include "fg/window/closeevent.h"
