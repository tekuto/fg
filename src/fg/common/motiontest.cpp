﻿#include "fg/util/test.h"
#include "fg/common/motion.h"

#include <vector>
#include <chrono>

struct SteadyClockDurationToFloat
{
    auto operator()(
        const std::chrono::steady_clock::duration & _DURATION
    ) const
    {
        return float( _DURATION.count() );
    }
};

struct MotionTest
{
    const int   DURATION;
    const int   BEGIN_TIME;
    const int   END_TIME;

    const int   CURRENT_TIME;

    const int   BEGIN_VALUE;
    const int   END_VALUE;
    const int   DIFF_VALUE;
};

TEST(
    MotionTest
    , Constructor
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    ASSERT_EQ( 30, MOTION.DURATION );
    ASSERT_EQ( 0, MOTION.BEGIN_TIME );
    ASSERT_EQ( 30, MOTION.END_TIME );
    ASSERT_EQ( 0, MOTION.CURRENT_TIME );
    ASSERT_EQ( 10, MOTION.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION.END_VALUE );
    ASSERT_EQ( 10, MOTION.DIFF_VALUE );
}

TEST(
    MotionTest
    , Constructor_otherTimePointType
)
{
    // コンパイルエラーが発生しないことをチェックする
    fg::Motion< std::chrono::steady_clock::duration, int, SteadyClockDurationToFloat >(
        10
        , 20
        , std::chrono::seconds( 30 )
    );
}

TEST(
    MotionTest
    , Constructor_withBeginTime
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
        , 40
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    ASSERT_EQ( 30, MOTION.DURATION );
    ASSERT_EQ( 40, MOTION.BEGIN_TIME );
    ASSERT_EQ( 70, MOTION.END_TIME );
    ASSERT_EQ( 0, MOTION.CURRENT_TIME );
    ASSERT_EQ( 10, MOTION.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION.END_VALUE );
    ASSERT_EQ( 10, MOTION.DIFF_VALUE );
}

TEST(
    MotionTest
    , Constructor_failed
)
{
    try {
        fg::Motion< int, int >(
            10
            , 20
            , -30
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::Motion< int, int >(
            10
            , 20
            , -30
            , 40
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }
}

TEST(
    MotionTest
    , Initialize
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 100
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    motion.update( 50 );

    motion.initialize();

    ASSERT_EQ( 0, MOTION.CURRENT_TIME );
}

TEST(
    MotionTest
    , GetBeginTime
)
{
    const auto  MOTION = fg::Motion< int, int >(
        10
        , 20
        , 30
        , 40
    );

    ASSERT_EQ( 40, MOTION.getBeginTime() );
}

TEST(
    MotionTest
    , GetDuration
)
{
    const auto  MOTION = fg::Motion< int, int >(
        10
        , 20
        , 30
        , 40
    );

    ASSERT_EQ( 30, MOTION.getDuration() );
}

TEST(
    MotionTest
    , GetCurrentTime
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 100
    );

    motion.update( 50 );

    ASSERT_EQ( 50, motion.getCurrentTime() );
}

TEST(
    MotionTest
    , SetCurrentTime
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 100
    );

    motion.setCurrentTime( 50 );

    ASSERT_EQ( 50, motion.getCurrentTime() );
}

TEST(
    MotionTest
    , Update
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 100
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    motion.update( 50 );

    ASSERT_EQ( 50, MOTION.CURRENT_TIME );

    motion.update( 60 );

    ASSERT_EQ( 110, MOTION.CURRENT_TIME );

    motion.update( 70 );

    ASSERT_EQ( 110, MOTION.CURRENT_TIME );
}

TEST(
    MotionTest
    , Update_forIntegerTimePoint
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 100
    );

    motion.update( 50 );

    ASSERT_EQ( 15, motion.calc() );
}

TEST(
    MotionTest
    , GetBeginValue
)
{
    const auto    MOTION = fg::Motion< int, int >(
        10
        , 20
        , 30
        , 40
    );

    ASSERT_EQ( 10, MOTION.getBeginValue() );
}

TEST(
    MotionTest
    , GetEndValue
)
{
    const auto    MOTION = fg::Motion< int, int >(
        10
        , 20
        , 30
        , 40
    );

    ASSERT_EQ( 20, MOTION.getEndValue() );
}

TEST(
    MotionTest
    , Calc
)
{
    auto    motion = fg::Motion< float, int >(
        10
        , 20
        , 100.0f
        , 50.0f
    );

    ASSERT_EQ( 10, motion.calc() );

    motion.update( 100 );

    ASSERT_EQ( 15, motion.calc() );

    motion.update( 100 );

    ASSERT_EQ( 20, motion.calc() );
}

TEST(
    MotionTest
    , Calc_durationZero
)
{
    auto    motion = fg::Motion< float, int >(
        10
        , 20
        , 0.0f
        , 30.0f
    );

    motion.update( 30 );

    ASSERT_EQ( 20, motion.calc() );
}

struct MotionLoopTest
{
    const int   DURATION;
    const int   INITIALIZE_TIME;

    const int   CURRENT_TIME;

    const int   BEGIN_VALUE;
    const int   END_VALUE;
    const int   DIFF_VALUE;
};

TEST(
    MotionLoopTest
    , Constructor
)
{
    auto    motion = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
    );
    const auto &    MOTION = reinterpret_cast< const MotionLoopTest & >( motion );

    ASSERT_EQ( 30, MOTION.DURATION );
    ASSERT_EQ( 0, MOTION.INITIALIZE_TIME );
    ASSERT_EQ( 0, MOTION.CURRENT_TIME );
    ASSERT_EQ( 10, MOTION.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION.END_VALUE );
    ASSERT_EQ( 10, MOTION.DIFF_VALUE );
}

TEST(
    MotionLoopTest
    , Constructor_otherTimePointType
)
{
    // コンパイルエラーが発生しないことをチェックする
    fg::MotionLoop< std::chrono::steady_clock::duration, int, SteadyClockDurationToFloat >(
        10
        , 20
        , std::chrono::seconds( 30 )
    );
}

TEST(
    MotionLoopTest
    , Constructor_withInitializeTime
)
{
    auto    motion = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
        , 40
    );
    const auto &    MOTION = reinterpret_cast< const MotionLoopTest & >( motion );

    ASSERT_EQ( 30, MOTION.DURATION );
    ASSERT_EQ( 40, MOTION.INITIALIZE_TIME );
    ASSERT_EQ( 40, MOTION.CURRENT_TIME );
    ASSERT_EQ( 10, MOTION.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION.END_VALUE );
    ASSERT_EQ( 10, MOTION.DIFF_VALUE );
}

TEST(
    MotionLoopTest
    , Constructor_failed
)
{
    try {
        fg::MotionLoop< int, int >(
            10
            , 20
            , 0
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::MotionLoop< int, int >(
            10
            , 20
            , -30
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::MotionLoop< int, int >(
            10
            , 20
            , 0
            , 30
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::MotionLoop< int, int >(
            10
            , 20
            , -30
            , 40
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }
}

TEST(
    MotionLoopTest
    , Initialize
)
{
    auto    motion = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
        , 25
    );
    const auto &    MOTION = reinterpret_cast< const MotionLoopTest & >( motion );

    motion.update( 15 );

    motion.initialize();

    ASSERT_EQ( 25, MOTION.CURRENT_TIME );
}

TEST(
    MotionLoopTest
    , GetDuration
)
{
    const auto  MOTION = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
    );

    ASSERT_EQ( 30, MOTION.getDuration() );
}

TEST(
    MotionLoopTest
    , GetCurrentTime
)
{
    const auto  MOTION = fg::MotionLoop< int, int >(
        10
        , 20
        , 100
        , 50
    );

    ASSERT_EQ( 50, MOTION.getCurrentTime() );
}

TEST(
    MotionLoopTest
    , SetCurrentTime
)
{
    auto    motion = fg::MotionLoop< int, int >(
        10
        , 20
        , 100
    );

    motion.setCurrentTime( 50 );

    ASSERT_EQ( 50, motion.getCurrentTime() );
}

TEST(
    MotionLoopTest
    , Update
)
{
    auto    motion = fg::MotionLoop< int, int >(
        10
        , 20
        , 100
    );
    const auto &    MOTION = reinterpret_cast< const MotionLoopTest & >( motion );

    motion.update( 50 );

    ASSERT_EQ( 50, MOTION.CURRENT_TIME );

    motion.update( 60 );

    ASSERT_EQ( 10, MOTION.CURRENT_TIME );

    motion.update( 250 );

    ASSERT_EQ( 60, MOTION.CURRENT_TIME );
}

TEST(
    MotionLoopTest
    , Update_forIntegerTimePoint
)
{
    auto    motion = fg::MotionLoop< int, int >(
        10
        , 20
        , 100
    );

    motion.update( 50 );

    ASSERT_EQ( 15, motion.calc() );
}

TEST(
    MotionLoopTest
    , GetBeginValue
)
{
    const auto  MOTION = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
    );

    ASSERT_EQ( 10, MOTION.getBeginValue() );
}

TEST(
    MotionLoopTest
    , GetEndValue
)
{
    const auto  MOTION = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
    );

    ASSERT_EQ( 20, MOTION.getEndValue() );
}

TEST(
    MotionLoopTest
    , Calc
)
{
    auto    motion = fg::MotionLoop< float, int >(
        0
        , 100
        , 100.0f
    );

    ASSERT_EQ( 0, motion.calc() );

    motion.update( 50 );

    ASSERT_EQ( 50, motion.calc() );

    motion.update( 50 );

    ASSERT_EQ( 0, motion.calc() );
}

struct MotionRoundTripTest
{
    const int   DURATION;
    const int   INITIALIZE_TIME;
    const bool  INITIALIZE_DIRECTION;

    const int   CURRENT_TIME;
    const bool  DIRECTION;

    const int   BEGIN_VALUE;
    const int   END_VALUE;
    const int   DIFF_VALUE;
};

TEST(
    MotionRoundTripTest
    , Constructor
)
{
    auto    motion = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
    );
    const auto &    MOTION = reinterpret_cast< const MotionRoundTripTest & >( motion );

    ASSERT_EQ( 30, MOTION.DURATION );
    ASSERT_EQ( 0, MOTION.INITIALIZE_TIME );
    ASSERT_TRUE( MOTION.INITIALIZE_DIRECTION );
    ASSERT_EQ( 0, MOTION.CURRENT_TIME );
    ASSERT_TRUE( MOTION.DIRECTION );
    ASSERT_EQ( 10, MOTION.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION.END_VALUE );
    ASSERT_EQ( 10, MOTION.DIFF_VALUE );
}

TEST(
    MotionRoundTripTest
    , Constructor_otherTimePointType
)
{
    // コンパイルエラーが発生しないことをチェックする
    fg::MotionRoundTrip< std::chrono::steady_clock::duration, int, SteadyClockDurationToFloat >(
        10
        , 20
        , std::chrono::seconds( 30 )
    );
}

TEST(
    MotionRoundTripTest
    , Constructor_withInitializeTime
)
{
    auto    motion1 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
        , 25
        , true
    );
    const auto &    MOTION1 = reinterpret_cast< const MotionRoundTripTest & >( motion1 );

    ASSERT_EQ( 30, MOTION1.DURATION );
    ASSERT_EQ( 25, MOTION1.INITIALIZE_TIME );
    ASSERT_TRUE( MOTION1.INITIALIZE_DIRECTION );
    ASSERT_EQ( 25, MOTION1.CURRENT_TIME );
    ASSERT_TRUE( MOTION1.DIRECTION );
    ASSERT_EQ( 10, MOTION1.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION1.END_VALUE );
    ASSERT_EQ( 10, MOTION1.DIFF_VALUE );

    auto    motion2 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
        , 25
        , false
    );
    const auto &    MOTION2 = reinterpret_cast< const MotionRoundTripTest & >( motion2 );

    ASSERT_EQ( 30, MOTION2.DURATION );
    ASSERT_EQ( 5, MOTION2.INITIALIZE_TIME );
    ASSERT_FALSE( MOTION2.INITIALIZE_DIRECTION );
    ASSERT_EQ( 5, MOTION2.CURRENT_TIME );
    ASSERT_FALSE( MOTION2.DIRECTION );
    ASSERT_EQ( 10, MOTION2.BEGIN_VALUE );
    ASSERT_EQ( 20, MOTION2.END_VALUE );
    ASSERT_EQ( 10, MOTION2.DIFF_VALUE );

    auto    motion3 = fg::MotionRoundTrip< float, int >(
        0
        , 100
        , 100.0f
        , 75
        , false
    );

    ASSERT_EQ( 75, motion3.calc() );

    motion3.update( 50 );

    ASSERT_EQ( 25, motion3.calc() );
}

TEST(
    MotionRoundTripTest
    , Constructor_failed
)
{
    try {
        fg::MotionRoundTrip< int, int >(
            10
            , 20
            , 0
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::MotionRoundTrip< int, int >(
            10
            , 20
            , -30
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::MotionRoundTrip< int, int >(
            10
            , 20
            , 0
            , 30
            , true
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }

    try {
        fg::MotionRoundTrip< int, int >(
            10
            , 20
            , -30
            , 40
            , true
        );
        ASSERT_TRUE( false );   // ここに到達したら不正
    } catch( const std::runtime_error & ) {
    } catch( ... ) {
        ASSERT_TRUE( false );   // ここに到達したら不正
    }
}

TEST(
    MotionRoundTripTest
    , Initialize
)
{
    auto    motion = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
        , 25
        , false
    );
    const auto &    MOTION = reinterpret_cast< const MotionRoundTripTest & >( motion );

    motion.update( 15 );

    motion.initialize();

    ASSERT_EQ( 5, MOTION.CURRENT_TIME );
    ASSERT_FALSE( MOTION.DIRECTION );
}

TEST(
    MotionRoundTripTest
    , GetDuration
)
{
    const auto  MOTION = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
    );

    ASSERT_EQ( 30, MOTION.getDuration() );
}

TEST(
    MotionRoundTripTest
    , GetCurrentTime
)
{
    const auto  MOTION1 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
        , 25
        , true
    );

    ASSERT_EQ( 25, MOTION1.getCurrentTime() );

    const auto  MOTION2 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
        , 25
        , false
    );

    ASSERT_EQ( 25, MOTION2.getCurrentTime() );
}

TEST(
    MotionRoundTripTest
    , GetDirection
)
{
    const auto  MOTION1 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
        , 25
        , true
    );

    ASSERT_TRUE( MOTION1.getDirection() );

    const auto  MOTION2 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
        , 25
        , false
    );

    ASSERT_FALSE( MOTION2.getDirection() );
}

TEST(
    MotionRoundTripTest
    , SetCurrentTime
)
{
    auto    motion1 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
        , 0
        , false
    );

    motion1.setCurrentTime(
        25
        , true
    );

    ASSERT_EQ( 25, motion1.getCurrentTime() );
    ASSERT_TRUE( motion1.getDirection() );

    auto    motion2 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
    );

    motion2.setCurrentTime(
        25
        , false
    );

    ASSERT_EQ( 25, motion2.getCurrentTime() );
    ASSERT_FALSE( motion2.getDirection() );
}

TEST(
    MotionRoundTripTest
    , Update
)
{
    auto    motion = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
    );
    const auto &    MOTION = reinterpret_cast< const MotionRoundTripTest & >( motion );

    motion.update( 50 );

    ASSERT_EQ( 50, MOTION.CURRENT_TIME );
    ASSERT_TRUE( MOTION.DIRECTION );

    motion.update( 60 );

    ASSERT_EQ( 10, MOTION.CURRENT_TIME );
    ASSERT_FALSE( MOTION.DIRECTION );

    motion.update( 250 );

    ASSERT_EQ( 60, MOTION.CURRENT_TIME );
    ASSERT_FALSE( MOTION.DIRECTION );
}

TEST(
    MotionRoundTripTest
    , Update_forIntegerTimePoint
)
{
    auto    motion = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 100
    );

    motion.update( 50 );

    ASSERT_EQ( 15, motion.calc() );
}

TEST(
    MotionRoundTripTest
    , GetBeginValue
)
{
    const auto  MOTION = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
    );

    ASSERT_EQ( 10, MOTION.getBeginValue() );
}

TEST(
    MotionRoundTripTest
    , GetEndValue
)
{
    const auto  MOTION = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
    );

    ASSERT_EQ( 20, MOTION.getEndValue() );
}

TEST(
    MotionRoundTripTest
    , Calc
)
{
    auto    motion = fg::MotionRoundTrip< float, int >(
        0
        , 100
        , 100.0f
    );

    ASSERT_EQ( 0, motion.calc() );

    motion.update( 50 );

    ASSERT_EQ( 50, motion.calc() );

    motion.update( 50 );

    ASSERT_EQ( 100, motion.calc() );

    motion.update( 25 );

    ASSERT_EQ( 75, motion.calc() );
}

struct MotionGroupTest
{
    const std::vector< fg::Motion_< int > * >           MOTION_PTRS;
    const std::vector< fg::MotionLoop_< int > * >       MOTION_LOOP_PTRS;
    const std::vector< fg::MotionRoundTrip_< int > * >  MOTION_ROUND_TRIP_PTRS;
    const std::vector< fg::MotionGroup< int > * >       MOTION_GROUP_PTRS;
};

TEST(
    MotionGroupTest
    , Constructor
)
{
    auto    motion1 = fg::Motion< int, int >(
        10
        , 20
        , 30
    );

    auto    motion2 = fg::Motion< int, float >(
        10.0f
        , 20.0f
        , 30
    );

    auto    motion3 = fg::Motion< int, float >(
        10.0f
        , 20.0f
        , 30
        , 40
    );

    auto    motionLoop1 = fg::MotionLoop< int, int >(
        10
        , 20
        , 30
    );

    auto    motionLoop2 = fg::MotionLoop< int, float >(
        10.0f
        , 20.0f
        , 30
    );

    auto    motionLoop3 = fg::MotionLoop< int, float >(
        10.0f
        , 20.0f
        , 30
        , 40
    );

    auto    motionRoundTrip1 = fg::MotionRoundTrip< int, int >(
        10
        , 20
        , 30
    );

    auto    motionRoundTrip2 = fg::MotionRoundTrip< int, float >(
        10.0f
        , 20.0f
        , 30
    );

    auto    motionRoundTrip3 = fg::MotionRoundTrip< int, float >(
        10.0f
        , 20.0f
        , 30
        , 40
        , true
    );

    auto    motionGroup1 = fg::MotionGroup< int >();
    auto    motionGroup2 = fg::MotionGroup< int >();
    auto    motionGroup3 = fg::MotionGroup< int >();

    auto    group = fg::MotionGroup< int >(
        motion1
        , motionLoop1
        , motionRoundTrip1
        , motionGroup1
        , motion2
        , motionLoop2
        , motionRoundTrip2
        , motionGroup2
        , motion3
        , motionLoop3
        , motionRoundTrip3
        , motionGroup3
    );
    const auto &    GROUP = reinterpret_cast< const MotionGroupTest & >( group );

    const auto &    MOTION_PTRS = GROUP.MOTION_PTRS;
    ASSERT_EQ( 3, MOTION_PTRS.capacity() );
    ASSERT_EQ( 3, MOTION_PTRS.size() );
    ASSERT_EQ( &motion1, MOTION_PTRS[ 0 ] );
    ASSERT_EQ( &motion2, MOTION_PTRS[ 1 ] );
    ASSERT_EQ( &motion3, MOTION_PTRS[ 2 ] );

    const auto &    MOTION_LOOP_PTRS = GROUP.MOTION_LOOP_PTRS;
    ASSERT_EQ( 3, MOTION_LOOP_PTRS.capacity() );
    ASSERT_EQ( 3, MOTION_LOOP_PTRS.size() );
    ASSERT_EQ( &motionLoop1, MOTION_LOOP_PTRS[ 0 ] );
    ASSERT_EQ( &motionLoop2, MOTION_LOOP_PTRS[ 1 ] );
    ASSERT_EQ( &motionLoop3, MOTION_LOOP_PTRS[ 2 ] );

    const auto &    MOTION_ROUND_TRIP_PTRS = GROUP.MOTION_ROUND_TRIP_PTRS;
    ASSERT_EQ( 3, MOTION_ROUND_TRIP_PTRS.capacity() );
    ASSERT_EQ( 3, MOTION_ROUND_TRIP_PTRS.size() );
    ASSERT_EQ( &motionRoundTrip1, MOTION_ROUND_TRIP_PTRS[ 0 ] );
    ASSERT_EQ( &motionRoundTrip2, MOTION_ROUND_TRIP_PTRS[ 1 ] );
    ASSERT_EQ( &motionRoundTrip3, MOTION_ROUND_TRIP_PTRS[ 2 ] );

    const auto &    MOTION_GROUP_PTRS = GROUP.MOTION_GROUP_PTRS;
    ASSERT_EQ( 3, MOTION_GROUP_PTRS.capacity() );
    ASSERT_EQ( 3, MOTION_GROUP_PTRS.size() );
    ASSERT_EQ( &motionGroup1, MOTION_GROUP_PTRS[ 0 ] );
    ASSERT_EQ( &motionGroup2, MOTION_GROUP_PTRS[ 1 ] );
    ASSERT_EQ( &motionGroup3, MOTION_GROUP_PTRS[ 2 ] );
}

TEST(
    MotionGroupTest
    , Initialize_Motion
)
{
    auto    motion1 = fg::Motion< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motion2 = fg::Motion< float, int >(
        300
        , 400
        , 100.0f
        , 50.0f
    );

    auto    group = fg::MotionGroup< float >(
        motion1
        , motion2
    );

    group.update( 75 );

    group.initialize();

    ASSERT_EQ( 100, motion1.calc() );
    ASSERT_EQ( 300, motion2.calc() );
}

TEST(
    MotionGroupTest
    , Initialize_MotionLoop

)
{
    auto    motion1 = fg::MotionLoop< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motion2 = fg::MotionLoop< float, int >(
        300
        , 400
        , 100.0f
        , 25.0f
    );

    auto    group = fg::MotionGroup< float >(
        motion1
        , motion2
    );

    group.update( 75 );

    group.initialize();

    ASSERT_EQ( 100, motion1.calc() );
    ASSERT_EQ( 325, motion2.calc() );
}

TEST(
    MotionGroupTest
    , Initialize_MotionRoundTrip
)
{
    auto    motion1 = fg::MotionRoundTrip< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motion2 = fg::MotionRoundTrip< float, int >(
        300
        , 400
        , 100.0f
        , 25.0f
        , false
    );

    auto    group = fg::MotionGroup< float >(
        motion1
        , motion2
    );

    group.update( 75 );

    group.initialize();

    ASSERT_EQ( 100, motion1.calc() );
    ASSERT_EQ( 325, motion2.calc() );
}

TEST(
    MotionGroupTest
    , Initialize_MotionGroup
)
{
    auto    motion1 = fg::Motion< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motionRoundTrip1 = fg::MotionRoundTrip< float, int >(
        300
        , 400
        , 100.0f
    );

    auto    motionGroup1 = fg::MotionGroup< float >(
        motion1
        , motionRoundTrip1
    );

    auto    motion2 = fg::Motion< float, int >(
        500
        , 600
        , 100.0f
        , 50.0f
    );

    auto    motionRoundTrip2 = fg::MotionRoundTrip< float, int >(
        700
        , 800
        , 100.0f
        , 25.0f
        , false
    );

    auto    motionGroup2 = fg::MotionGroup< float >(
        motion2
        , motionRoundTrip2
    );

    auto    group = fg::MotionGroup< float >(
        motionGroup1
        , motionGroup2
    );

    group.update( 75 );

    group.initialize();

    ASSERT_EQ( 100, motion1.calc() );
    ASSERT_EQ( 300, motionRoundTrip1.calc() );
    ASSERT_EQ( 500, motion2.calc() );
    ASSERT_EQ( 725, motionRoundTrip2.calc() );
}

TEST(
    MotionGroupTest
    , Update_Motion
)
{
    auto    motion1 = fg::Motion< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motion2 = fg::Motion< float, int >(
        300
        , 400
        , 100.0f
        , 50.0f
    );

    auto    group = fg::MotionGroup< float >(
        motion1
        , motion2
    );

    group.update( 75 );

    ASSERT_EQ( 175, motion1.calc() );
    ASSERT_EQ( 325, motion2.calc() );
}

TEST(
    MotionGroupTest
    , Update_MotionLoop
)
{
    auto    motion1 = fg::MotionLoop< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motion2 = fg::MotionLoop< float, int >(
        300
        , 400
        , 100.0f
        , 75.0f
    );

    auto    group = fg::MotionGroup< float >(
        motion1
        , motion2
    );

    group.update( 75 );

    ASSERT_EQ( 175, motion1.calc() );
    ASSERT_EQ( 350, motion2.calc() );
}

TEST(
    MotionGroupTest
    , Update_MotionRoundTrip
)
{
    auto    motion1 = fg::MotionRoundTrip< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motion2 = fg::MotionRoundTrip< float, int >(
        300
        , 400
        , 100.0f
        , 25.0f
        , false
    );

    auto    group = fg::MotionGroup< float >(
        motion1
        , motion2
    );

    group.update( 75 );

    ASSERT_EQ( 175, motion1.calc() );
    ASSERT_EQ( 350, motion2.calc() );
}

TEST(
    MotionGroupTest
    , Update_MotionGroup
)
{
    auto    motion1 = fg::Motion< float, int >(
        100
        , 200
        , 100.0f
    );

    auto    motionRoundTrip1 = fg::MotionRoundTrip< float, int >(
        300
        , 400
        , 100.0f
    );

    auto    motionGroup1 = fg::MotionGroup< float >(
        motion1
        , motionRoundTrip1
    );

    auto    motion2 = fg::Motion< float, int >(
        500
        , 600
        , 100.0f
        , 50.0f
    );

    auto    motionRoundTrip2 = fg::MotionRoundTrip< float, int >(
        700
        , 800
        , 100.0f
        , 25.0f
        , false
    );

    auto    motionGroup2 = fg::MotionGroup< float >(
        motion2
        , motionRoundTrip2
    );

    auto    group = fg::MotionGroup< float >(
        motionGroup1
        , motionGroup2
    );

    group.update( 75 );

    ASSERT_EQ( 175, motion1.calc() );
    ASSERT_EQ( 375, motionRoundTrip1.calc() );
    ASSERT_EQ( 525, motion2.calc() );
    ASSERT_EQ( 750, motionRoundTrip2.calc() );
}

struct MotionManagerTest
{
    const MotionGroupTest   MOTION_GROUP;

    const int   PREV_TIME_POINT;
};

TEST(
    MotionManagerTest
    , Constructor
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
    );

    auto    motionLoop = fg::MotionLoop< int, int >(
        40
        , 50
        , 60
    );

    auto    motionRoundTrip = fg::MotionRoundTrip< int, int >(
        70
        , 80
        , 90
    );

    auto    motionGroup = fg::MotionGroup< int >();

    auto    manager = fg::MotionManager< int >(
        motion
        , motionLoop
        , motionRoundTrip
        , motionGroup
    );
    const auto &    MANAGER = reinterpret_cast< const MotionManagerTest & >( manager );

    const auto &    GROUP = MANAGER.MOTION_GROUP;

    const auto &    MOTION_PTRS = GROUP.MOTION_PTRS;
    ASSERT_EQ( 1, MOTION_PTRS.size() );
    ASSERT_EQ( &motion, MOTION_PTRS[ 0 ] );

    const auto &    MOTION_LOOP_PTRS = GROUP.MOTION_LOOP_PTRS;
    ASSERT_EQ( 1, MOTION_LOOP_PTRS.size() );
    ASSERT_EQ( &motionLoop, MOTION_LOOP_PTRS[ 0 ] );

    const auto &    MOTION_ROUND_TRIP_PTRS = GROUP.MOTION_ROUND_TRIP_PTRS;
    ASSERT_EQ( 1, MOTION_ROUND_TRIP_PTRS.size() );
    ASSERT_EQ( &motionRoundTrip, MOTION_ROUND_TRIP_PTRS[ 0 ] );

    const auto &    MOTION_GROUP_PTRS = GROUP.MOTION_GROUP_PTRS;
    ASSERT_EQ( 1, MOTION_GROUP_PTRS.size() );
    ASSERT_EQ( &motionGroup, MOTION_GROUP_PTRS[ 0 ] );

    ASSERT_EQ( 0, MANAGER.PREV_TIME_POINT );
}

TEST(
    MotionManagerTest
    , Constructor_withInitializeTime
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
    );

    auto    motionLoop = fg::MotionLoop< int, int >(
        40
        , 50
        , 60
    );

    auto    motionRoundTrip = fg::MotionRoundTrip< int, int >(
        70
        , 80
        , 90
    );

    auto    motionGroup = fg::MotionGroup< int >();

    auto    manager = fg::MotionManager< int >(
        100
        , motion
        , motionLoop
        , motionRoundTrip
        , motionGroup
    );
    const auto &    MANAGER = reinterpret_cast< const MotionManagerTest & >( manager );

    const auto &    GROUP = MANAGER.MOTION_GROUP;

    const auto &    MOTION_PTRS = GROUP.MOTION_PTRS;
    ASSERT_EQ( 1, MOTION_PTRS.size() );
    ASSERT_EQ( &motion, MOTION_PTRS[ 0 ] );

    const auto &    MOTION_LOOP_PTRS = GROUP.MOTION_LOOP_PTRS;
    ASSERT_EQ( 1, MOTION_LOOP_PTRS.size() );
    ASSERT_EQ( &motionLoop, MOTION_LOOP_PTRS[ 0 ] );

    const auto &    MOTION_ROUND_TRIP_PTRS = GROUP.MOTION_ROUND_TRIP_PTRS;
    ASSERT_EQ( 1, MOTION_ROUND_TRIP_PTRS.size() );
    ASSERT_EQ( &motionRoundTrip, MOTION_ROUND_TRIP_PTRS[ 0 ] );

    const auto &    MOTION_GROUP_PTRS = GROUP.MOTION_GROUP_PTRS;
    ASSERT_EQ( 1, MOTION_GROUP_PTRS.size() );
    ASSERT_EQ( &motionGroup, MOTION_GROUP_PTRS[ 0 ] );

    ASSERT_EQ( 100, MANAGER.PREV_TIME_POINT );
}

TEST(
    MotionManagerTest
    , Initialize
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    auto    motionGroup = fg::MotionGroup< int >();

    auto    manager = fg::MotionManager< int >(
        100
        , motion
    );
    const auto &    MANAGER = reinterpret_cast< const MotionManagerTest & >( manager );

    manager.update( 110 );

    manager.initialize( 200 );

    ASSERT_EQ( 200, MANAGER.PREV_TIME_POINT );
    ASSERT_EQ( 10, MOTION.CURRENT_TIME );
}

TEST(
    MotionManagerTest
    , InitializeAll
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    auto    motionGroup = fg::MotionGroup< int >();

    auto    manager = fg::MotionManager< int >(
        100
        , motion
    );
    const auto &    MANAGER = reinterpret_cast< const MotionManagerTest & >( manager );

    manager.update( 110 );

    manager.initializeAll( 200 );

    ASSERT_EQ( 200, MANAGER.PREV_TIME_POINT );
    ASSERT_EQ( 0, MOTION.CURRENT_TIME );
}

TEST(
    MotionManagerTest
    , GetPrevTimePoint
)
{
    const auto  MANAGER = fg::MotionManager< int >( 10 );

    ASSERT_EQ( 10, MANAGER.getPrevTimePoint() );
}

TEST(
    MotionManagerTest
    , Update
)
{
    auto    motion = fg::Motion< int, int >(
        10
        , 20
        , 30
    );
    const auto &    MOTION = reinterpret_cast< const MotionTest & >( motion );

    auto    motionGroup = fg::MotionGroup< int >();

    auto    manager = fg::MotionManager< int >(
        100
        , motion
    );
    const auto &    MANAGER = reinterpret_cast< const MotionManagerTest & >( manager );

    manager.update( 110 );

    ASSERT_EQ( 110, MANAGER.PREV_TIME_POINT );
    ASSERT_EQ( 10, MOTION.CURRENT_TIME );

    manager.update( 120 );

    ASSERT_EQ( 120, MANAGER.PREV_TIME_POINT );
    ASSERT_EQ( 20, MOTION.CURRENT_TIME );
}
