﻿#include "fg/util/test.h"
#include "fg/common/unique.h"

struct ConstructorConstUniqueTestImpl
{
    int i;
};

class ConstructorConstUniqueTest : public fg::UniqueWrapper< ConstructorConstUniqueTest, ConstructorConstUniqueTestImpl >
{
public:
    static ConstUnique createConstUnique(
        int _i
    )
    {
        return new ConstructorConstUniqueTestImpl{
            _i,
        };
    }

    static Unique createUnique(
        int _i
    )
    {
        return new ConstructorConstUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        ConstructorConstUniqueTest *    _this
    )
    {
        delete &**_this;
    }
};

struct ConstructorConstUniqueTestUnique
{
    std::unique_ptr< ConstructorConstUniqueTestImpl >    uniquePtr;
};

TEST(
    ConstUniqueTest
    , Constructor_default
)
{
    fg::ConstUnique< ConstructorConstUniqueTest >  constUnique;
    ASSERT_EQ( nullptr, reinterpret_cast< ConstructorConstUniqueTestUnique & >( constUnique ).uniquePtr.get() );

    auto    constUnique2 = fg::ConstUnique< ConstructorConstUniqueTest >();
    ASSERT_EQ( nullptr, reinterpret_cast< ConstructorConstUniqueTestUnique & >( constUnique2 ).uniquePtr.get() );
}

TEST(
    ConstUniqueTest
    , Constructor_move
)
{
    auto    constUnique = ConstructorConstUniqueTest::createConstUnique( 10 );
    const auto  IMPL_PTR = constUnique.get();
    ASSERT_NE( nullptr, IMPL_PTR );

    const auto  MOVE_CONST_UNIQUE = std::move( constUnique );
    ASSERT_EQ( IMPL_PTR, MOVE_CONST_UNIQUE.get() );

    ASSERT_EQ( nullptr, constUnique.get() );
}

TEST(
    ConstUniqueTest
    , Constructor_moveUnique
)
{
    auto    unique = ConstructorConstUniqueTest::createUnique( 10 );
    const auto  IMPL_PTR = unique.get();
    ASSERT_NE( nullptr, IMPL_PTR );

    auto    moveConstUnique = ConstructorConstUniqueTest::ConstUnique( std::move( unique ) );
    ASSERT_EQ( IMPL_PTR, moveConstUnique.get() );

    ASSERT_EQ( nullptr, unique.get() );
}

TEST(
    ConstUniqueTest
    , Move
)
{
    auto    constUnique = ConstructorConstUniqueTest::createConstUnique( 10 );
    const auto  IMPL_PTR = constUnique.get();
    ASSERT_NE( nullptr, IMPL_PTR );

    auto    moveConstUnique = ConstructorConstUniqueTest::ConstUnique();

    moveConstUnique = std::move( constUnique );
    ASSERT_EQ( IMPL_PTR, moveConstUnique.get() );

    ASSERT_EQ( nullptr, constUnique.get() );
}

TEST(
    ConstUniqueTest
    , Move_unique
)
{
    auto    unique = ConstructorConstUniqueTest::createUnique( 10 );
    const auto  IMPL_PTR = unique.get();
    ASSERT_NE( nullptr, IMPL_PTR );

    auto    moveConstUnique = ConstructorConstUniqueTest::ConstUnique();

    moveConstUnique = std::move( unique );
    ASSERT_EQ( IMPL_PTR, moveConstUnique.get() );

    ASSERT_EQ( nullptr, unique.get() );
}

struct CreateConstUniqueTestImpl
{
    int i;
};

class CreateConstUniqueTest : public fg::UniqueWrapper< CreateConstUniqueTest, CreateConstUniqueTestImpl >
{
public:
    static ConstUnique create(
        int _i
    )
    {
        return new CreateConstUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        CreateConstUniqueTest * _this
    )
    {
        delete &**_this;
    }
};

struct CreateConstUniqueTestConstUnique
{
    std::unique_ptr< CreateConstUniqueTestImpl >    uniquePtr;
};

struct CreateConstUniqueTestConstInterfacePtr
{
    const CreateConstUniqueTest *   INTERFACE_PTR;
};

TEST(
    ConstUniqueTest
    , Create
)
{
    auto    constUnique = CreateConstUniqueTest::create( 10 );

    auto    implPtr = reinterpret_cast< CreateConstUniqueTestConstUnique & >( constUnique ).uniquePtr.get();
    ASSERT_NE( nullptr, implPtr );
    const auto &    IMPL = *implPtr;

    ASSERT_EQ( 10, IMPL.i );
}

struct DeleteConstUniqueTestImpl
{
    int &   i;
};

class DeleteConstUniqueTest : public fg::UniqueWrapper< DeleteConstUniqueTest, DeleteConstUniqueTestImpl >
{
public:
    static ConstUnique create(
        int &   _i
    )
    {
        return new DeleteConstUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        DeleteConstUniqueTest * _this
    )
    {
        auto    implPtr = &**_this;

        implPtr->i = 20;

        delete implPtr;
    }
};

struct DeleteConstUniqueTestConstUnique
{
    std::unique_ptr< DeleteConstUniqueTestImpl >    uniquePtr;
};

TEST(
    ConstUniqueTest
    , Delete
)
{
    auto    i = 10;

    {
        auto    constUnique = DeleteConstUniqueTest::create( i );

        auto    implPtr = reinterpret_cast< DeleteConstUniqueTestConstUnique & >( constUnique ).uniquePtr.get();
        ASSERT_NE( nullptr, implPtr );
    }

    ASSERT_EQ( 20, i );
}

struct DeleteOmitDestroyConstUniqueTestImpl
{
    int &   i;

    ~DeleteOmitDestroyConstUniqueTestImpl(
    )
    {
        this->i = 20;
    }
};

class DeleteOmitDestroyConstUniqueTest : public fg::UniqueWrapper< DeleteOmitDestroyConstUniqueTest, DeleteOmitDestroyConstUniqueTestImpl >
{
public:
    static ConstUnique create(
        int &   _i
    )
    {
        return new DeleteOmitDestroyConstUniqueTestImpl{
            _i,
        };
    }
};

struct DeleteOmitDestroyConstUniqueTestConstUnique
{
    std::unique_ptr< DeleteOmitDestroyConstUniqueTestImpl > uniquePtr;
};

TEST(
    ConstUniqueTest
    , Delete_omitDestroy
)
{
    auto    i = 10;

    {
        auto    constUnique = DeleteOmitDestroyConstUniqueTest::create( i );

        auto    implPtr = reinterpret_cast< DeleteOmitDestroyConstUniqueTestConstUnique & >( constUnique ).uniquePtr.get();
        ASSERT_NE( nullptr, implPtr );
    }

    ASSERT_EQ( 20, i );
}

TEST(
    ConstUniqueTest
    , Destroy
)
{
    auto    i = 10;

    auto    constUnique = DeleteConstUniqueTest::create( i );

    constUnique.destroy();

    ASSERT_EQ( reinterpret_cast< DeleteConstUniqueTestConstUnique & >( constUnique ).uniquePtr.get(), nullptr );

    ASSERT_EQ( 20, i );
}

TEST(
    ConstUniqueTest
    , Get
)
{
    const auto  UNIQUE = CreateConstUniqueTest::create( 10 );
    ASSERT_EQ( reinterpret_cast< const CreateConstUniqueTestConstInterfacePtr & >( UNIQUE ).INTERFACE_PTR, UNIQUE.get() );
}

struct AsteriskOperatorConstUniqueTestImpl
{
};

class AsteriskOperatorConstUniqueTest : public fg::UniqueWrapper< AsteriskOperatorConstUniqueTest, AsteriskOperatorConstUniqueTestImpl >
{
public:
    static ConstUnique create(
    )
    {
        return new AsteriskOperatorConstUniqueTestImpl;
    }

    static void destroy(
        AsteriskOperatorConstUniqueTest *   _this
    )
    {
        delete &**_this;
    }
};

TEST(
    ConstUniqueTest
    , AsteriskOperator
)
{
    const auto  UNIQUE = AsteriskOperatorConstUniqueTest::create();
    ASSERT_EQ( UNIQUE.get(), &( *UNIQUE ) );
}

struct ArrowOperatorConstUniqueTestImpl
{
};

class ArrowOperatorConstUniqueTest : public fg::UniqueWrapper< ArrowOperatorConstUniqueTest, ArrowOperatorConstUniqueTestImpl >
{
public:
    static ConstUnique create(
    )
    {
        return new ArrowOperatorConstUniqueTestImpl;
    }

    static void destroy(
        ArrowOperatorConstUniqueTest *  _this
    )
    {
        delete &**_this;
    }

    const void * getPtr(
    ) const
    {
        return this;
    }
};

TEST(
    ConstUniqueTest
    , ArrowOperator
)
{
    const auto  UNIQUE = ArrowOperatorConstUniqueTest::create();
    ASSERT_EQ( UNIQUE.get(), UNIQUE->getPtr() );
}

struct ReleaseConstUniqueTestImpl
{
    int &   i;

    ~ReleaseConstUniqueTestImpl(
    )
    {
        this->i = 20;
    }
};

class ReleaseConstUniqueTest : public fg::UniqueWrapper< ReleaseConstUniqueTest, ReleaseConstUniqueTestImpl >
{
public:
    static ConstUnique create(
        int &   _i
    )
    {
        return new ReleaseConstUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        ReleaseConstUniqueTest *    _this
    )
    {
        delete &**_this;
    }
};

TEST(
    ConstUniqueTest
    , Release
)
{
    auto    i = 10;

    auto    unique = ReleaseConstUniqueTest::create( i );
    ASSERT_NE( nullptr, unique.get() );

    auto    implPtr = &**( unique.release() );

    unique.destroy();

    ASSERT_EQ( 10, i );

    delete implPtr;

    ASSERT_EQ( 20, i );
}

struct ConstructorUniqueTestImpl
{
    int i;
};

class ConstructorUniqueTest : public fg::UniqueWrapper< ConstructorUniqueTest, ConstructorUniqueTestImpl >
{
public:
    static Unique create(
        int _i
    )
    {
        return new ConstructorUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        ConstructorUniqueTest * _this
    )
    {
        delete &**_this;
    }
};

struct ConstructorUniqueTestUnique
{
    std::unique_ptr< ConstructorUniqueTestImpl >    uniquePtr;
};

TEST(
    UniqueTest
    , Constructor_default
)
{
    ConstructorUniqueTest::Unique   unique;
    ASSERT_EQ( nullptr, reinterpret_cast< ConstructorUniqueTestUnique & >( unique ).uniquePtr.get() );

    auto    unique2 = ConstructorUniqueTest::Unique();
    ASSERT_EQ( nullptr, reinterpret_cast< ConstructorUniqueTestUnique & >( unique2 ).uniquePtr.get() );
}

TEST(
    UniqueTest
    , Constructor_move
)
{
    auto    unique = ConstructorUniqueTest::create( 10 );
    const auto  IMPL_PTR = unique.get();
    ASSERT_NE( nullptr, IMPL_PTR );

    auto    moveUnique = std::move( unique );
    ASSERT_EQ( IMPL_PTR, moveUnique.get() );

    ASSERT_EQ( nullptr, unique.get() );
}

TEST(
    UniqueTest
    , Move
)
{
    auto    unique = ConstructorUniqueTest::create( 10 );
    const auto  IMPL_PTR = unique.get();
    ASSERT_NE( nullptr, IMPL_PTR );

    auto    moveUnique = ConstructorUniqueTest::Unique();

    moveUnique = std::move( unique );
    ASSERT_EQ( IMPL_PTR, moveUnique.get() );

    ASSERT_EQ( nullptr, unique.get() );
}

struct CreateUniqueTestImpl
{
    int i;
};

class CreateUniqueTest : public fg::UniqueWrapper< CreateUniqueTest, CreateUniqueTestImpl >
{
public:
    static Unique create(
        int _i
    )
    {
        return new CreateUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        CreateUniqueTest *  _this
    )
    {
        delete &**_this;
    }
};

struct CreateUniqueTestUnique
{
    std::unique_ptr< CreateUniqueTestImpl > uniquePtr;
};

struct CreateUniqueTestInterfacePtr
{
    CreateUniqueTest *  interfacePtr;
};

TEST(
    UniqueTest
    , Create
)
{
    auto    unique = CreateUniqueTest::create( 10 );

    auto    implPtr = reinterpret_cast< CreateUniqueTestUnique & >( unique ).uniquePtr.get();
    ASSERT_NE( nullptr, implPtr );
    const auto &    IMPL = *implPtr;

    ASSERT_EQ( 10, IMPL.i );
}

struct DeleteUniqueTestImpl
{
    int &   i;
};

class DeleteUniqueTest : public fg::UniqueWrapper< DeleteUniqueTest, DeleteUniqueTestImpl >
{
public:
    static Unique create(
        int &   _i
    )
    {
        return new DeleteUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        DeleteUniqueTest *  _this
    )
    {
        auto    implPtr = &**_this;

        implPtr->i = 20;

        delete implPtr;
    }
};

struct DeleteUniqueTestUnique
{
    std::unique_ptr< DeleteUniqueTestImpl > uniquePtr;
};

TEST(
    UniqueTest
    , Delete
)
{
    auto    i = 10;

    {
        auto    unique = DeleteUniqueTest::create( i );

        auto    implPtr = reinterpret_cast< DeleteUniqueTestUnique & >( unique ).uniquePtr.get();
        ASSERT_NE( nullptr, implPtr );
    }

    ASSERT_EQ( 20, i );
}

TEST(
    UniqueTest
    , Destroy
)
{
    auto    i = 10;

    auto    unique = DeleteUniqueTest::create( i );

    unique.destroy();

    ASSERT_EQ( reinterpret_cast< DeleteUniqueTestUnique & >( unique ).uniquePtr.get(), nullptr );

    ASSERT_EQ( 20, i );
}

TEST(
    UniqueTest
    , Get
)
{
    const auto  UNIQUE = CreateUniqueTest::create( 10 );
    ASSERT_EQ( reinterpret_cast< const CreateUniqueTestInterfacePtr & >( UNIQUE ).interfacePtr, UNIQUE.get() );
}

struct AsteriskOperatorUniqueTestImpl
{
};

class AsteriskOperatorUniqueTest : public fg::UniqueWrapper< AsteriskOperatorUniqueTest, AsteriskOperatorUniqueTestImpl >
{
public:
    static Unique create(
    )
    {
        return new AsteriskOperatorUniqueTestImpl;
    }

    static void destroy(
        AsteriskOperatorUniqueTest *    _this
    )
    {
        delete &**_this;
    }
};

TEST(
    UniqueTest
    , AsteriskOperator
)
{
    const auto  UNIQUE = AsteriskOperatorUniqueTest::create();
    ASSERT_EQ( UNIQUE.get(), &( *UNIQUE ) );
}

struct ArrowOperatorUniqueTestImpl
{
};

class ArrowOperatorUniqueTest : public fg::UniqueWrapper< ArrowOperatorUniqueTest, ArrowOperatorUniqueTestImpl >
{
public:
    static Unique create(
    )
    {
        return new ArrowOperatorUniqueTestImpl;
    }

    static void destroy(
        ArrowOperatorUniqueTest *   _this
    )
    {
        delete &**_this;
    }

    void * getPtr(
    )
    {
        return this;
    }
};

TEST(
    UniqueTest
    , ArrowOperator
)
{
    const auto  UNIQUE = ArrowOperatorUniqueTest::create();
    ASSERT_EQ( UNIQUE.get(), UNIQUE->getPtr() );
}

struct ReleaseUniqueTestImpl
{
    int &   i;

    ~ReleaseUniqueTestImpl(
    )
    {
        this->i = 20;
    }
};

class ReleaseUniqueTest : public fg::UniqueWrapper< ReleaseUniqueTest, ReleaseUniqueTestImpl >
{
public:
    static Unique create(
        int &   _i
    )
    {
        return new ReleaseUniqueTestImpl{
            _i,
        };
    }

    static void destroy(
        ReleaseUniqueTest * _this
    )
    {
        delete &**_this;
    }
};

TEST(
    UniqueTest
    , Release
)
{
    auto    i = 10;

    auto    unique = ReleaseUniqueTest::create( i );
    ASSERT_NE( nullptr, unique.get() );

    auto    implPtr = &**( unique.release() );

    unique.destroy();

    ASSERT_EQ( 10, i );

    delete implPtr;

    ASSERT_EQ( 20, i );
}

struct AsteriskOperatorTestImpl
{
    int i;
};

int getImpl(
    const AsteriskOperatorTestImpl *    _THIS
)
{
    return _THIS->i;
}

void setImpl(
    AsteriskOperatorTestImpl *  _this
    , int                       _i
)
{
    _this->i = _i;
}

class AsteriskOperatorTest : public fg::UniqueWrapper< AsteriskOperatorTest, AsteriskOperatorTestImpl >
{
public:
    static Unique create(
        int _i
    )
    {
        return new AsteriskOperatorTestImpl{
            _i,
        };
    }

    static void destroy(
        AsteriskOperatorTest *  _this
    )
    {
        delete &**_this;
    }

    int get(
    ) const
    {
        return getImpl( &**this );
    }

    void set(
        int _i
    )
    {
        return setImpl(
            &**this
            , _i
        );
    }
};

TEST(
    UniqueWrapperTest
    , AsteriskOperator_const
)
{
    const auto  UNIQUE = AsteriskOperatorTest::create( 10 );

    ASSERT_EQ( 10, UNIQUE->get() );
}

TEST(
    UniqueWrapperTest
    , AsteriskOperator
)
{
    const auto  UNIQUE = AsteriskOperatorTest::create( 10 );

    UNIQUE->set( 20 );

    ASSERT_EQ( 20, UNIQUE->get() );
}

struct ArrowOperatorTestImpl
{
    int i;

    int get(
    ) const
    {
        return this->i;
    }

    void set(
        int _i
    )
    {
        this->i = _i;
    }
};

class ArrowOperatorTest : public fg::UniqueWrapper< ArrowOperatorTest, ArrowOperatorTestImpl >
{
public:
    static Unique create(
        int _i
    )
    {
        return new ArrowOperatorTestImpl{
            _i,
        };
    }

    static void destroy(
        ArrowOperatorTest * _this
    )
    {
        delete &**_this;
    }

    int get(
    ) const
    {
        return ( *this )->get();
    }

    void set(
        int _i
    )
    {
        ( *this )->set( _i );
    }
};

TEST(
    UniqueWrapperTest
    , ArrowOperator_const
)
{
    const auto  UNIQUE = ArrowOperatorTest::create( 10 );

    ASSERT_EQ( 10, UNIQUE->get() );
}

TEST(
    UniqueWrapperTest
    , ArrowOperator
)
{
    const auto  UNIQUE = ArrowOperatorTest::create( 10 );

    UNIQUE->set( 20 );

    ASSERT_EQ( 20, UNIQUE->get() );
}

class OmitImplTest : public fg::UniqueWrapper< OmitImplTest >
{
    int i;

    OmitImplTest(
        int _i
    )
        : i( _i )
    {
    }

public:
    static Unique create(
        int _i
    )
    {
        return new OmitImplTest( _i );
    }

    static void destroy(
        OmitImplTest *  _this
    )
    {
        delete _this;
    }

    int get(
    ) const
    {
        return this->i;
    }

    void set(
        int _i
    )
    {
        this->i = _i;
    }
};

TEST(
    UniqueWrapperTest
    , OmitImpl
)
{
    const auto  UNIQUE = ArrowOperatorTest::create( 10 );

    UNIQUE->set( 20 );

    ASSERT_EQ( 20, UNIQUE->get() );
}
