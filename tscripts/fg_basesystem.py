# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.BUILDER = cpp.shlib

module.TARGET = 'fg-basesystem'

module.SOURCE = {
    'basesystem' : {
        'window' : [
            'mainwindow.cpp',
            'mainwindowdrawevent.cpp',
        ],
        'controller' : {
            'raw' : [
                'event.cpp',
            ],
        },
    },
}
