# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.BUILDER = cpp.shlib

module.TARGET = 'fg-gl'

module.SOURCE = {
    'gl' : [
        'contextargs.cpp',
        'context.cpp',
        'currentcontext.cpp',
        'thread.cpp',
        'threadexecutor.cpp',
        'textures.cpp',
        'texturebinder2d.cpp',
        'samplers.cpp',
        'buffers.cpp',
        'bufferbinderarray.cpp',
        'bufferbinderelementarray.cpp',
        'vertexarrays.cpp',
        'vertexarraybinder.cpp',
        'vertexshader.cpp',
        'fragmentshader.cpp',
        'picture.cpp',
        'program.cpp',
    ],
}
