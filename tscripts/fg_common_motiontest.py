# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'fg-common-motiontest'

module.SOURCE = {
    'common' : 'motiontest.cpp',
}
