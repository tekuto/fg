# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.gtest

module.TARGET = 'fg-common-uniquetest'

module.SOURCE = {
    'common' : 'uniquetest.cpp',
}
