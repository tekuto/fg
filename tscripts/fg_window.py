# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.BUILDER = cpp.shlib

module.TARGET = 'fg-window'

module.SOURCE = {
    'window' : [
        'window.cpp',
        'eventmanagers.cpp',
        'paintevent.cpp',
        'closeevent.cpp',
        'eventprocessor.cpp',
    ],
}
