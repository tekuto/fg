# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.BUILDER = cpp.shlib

module.TARGET = 'fg-controller'

module.SOURCE = {
    'controller' : {
        'raw' : [
            'id.cpp',
            'button.cpp',
            'axis.cpp',
            'connectaction.cpp',
            'buttonaction.cpp',
            'axisaction.cpp',
            'action.cpp',
            'actionbuffer.cpp',
        ],
    },
}
