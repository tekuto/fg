# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.BUILDER = cpp.shlib

module.TARGET = 'fg-core'

module.SOURCE = {
    'core' : {
        'module' : [
            'context.cpp',
        ],
        'basesystem' : [
            'context.cpp',
        ],
        'game' : [
            'context.cpp',
        ],
        'package' : [
            'resource.cpp',
        ],
        'dataelement' : [
            'dataelement.cpp',
            'string.cpp',
            'list.cpp',
            {
                'map' : [
                    'map.cpp',
                    'keys.cpp',
                ],
            },
        ],
        'state' : [
            'state.cpp',
            'creating.cpp',
            'thread.cpp',
            'threadsingleton.cpp',
            'threadbackground.cpp',
            'threadbackgroundsingleton.cpp',
            'event.cpp',
            'eventbackground.cpp',
            'enterevent.cpp',
            'exitevent.cpp',
            'reenterevent.cpp',
            'leaveevent.cpp',
            'joiner.cpp',
            'eventmanager.cpp',
            'eventregistermanager.cpp',
        ],
        'save' : [
            'save.cpp',
            'writer.cpp',
            'writenotify.cpp',
            'reader.cpp',
            'readnotify.cpp',
            'remover.cpp',
            'removenotify.cpp',
        ],
    },
}
