# -*- coding: utf-8 -*-

from taf import *

from waflib.Tools import waf_unit_test

APPNAME = 'fg'
VERSION = '0.45.1'

out = 'build'

taf.PACKAGE_NAME = 'fg'

taf.LOAD_TOOLS = [
    'compiler_cxx',
    'taf.tools.cpp',
]

taf.POST_FUNCTIONS = [
    waf_unit_test.summary,
]
