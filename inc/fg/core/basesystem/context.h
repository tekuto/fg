﻿#ifndef FG_CORE_BASESYSTEM_CONTEXT_H
#define FG_CORE_BASESYSTEM_CONTEXT_H

#include "fg/def/core/basesystem/context.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_VOID(
        fgBasesystemContextSetStateData(
            FgBasesystemContext *
            , void *
            , FgBasesystemContextStateDataDestroyProc
        )
    )

    FG_FUNCTION_VOID(
        fgBasesystemContextSetWaitEndProc(
            FgBasesystemContext *
            , FgBasesystemContextWaitEndProc
            , void *
        )
    )

    FG_FUNCTION_PTR(
        FgState
        , fgBasesystemContextGetState(
            FgBasesystemContext *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    void BasesystemContext< STATE_DATA_T >::setStateData(
        STATE_DATA_T *          _dataPtr
        , StateDataDestroyProc  _procPtr
    )
    {
        fgBasesystemContextSetStateData(
            &**this
            , _dataPtr
            , reinterpret_cast< FgBasesystemContextStateDataDestroyProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    template< typename DATA_T >
    void BasesystemContext< STATE_DATA_T >::setWaitEndProc(
        WaitEndProc< DATA_T >   _procPtr
        , DATA_T &              _data
    )
    {
        fgBasesystemContextSetWaitEndProc(
            &**this
            , reinterpret_cast< FgBasesystemContextWaitEndProc >( _procPtr )
            , &_data
        );
    }

    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & BasesystemContext< STATE_DATA_T >::getState(
    )
    {
        return reinterpret_cast< State< STATE_DATA_T > & >( *fgBasesystemContextGetState( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_BASESYSTEM_CONTEXT_H
