﻿#ifndef FG_CORE_MODULE_CONTEXT_H
#define FG_CORE_MODULE_CONTEXT_H

#include "fg/def/core/module/context.h"
//#include "fg/def/core/module/savedata.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        void
        , fgModuleContextGetData(
            FgModuleContext *
        )
    )

    FG_FUNCTION_VOID(
        fgModuleContextSetData(
            FgModuleContext *
            , void *
            , FgModuleContextDataDestroyProc
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename DATA_T >
    DATA_T & ModuleContext::getData(
    )
    {
        return *static_cast< DATA_T * >( fgModuleContextGetData( &**this ) );
    }

    template< typename DATA_T >
    void ModuleContext::setData(
        DATA_T *                    _dataPtr
        , DataDestroyProc< DATA_T > _procPtr
    )
    {
        fgModuleContextSetData(
            &**this
            , _dataPtr
            , reinterpret_cast< FgModuleContextDataDestroyProc >( _procPtr )
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_MODULE_CONTEXT_H
