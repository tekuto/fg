﻿#ifndef FG_CORE_SAVE_READNOTIFY_H
#define FG_CORE_SAVE_READNOTIFY_H

#include "fg/def/core/save/readnotify.h"
#include "fg/def/core/save/save.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgSaveReadNotifyGetState(
            FgSaveReadNotify *
        )
    )

    FG_FUNCTION_PTR(
        const FgSave
        , fgSaveReadNotifyGetSave(
            const FgSaveReadNotify *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & SaveReadNotify< STATE_DATA_T >::getState(
    )
    {
        return reinterpret_cast< State< STATE_DATA_T > & >( *fgSaveReadNotifyGetState( &**this ) );
    }

    template< typename STATE_DATA_T >
    const Save * SaveReadNotify< STATE_DATA_T >::getSave(
    ) const
    {
        return reinterpret_cast< const Save * >( fgSaveReadNotifyGetSave( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_READNOTIFY_H
