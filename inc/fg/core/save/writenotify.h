﻿#ifndef FG_CORE_SAVE_WRITENOTIFY_H
#define FG_CORE_SAVE_WRITENOTIFY_H

#include "fg/def/core/save/writenotify.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgSaveWriteNotifyGetState(
            FgSaveWriteNotify *
        )
    )

    FG_FUNCTION_BOOL(
        fgSaveWriteNotifyWrote(
            const FgSaveWriteNotify *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & SaveWriteNotify< STATE_DATA_T >::getState(
    )
    {
        return reinterpret_cast< State< STATE_DATA_T > & >( *fgSaveWriteNotifyGetState( &**this ) );
    }

    template< typename STATE_DATA_T >
    bool SaveWriteNotify< STATE_DATA_T >::wrote(
    ) const
    {
        return fgSaveWriteNotifyWrote( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_WRITENOTIFY_H
