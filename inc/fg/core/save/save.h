﻿#ifndef FG_CORE_SAVE_SAVE_H
#define FG_CORE_SAVE_SAVE_H

#include "fg/def/core/save/save.h"
#include "fg/def/core/dataelement/dataelement.h"
#include "fg/def/core/dataelement/string.h"
#include "fg/def/core/dataelement/list.h"
#include "fg/def/core/dataelement/map/map.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgSave
        , fgSaveCreateForString(
            const FgDataElementString *
        )
    )

    FG_FUNCTION_PTR(
        FgSave
        , fgSaveCreateForList(
            const FgDataElementList *
        )
    )

    FG_FUNCTION_PTR(
        FgSave
        , fgSaveCreateForMap(
            const FgDataElementMap *
        )
    )

    FG_FUNCTION_VOID(
        fgSaveDestroy(
            FgSave *
        )
    )

    FG_FUNCTION_PTR(
        FgSave
        , fgSaveClone(
            const FgSave *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElement
        , fgSaveGetDataElement(
            const FgSave *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline Save::ConstUnique Save::create(
        const DataElementString &   _STRING
    )
    {
        return fgSaveCreateForString( &*_STRING );
    }

    inline Save::ConstUnique Save::create(
        const DataElementList & _LIST
    )
    {
        return fgSaveCreateForList( &*_LIST );
    }

    inline Save::ConstUnique Save::create(
        const DataElementMap &  _MAP
    )
    {
        return fgSaveCreateForMap( &*_MAP );
    }

    inline void Save::destroy(
        Save *  _this
    )
    {
        fgSaveDestroy( &**_this );
    }

    inline Save::ConstUnique Save::clone(
    ) const
    {
        return fgSaveClone( &**this );
    }

    inline const DataElement & Save::getDataElement(
    ) const
    {
        return reinterpret_cast< const DataElement & >( *fgSaveGetDataElement( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_SAVE_H
