﻿#ifndef FG_CORE_SAVE_REMOVENOTIFY_H
#define FG_CORE_SAVE_REMOVENOTIFY_H

#include "fg/def/core/save/removenotify.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgSaveRemoveNotifyGetState(
            FgSaveRemoveNotify *
        )
    )

    FG_FUNCTION_BOOL(
        fgSaveRemoveNotifyRemoved(
            const FgSaveRemoveNotify *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & SaveRemoveNotify< STATE_DATA_T >::getState(
    )
    {
        return reinterpret_cast< State< STATE_DATA_T > & >( *fgSaveRemoveNotifyGetState( &**this ) );
    }

    template< typename STATE_DATA_T >
    bool SaveRemoveNotify< STATE_DATA_T >::removed(
    ) const
    {
        return fgSaveRemoveNotifyRemoved( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_REMOVENOTIFY_H
