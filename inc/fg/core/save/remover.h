﻿#ifndef FG_CORE_SAVE_REMOVER_H
#define FG_CORE_SAVE_REMOVER_H

#include "fg/def/core/save/remover.h"
#include "fg/def/core/save/removenotify.h"
#include "fg/def/core/state/state.h"
#include "fg/def/core/module/context.h"
#include "fg/def/core/basesystem/context.h"
#include "fg/def/core/game/context.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_BOOL(
        fgSaveRemoverRemove(
            const FgModuleContext *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        FgSaveRemover
        , fgSaveRemoverCreateForModuleContext(
            FgState *
            , const FgModuleContext *
            , const char *
            , FgSaveRemoveNotifyProc
        )
    )

    FG_FUNCTION_PTR(
        FgSaveRemover
        , fgSaveRemoverCreateForBasesystemContext(
            FgState *
            , const FgBasesystemContext *
            , const char *
            , FgSaveRemoveNotifyProc
        )
    )

    FG_FUNCTION_PTR(
        FgSaveRemover
        , fgSaveRemoverCreateForGameContext(
            FgState *
            , const FgGameContext *
            , const char *
            , FgSaveRemoveNotifyProc
        )
    )

    FG_FUNCTION_VOID(
        fgSaveRemoverDestroy(
            FgSaveRemover *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline bool SaveRemover::remove(
        const ModuleContext &   _MODULE_CONTEXT
        , const char *          _NAME
    )
    {
        return fgSaveRemoverRemove(
            &*_MODULE_CONTEXT
            , _NAME
        );
    }

    template< typename STATE_DATA_T >
    SaveRemover::Unique SaveRemover::create(
        State< STATE_DATA_T > &                             _state
        , const ModuleContext &                             _MODULE_CONTEXT
        , const char *                                      _NAME
        , typename SaveRemoveNotify< STATE_DATA_T >::Proc   _notifyProcPtr
    )
    {
        return fgSaveRemoverCreateForModuleContext(
            &*_state
            , &*_MODULE_CONTEXT
            , _NAME
            , reinterpret_cast< FgSaveRemoveNotifyProc >( _notifyProcPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename BASESYSTEM_CONTEXT_STATE_DATA_T
    >
    SaveRemover::Unique SaveRemover::create(
        State< STATE_DATA_T > &                                         _state
        , const BasesystemContext< BASESYSTEM_CONTEXT_STATE_DATA_T > &  _BASESYSTEM_CONTEXT
        , const char *                                                  _NAME
        , typename SaveRemoveNotify< STATE_DATA_T >::Proc               _notifyProcPtr
    )
    {
        return fgSaveRemoverCreateForBasesystemContext(
            &*_state
            , &*_BASESYSTEM_CONTEXT
            , _NAME
            , reinterpret_cast< FgSaveRemoveNotifyProc >( _notifyProcPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename GAME_CONTEXT_STATE_DATA_T
    >
    SaveRemover::Unique SaveRemover::create(
        State< STATE_DATA_T > &                             _state
        , const GameContext< GAME_CONTEXT_STATE_DATA_T > &  _GAME_CONTEXT
        , const char *                                      _NAME
        , typename SaveRemoveNotify< STATE_DATA_T >::Proc   _notifyProcPtr
    )
    {
        return fgSaveRemoverCreateForGameContext(
            &*_state
            , &*_GAME_CONTEXT
            , _NAME
            , reinterpret_cast< FgSaveRemoveNotifyProc >( _notifyProcPtr )
        );
    }

    inline void SaveRemover::destroy(
        SaveRemover *   _this
    )
    {
        fgSaveRemoverDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_REMOVER_H
