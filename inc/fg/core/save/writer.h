﻿#ifndef FG_CORE_SAVE_WRITER_H
#define FG_CORE_SAVE_WRITER_H

#include "fg/def/core/save/writer.h"
#include "fg/def/core/save/save.h"
#include "fg/def/core/save/writenotify.h"
#include "fg/def/core/state/state.h"
#include "fg/def/core/module/context.h"
#include "fg/def/core/basesystem/context.h"
#include "fg/def/core/game/context.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_BOOL(
        fgSaveWriterWrite(
            const FgModuleContext *
            , const char *
            , const FgSave *
        )
    )

    FG_FUNCTION_PTR(
        FgSaveWriter
        , fgSaveWriterCreateForModuleContext(
            FgState *
            , const FgModuleContext *
            , const char *
            , const FgSave *
            , FgSaveWriteNotifyProc
        )
    )

    FG_FUNCTION_PTR(
        FgSaveWriter
        , fgSaveWriterCreateForBasesystemContext(
            FgState *
            , const FgBasesystemContext *
            , const char *
            , const FgSave *
            , FgSaveWriteNotifyProc
        )
    )

    FG_FUNCTION_PTR(
        FgSaveWriter
        , fgSaveWriterCreateForGameContext(
            FgState *
            , const FgGameContext *
            , const char *
            , const FgSave *
            , FgSaveWriteNotifyProc
        )
    )

    FG_FUNCTION_VOID(
        fgSaveWriterDestroy(
            FgSaveWriter *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline bool SaveWriter::write(
        const ModuleContext &   _MODULE_CONTEXT
        , const char *          _NAME
        , const Save &          _SAVE
    )
    {
        return fgSaveWriterWrite(
            &*_MODULE_CONTEXT
            , _NAME
            , &*_SAVE
        );
    }

    template< typename STATE_DATA_T >
    SaveWriter::Unique SaveWriter::create(
        State< STATE_DATA_T > &                             _state
        , const ModuleContext &                             _MODULE_CONTEXT
        , const char *                                      _NAME
        , const Save &                                      _SAVE
        , typename SaveWriteNotify< STATE_DATA_T >::Proc    _notifyProcPtr
    )
    {
        return fgSaveWriterCreateForModuleContext(
            &*_state
            , &*_MODULE_CONTEXT
            , _NAME
            , &*_SAVE
            , reinterpret_cast< FgSaveWriteNotifyProc >( _notifyProcPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename BASESYSTEM_CONTEXT_STATE_DATA_T
    >
    SaveWriter::Unique SaveWriter::create(
        State< STATE_DATA_T > &                                         _state
        , const BasesystemContext< BASESYSTEM_CONTEXT_STATE_DATA_T > &  _BASESYSTEM_CONTEXT
        , const char *                                                  _NAME
        , const Save &                                                  _SAVE
        , typename SaveWriteNotify< STATE_DATA_T >::Proc                _notifyProcPtr
    )
    {
        return fgSaveWriterCreateForBasesystemContext(
            &*_state
            , &*_BASESYSTEM_CONTEXT
            , _NAME
            , &*_SAVE
            , reinterpret_cast< FgSaveWriteNotifyProc >( _notifyProcPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename GAME_CONTEXT_STATE_DATA_T
    >
    SaveWriter::Unique SaveWriter::create(
        State< STATE_DATA_T > &                             _state
        , const GameContext< GAME_CONTEXT_STATE_DATA_T > &  _GAME_CONTEXT
        , const char *                                      _NAME
        , const Save &                                      _SAVE
        , typename SaveWriteNotify< STATE_DATA_T >::Proc    _notifyProcPtr
    )
    {
        return fgSaveWriterCreateForGameContext(
            &*_state
            , &*_GAME_CONTEXT
            , _NAME
            , &*_SAVE
            , reinterpret_cast< FgSaveWriteNotifyProc >( _notifyProcPtr )
        );
    }

    inline void SaveWriter::destroy(
        SaveWriter *    _this
    )
    {
        fgSaveWriterDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_WRITER_H
