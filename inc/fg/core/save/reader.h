﻿#ifndef FG_CORE_SAVE_READER_H
#define FG_CORE_SAVE_READER_H

#include "fg/def/core/save/reader.h"
#include "fg/def/core/save/readnotify.h"
#include "fg/def/core/state/state.h"
#include "fg/def/core/module/context.h"
#include "fg/def/core/basesystem/context.h"
#include "fg/def/core/game/context.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgSave
        , fgSaveReaderRead(
            const FgModuleContext *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        FgSaveReader
        , fgSaveReaderCreateForModuleContext(
            FgState *
            , const FgModuleContext *
            , const char *
            , FgSaveReadNotifyProc
        )
    )

    FG_FUNCTION_PTR(
        FgSaveReader
        , fgSaveReaderCreateForBasesystemContext(
            FgState *
            , const FgBasesystemContext *
            , const char *
            , FgSaveReadNotifyProc
        )
    )

    FG_FUNCTION_PTR(
        FgSaveReader
        , fgSaveReaderCreateForGameContext(
            FgState *
            , const FgGameContext *
            , const char *
            , FgSaveReadNotifyProc
        )
    )

    FG_FUNCTION_VOID(
        fgSaveReaderDestroy(
            FgSaveReader *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline Save::ConstUnique SaveReader::read(
        const ModuleContext &   _MODULE_CONTEXT
        , const char *          _NAME
    )
    {
        return fgSaveReaderRead(
            &*_MODULE_CONTEXT
            , _NAME
        );
    }

    template< typename STATE_DATA_T >
    SaveReader::Unique SaveReader::create(
        State< STATE_DATA_T > &                         _state
        , const ModuleContext &                         _MODULE_CONTEXT
        , const char *                                  _NAME
        , typename SaveReadNotify< STATE_DATA_T >::Proc _notifyProcPtr
    )
    {
        return fgSaveReaderCreateForModuleContext(
            &*_state
            , &*_MODULE_CONTEXT
            , _NAME
            , reinterpret_cast< FgSaveReadNotifyProc >( _notifyProcPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename BASESYSTEM_CONTEXT_STATE_DATA_T
    >
    SaveReader::Unique SaveReader::create(
        State< STATE_DATA_T > &                                         _state
        , const BasesystemContext< BASESYSTEM_CONTEXT_STATE_DATA_T > &  _BASESYSTEM_CONTEXT
        , const char *                                                  _NAME
        , typename SaveReadNotify< STATE_DATA_T >::Proc                 _notifyProcPtr
    )
    {
        return fgSaveReaderCreateForBasesystemContext(
            &*_state
            , &*_BASESYSTEM_CONTEXT
            , _NAME
            , reinterpret_cast< FgSaveReadNotifyProc >( _notifyProcPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename GAME_CONTEXT_STATE_DATA_T
    >
    SaveReader::Unique SaveReader::create(
        State< STATE_DATA_T > &                             _state
        , const GameContext< GAME_CONTEXT_STATE_DATA_T > &  _GAME_CONTEXT
        , const char *                                      _NAME
        , typename SaveReadNotify< STATE_DATA_T >::Proc     _notifyProcPtr
    )
    {
        return fgSaveReaderCreateForGameContext(
            &*_state
            , &*_GAME_CONTEXT
            , _NAME
            , reinterpret_cast< FgSaveReadNotifyProc >( _notifyProcPtr )
        );
    }

    inline void SaveReader::destroy(
        SaveReader *    _this
    )
    {
        fgSaveReaderDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_SAVE_READER_H
