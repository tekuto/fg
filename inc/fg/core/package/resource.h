﻿#ifndef FG_CORE_PACKAGE_RESOURCE_H
#define FG_CORE_PACKAGE_RESOURCE_H

#include "fg/def/core/package/resource.h"
#include "fg/def/core/module/context.h"
#include "fg/def/core/basesystem/context.h"
#include "fg/def/core/game/context.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgPackageResource
        , fgPackageResourceCreateForModuleContext(
            const FgModuleContext *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        FgPackageResource
        , fgPackageResourceCreateForBasesystemContext(
            const FgBasesystemContext *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        FgPackageResource
        , fgPackageResourceCreateForGameContext(
            const FgGameContext *
            , const char *
        )
    )

    FG_FUNCTION_VOID(
        fgPackageResourceDestroy(
            FgPackageResource *
        )
    )

    FG_FUNCTION_NUM(
        size_t
        , fgPackageResourceGetSize(
            const FgPackageResource *
        )
    )

    FG_FUNCTION_PTR(
        const void
        , fgPackageResourceGetData(
            const FgPackageResource *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline PackageResource::ConstUnique PackageResource::create(
        const ModuleContext &   _MODULE_CONTEXT
        , const char *          _FILE_NAME
    )
    {
        return fgPackageResourceCreateForModuleContext(
            &*_MODULE_CONTEXT
            , _FILE_NAME
        );
    }

    template< typename STATE_DATA_T >
    PackageResource::ConstUnique PackageResource::create(
        const BasesystemContext< STATE_DATA_T > &   _BASESYSTEM_CONTEXT
        , const char *                              _FILE_NAME
    )
    {
        return fgPackageResourceCreateForBasesystemContext(
            &*_BASESYSTEM_CONTEXT
            , _FILE_NAME
        );
    }

    template< typename STATE_DATA_T >
    PackageResource::ConstUnique PackageResource::create(
        const GameContext< STATE_DATA_T > & _GAME_CONTEXT
        , const char *                      _FILE_NAME
    )
    {
        return fgPackageResourceCreateForGameContext(
            &*_GAME_CONTEXT
            , _FILE_NAME
        );
    }

    inline void PackageResource::destroy(
        PackageResource *   _this
    )
    {
        fgPackageResourceDestroy( &**_this );
    }

    inline std::size_t PackageResource::getSize(
    ) const
    {
        return fgPackageResourceGetSize( &**this );
    }

    inline const void * PackageResource::getData(
    ) const
    {
        return fgPackageResourceGetData( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_PACKAGE_RESOURCE_H
