﻿#ifndef FG_CORE_DATAELEMENT_LIST_H
#define FG_CORE_DATAELEMENT_LIST_H

#include "fg/def/core/dataelement/list.h"
#include "fg/def/core/dataelement/dataelement.h"
#include "fg/def/core/dataelement/string.h"
#include "fg/def/core/dataelement/map/map.h"
#include "fg/def/common/unique.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgDataElementList
        , fgDataElementListCreate(
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListDestroy(
            FgDataElementList *
        )
    )

    FG_FUNCTION_NUM(
        size_t
        , fgDataElementListGetSize(
            const FgDataElementList *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElement
        , fgDataElementListGetDataElement(
            const FgDataElementList *
            , size_t
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementString
        , fgDataElementListGetDataElementString(
            const FgDataElementList *
            , size_t
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementList
        , fgDataElementListGetDataElementList(
            const FgDataElementList *
            , size_t
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementMap
        , fgDataElementListGetDataElementMap(
            const FgDataElementList *
            , size_t
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListAddDataElementString(
            FgDataElementList *
            , const FgDataElementString *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListAddMoveDataElementString(
            FgDataElementList *
            , const FgDataElementString *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListAddDataElementList(
            FgDataElementList *
            , const FgDataElementList *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListAddMoveDataElementList(
            FgDataElementList *
            , const FgDataElementList *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListAddDataElementMap(
            FgDataElementList *
            , const FgDataElementMap *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementListAddMoveDataElementMap(
            FgDataElementList *
            , const FgDataElementMap *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline DataElementList::Unique DataElementList::create(
    )
    {
        return fgDataElementListCreate();
    }

    inline void DataElementList::destroy(
        DataElementList *   _this
    )
    {
        fgDataElementListDestroy( &**_this );
    }

    inline std::size_t DataElementList::getSize(
    ) const
    {
        return fgDataElementListGetSize( &**this );
    }

    inline const DataElement & DataElementList::getDataElement(
        std::size_t _index
    ) const
    {
        return *reinterpret_cast< const DataElement * >(
            fgDataElementListGetDataElement(
                &**this
                , _index
            )
        );
    }

    inline const DataElementString * DataElementList::getDataElementString(
        std::size_t _index
    ) const
    {
        return reinterpret_cast< const DataElementString * >(
            fgDataElementListGetDataElementString(
                &**this
                , _index
            )
        );
    }

    inline const DataElementList * DataElementList::getDataElementList(
        std::size_t _index
    ) const
    {
        return reinterpret_cast< const DataElementList * >(
            fgDataElementListGetDataElementList(
                &**this
                , _index
            )
        );
    }

    inline const DataElementMap * DataElementList::getDataElementMap(
        std::size_t _index
    ) const
    {
        return reinterpret_cast< const DataElementMap * >(
            fgDataElementListGetDataElementMap(
                &**this
                , _index
            )
        );
    }

    inline void DataElementList::addDataElement(
        const DataElementString &   _STRING
    )
    {
        fgDataElementListAddDataElementString(
            &**this
            , &*_STRING
        );
    }

    inline void DataElementList::addDataElement(
        fg::ConstUnique< DataElementString > && _stringUnique
    )
    {
        fgDataElementListAddMoveDataElementString(
            &**this
            , &**( _stringUnique.release() )
        );
    }

    inline void DataElementList::addDataElement(
        const DataElementList & _LIST
    )
    {
        fgDataElementListAddDataElementList(
            &**this
            , &*_LIST
        );
    }

    inline void DataElementList::addDataElement(
        fg::ConstUnique< DataElementList > &&   _listUnique
    )
    {
        fgDataElementListAddMoveDataElementList(
            &**this
            , &**( _listUnique.release() )
        );
    }

    inline void DataElementList::addDataElement(
        const DataElementMap &  _MAP
    )
    {
        fgDataElementListAddDataElementMap(
            &**this
            , &*_MAP
        );
    }

    inline void DataElementList::addDataElement(
        fg::ConstUnique< DataElementMap > &&    _mapUnique
    )
    {
        fgDataElementListAddMoveDataElementMap(
            &**this
            , &**( _mapUnique.release() )
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_DATAELEMENT_LIST_H
