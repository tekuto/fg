﻿#ifndef FG_CORE_DATAELEMENT_DATAELEMENT_H
#define FG_CORE_DATAELEMENT_DATAELEMENT_H

#include "fg/def/core/dataelement/dataelement.h"
#include "fg/def/core/dataelement/string.h"
#include "fg/def/core/dataelement/list.h"
#include "fg/def/core/dataelement/map/map.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const FgDataElementString
        , fgDataElementGetDataElementString(
            const FgDataElement *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementList
        , fgDataElementGetDataElementList(
            const FgDataElement *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementMap
        , fgDataElementGetDataElementMap(
            const FgDataElement *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const DataElementString * DataElement::getDataElementString(
    ) const
    {
        return reinterpret_cast< const DataElementString * >( fgDataElementGetDataElementString( &**this ) );
    }

    inline const DataElementList * DataElement::getDataElementList(
    ) const
    {
        return reinterpret_cast< const DataElementList * >( fgDataElementGetDataElementList( &**this ) );
    }

    inline const DataElementMap * DataElement::getDataElementMap(
    ) const
    {
        return reinterpret_cast< const DataElementMap * >( fgDataElementGetDataElementMap( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_DATAELEMENT_DATAELEMENT_H
