﻿#ifndef FG_CORE_DATAELEMENT_MAP_KEYS_H
#define FG_CORE_DATAELEMENT_MAP_KEYS_H

#include "fg/def/core/dataelement/map/keys.h"
#include "fg/def/core/dataelement/map/map.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgDataElementMapKeys
        , fgDataElementMapKeysCreate(
            const FgDataElementMap *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapKeysDestroy(
            FgDataElementMapKeys *
        )
    )

    FG_FUNCTION_NUM(
        size_t
        , fgDataElementMapKeysGetSize(
            const FgDataElementMapKeys *
        )
    )

    FG_FUNCTION_PTR(
        const char
        , fgDataElementMapKeysGetKey(
            const FgDataElementMapKeys *
            , size_t
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline DataElementMapKeys::ConstUnique DataElementMapKeys::create(
        const DataElementMap &  _MAP
    )
    {
        return fgDataElementMapKeysCreate( &*_MAP );
    }

    inline void DataElementMapKeys::destroy(
        DataElementMapKeys *    _this
    )
    {
        fgDataElementMapKeysDestroy( &**_this );
    }

    inline std::size_t DataElementMapKeys::getSize(
    ) const
    {
        return fgDataElementMapKeysGetSize( &**this );
    }

    inline const char * DataElementMapKeys::getKey(
        std::size_t _index
    ) const
    {
        return fgDataElementMapKeysGetKey(
            &**this
            , _index
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_DATAELEMENT_MAP_KEYS_H
