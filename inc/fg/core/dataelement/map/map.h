﻿#ifndef FG_CORE_DATAELEMENT_MAP_MAP_H
#define FG_CORE_DATAELEMENT_MAP_MAP_H

#include "fg/def/core/dataelement/map/map.h"
#include "fg/def/core/dataelement/dataelement.h"
#include "fg/def/core/dataelement/string.h"
#include "fg/def/core/dataelement/list.h"
#include "fg/def/common/unique.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgDataElementMap
        , fgDataElementMapCreate(
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapDestroy(
            FgDataElementMap *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElement
        , fgDataElementMapGetDataElement(
            const FgDataElementMap *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementString
        , fgDataElementMapGetDataElementString(
            const FgDataElementMap *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementList
        , fgDataElementMapGetDataElementList(
            const FgDataElementMap *
            , const char *
        )
    )

    FG_FUNCTION_PTR(
        const FgDataElementMap
        , fgDataElementMapGetDataElementMap(
            const FgDataElementMap *
            , const char *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapAddDataElementString(
            FgDataElementMap *
            , const char *
            , const FgDataElementString *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapAddMoveDataElementString(
            FgDataElementMap *
            , const char *
            , const FgDataElementString *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapAddDataElementList(
            FgDataElementMap *
            , const char *
            , const FgDataElementList *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapAddMoveDataElementList(
            FgDataElementMap *
            , const char *
            , const FgDataElementList *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapAddDataElementMap(
            FgDataElementMap *
            , const char *
            , const FgDataElementMap *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementMapAddMoveDataElementMap(
            FgDataElementMap *
            , const char *
            , const FgDataElementMap *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline DataElementMap::Unique DataElementMap::create(
    )
    {
        return fgDataElementMapCreate();
    }

    inline void DataElementMap::destroy(
        DataElementMap *    _this
    )
    {
        fgDataElementMapDestroy( &**_this );
    }

    inline const DataElement * DataElementMap::getDataElement(
        const char *    _KEY
    ) const
    {
        return reinterpret_cast< const DataElement * >(
            fgDataElementMapGetDataElement(
                &**this
                , _KEY
            )
        );
    }

    inline const DataElementString * DataElementMap::getDataElementString(
        const char *    _KEY
    ) const
    {
        return reinterpret_cast< const DataElementString * >(
            fgDataElementMapGetDataElementString(
                &**this
                , _KEY
            )
        );
    }

    inline const DataElementList * DataElementMap::getDataElementList(
        const char *    _KEY
    ) const
    {
        return reinterpret_cast< const DataElementList * >(
            fgDataElementMapGetDataElementList(
                &**this
                , _KEY
            )
        );
    }

    inline const DataElementMap * DataElementMap::getDataElementMap(
        const char *    _KEY
    ) const
    {
        return reinterpret_cast< const DataElementMap * >(
            fgDataElementMapGetDataElementMap(
                &**this
                , _KEY
            )
        );
    }

    inline void DataElementMap::addDataElement(
        const char *                _KEY
        , const DataElementString & _STRING
    )
    {
        fgDataElementMapAddDataElementString(
            &**this
            , _KEY
            , &*_STRING
        );
    }

    inline void DataElementMap::addDataElement(
        const char *                                _KEY
        , fg::ConstUnique< DataElementString > &&   _stringUnique
    )
    {
        fgDataElementMapAddMoveDataElementString(
            &**this
            , _KEY
            , &**( _stringUnique.release() )
        );
    }

    inline void DataElementMap::addDataElement(
        const char *                _KEY
        , const DataElementList &   _LIST
    )
    {
        fgDataElementMapAddDataElementList(
            &**this
            , _KEY
            , &*_LIST
        );
    }

    inline void DataElementMap::addDataElement(
        const char *                            _KEY
        , fg::ConstUnique< DataElementList > && _listUnique
    )
    {
        fgDataElementMapAddMoveDataElementList(
            &**this
            , _KEY
            , &**( _listUnique.release() )
        );
    }

    inline void DataElementMap::addDataElement(
        const char *                _KEY
        , const DataElementMap &    _MAP
    )
    {
        fgDataElementMapAddDataElementMap(
            &**this
            , _KEY
            , &*_MAP
        );
    }

    inline void DataElementMap::addDataElement(
        const char *                            _KEY
        , fg::ConstUnique< DataElementMap > &&  _mapUnique
    )
    {
        fgDataElementMapAddMoveDataElementMap(
            &**this
            , _KEY
            , &**( _mapUnique.release() )
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_DATAELEMENT_MAP_MAP_H
