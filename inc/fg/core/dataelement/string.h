﻿#ifndef FG_CORE_DATAELEMENT_STRING_H
#define FG_CORE_DATAELEMENT_STRING_H

#include "fg/def/core/dataelement/string.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgDataElementString
        , fgDataElementStringCreate(
            const char *
        )
    )

    FG_FUNCTION_VOID(
        fgDataElementStringDestroy(
            FgDataElementString *
        )
    )

    FG_FUNCTION_PTR(
        const char
        , fgDataElementStringGetString(
            const FgDataElementString *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline DataElementString::ConstUnique DataElementString::create(
        const char *    _STRING
    )
    {
        return fgDataElementStringCreate( _STRING );
    }

    inline void DataElementString::destroy(
        DataElementString * _this
    )
    {
        fgDataElementStringDestroy( &**_this );
    }

    inline const char * DataElementString::getString(
    ) const
    {
        return fgDataElementStringGetString( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_DATAELEMENT_STRING_H
