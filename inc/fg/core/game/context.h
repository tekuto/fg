﻿#ifndef FG_CORE_GAME_CONTEXT_H
#define FG_CORE_GAME_CONTEXT_H

#include "fg/def/core/game/context.h"
#include "fg/def/core/state/creating.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_VOID(
        fgGameContextSetStateData(
            FgGameContext *
            , void *
            , FgGameContextStateDataDestroyProc
        )
    )

    FG_FUNCTION_PTR(
        FgCreatingState
        , fgGameContextGetCreatingState(
            FgGameContext *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    void GameContext< STATE_DATA_T >::setStateData(
        STATE_DATA_T *          _dataPtr
        , StateDataDestroyProc  _procPtr
    )
    {
        fgGameContextSetStateData(
            &**this
            , _dataPtr
            , reinterpret_cast< FgGameContextStateDataDestroyProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    CreatingState< STATE_DATA_T > & GameContext< STATE_DATA_T >::getCreatingState(
    )
    {
        return reinterpret_cast< CreatingState< STATE_DATA_T > & >( *fgGameContextGetCreatingState( &**this ) );
    }

    inline CreatingState<> & GameContext<>::getCreatingState(
    )
    {
        return reinterpret_cast< CreatingState<> & >( *fgGameContextGetCreatingState( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_GAME_CONTEXT_H
