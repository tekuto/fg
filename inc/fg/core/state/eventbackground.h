﻿#ifndef FG_CORE_STATE_EVENTBACKGROUND_H
#define FG_CORE_STATE_EVENTBACKGROUND_H

#include "fg/def/core/state/eventbackground.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgStateEventBackgroundGetState(
            FgStateEventBackground *
        )
    )

    FG_FUNCTION_PTR(
        void
        , fgStateEventBackgroundGetData(
            FgStateEventBackground *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template<
        typename STATE_DATA_T
        , typename EVENT_DATA_T
    >
    State< STATE_DATA_T > & StateEventBackground< STATE_DATA_T, EVENT_DATA_T >::getState(
    )
    {
        return *reinterpret_cast< State< STATE_DATA_T > * >( fgStateEventBackgroundGetState( &**this ) );
    }

    template<
        typename STATE_DATA_T
        , typename EVENT_DATA_T
    >
    EVENT_DATA_T & StateEventBackground< STATE_DATA_T, EVENT_DATA_T >::getData(
    )
    {
        return *static_cast< EVENT_DATA_T * >( fgStateEventBackgroundGetData( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_EVENTBACKGROUND_H
