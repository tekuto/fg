﻿#ifndef FG_CORE_STATE_REENTEREVENT_H
#define FG_CORE_STATE_REENTEREVENT_H

#include "fg/core/state/event.h"
#include "fg/def/core/state/reenterevent.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const void
        , fgStateReenterEventDataGetPrevDataDestroyProcPtr(
            const FgStateReenterEventData *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const void * StateReenterEventData::getPrevDataDestroyProcPtr(
    ) const
    {
        return fgStateReenterEventDataGetPrevDataDestroyProcPtr( &**this );
    }

    template< typename STATE_DATA_T >
    bool StateReenterEventData::isPrevState(
    ) const
    {
        return this->getPrevDataDestroyProcPtr() == STATE_DATA_T::destroy;
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_REENTEREVENT_H
