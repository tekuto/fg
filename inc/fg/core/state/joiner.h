﻿#ifndef FG_CORE_STATE_JOINER_H
#define FG_CORE_STATE_JOINER_H

#include "fg/def/core/state/joiner.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgStateJoiner
        , fgStateJoinerCreate(
        )
    )

    FG_FUNCTION_VOID(
        fgStateJoinerDestroy(
            FgStateJoiner *
        )
    )

    FG_FUNCTION_VOID(
        fgStateJoinerJoin(
            FgStateJoiner *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline StateJoiner::Unique StateJoiner::create(
    )
    {
        return fgStateJoinerCreate();
    }

    inline void StateJoiner::destroy(
        StateJoiner *   _this
    )
    {
        fgStateJoinerDestroy( &**_this );
    }

    inline void StateJoiner::join(
    )
    {
        fgStateJoinerJoin( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_JOINER_H
