﻿#ifndef FG_CORE_STATE_EVENTMANAGER_H
#define FG_CORE_STATE_EVENTMANAGER_H

#include "fg/def/core/state/eventmanager.h"
#include "fg/def/core/state/joiner.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgStateEventManager
        , fgStateEventManagerCreate(
        )
    )

    FG_FUNCTION_VOID(
        fgStateEventManagerDestroy(
            FgStateEventManager *
        )
    )

    FG_FUNCTION_VOID(
        fgStateEventManagerExecute(
            FgStateEventManager *
            , FgStateJoiner *
            , void *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename EVENT_DATA_T >
    typename StateEventManager< EVENT_DATA_T >::Unique StateEventManager< EVENT_DATA_T >::create(
    )
    {
        return fgStateEventManagerCreate();
    }

    template< typename EVENT_DATA_T >
    void StateEventManager< EVENT_DATA_T >::destroy(
        StateEventManager< EVENT_DATA_T > * _this
    )
    {
        fgStateEventManagerDestroy( &**_this );
    }

    template< typename EVENT_DATA_T >
    void StateEventManager< EVENT_DATA_T >::execute(
        StateJoiner &       _joiner
        , EVENT_DATA_T &    _eventData
    )
    {
        fgStateEventManagerExecute(
            &**this
            , &*_joiner
            , &_eventData
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_EVENTMANAGER_H
