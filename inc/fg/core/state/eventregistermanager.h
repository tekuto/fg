﻿#ifndef FG_CORE_STATE_EVENTREGISTERMANAGER_H
#define FG_CORE_STATE_EVENTREGISTERMANAGER_H

#include "fg/def/core/state/eventregistermanager.h"
#include "fg/def/core/state/eventmanager.h"
#include "fg/def/core/state/creating.h"
#include "fg/def/core/state/state.h"
#include "fg/def/core/state/event.h"
#include "fg/def/core/state/eventbackground.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgStateEventRegisterManager
        , fgStateEventRegisterManagerCreateForCreatingState(
            FgStateEventManager *
            , FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_PTR(
        FgStateEventRegisterManager
        , fgStateEventRegisterManagerCreateForState(
            FgStateEventManager *
            , FgState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_PTR(
        FgStateEventRegisterManager
        , fgStateEventRegisterManagerCreateBackgroundForCreatingState(
            FgStateEventManager *
            , FgCreatingState *
            , FgStateEventBackgroundProc
        )
    )

    FG_FUNCTION_PTR(
        FgStateEventRegisterManager
        , fgStateEventRegisterManagerCreateBackgroundForState(
            FgStateEventManager *
            , FgState *
            , FgStateEventBackgroundProc
        )
    )

    FG_FUNCTION_VOID(
        fgStateEventRegisterManagerDestroy(
            FgStateEventRegisterManager *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename EVENT_DATA_T >
    template< typename STATE_DATA_T >
    typename StateEventRegisterManager< EVENT_DATA_T >::Unique StateEventRegisterManager< EVENT_DATA_T >::create(
        StateEventManager< EVENT_DATA_T > &                         _eventManager
        , CreatingState< STATE_DATA_T > &                           _state
        , typename StateEvent< STATE_DATA_T, EVENT_DATA_T >::Proc   _procPtr
    )
    {
        return fgStateEventRegisterManagerCreateForCreatingState(
            &*_eventManager
            , &*_state
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename EVENT_DATA_T >
    template< typename STATE_DATA_T >
    typename StateEventRegisterManager< EVENT_DATA_T >::Unique StateEventRegisterManager< EVENT_DATA_T >::create(
        StateEventManager< EVENT_DATA_T > &                         _eventManager
        , State< STATE_DATA_T > &                                   _state
        , typename StateEvent< STATE_DATA_T, EVENT_DATA_T >::Proc   _procPtr
    )
    {
        return fgStateEventRegisterManagerCreateForState(
            &*_eventManager
            , &*_state
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename EVENT_DATA_T >
    template< typename STATE_DATA_T >
    typename StateEventRegisterManager< EVENT_DATA_T >::Unique StateEventRegisterManager< EVENT_DATA_T >::create(
        StateEventManager< EVENT_DATA_T > &                                 _eventManager
        , CreatingState< STATE_DATA_T > &                                   _state
        , typename StateEventBackground< STATE_DATA_T, EVENT_DATA_T >::Proc _procPtr
    )
    {
        return fgStateEventRegisterManagerCreateBackgroundForCreatingState(
            &*_eventManager
            , &*_state
            , reinterpret_cast< FgStateEventBackgroundProc >( _procPtr )
        );
    }

    template< typename EVENT_DATA_T >
    template< typename STATE_DATA_T >
    typename StateEventRegisterManager< EVENT_DATA_T >::Unique StateEventRegisterManager< EVENT_DATA_T >::create(
        StateEventManager< EVENT_DATA_T > &                                 _eventManager
        , State< STATE_DATA_T > &                                           _state
        , typename StateEventBackground< STATE_DATA_T, EVENT_DATA_T >::Proc _procPtr
    )
    {
        return fgStateEventRegisterManagerCreateBackgroundForState(
            &*_eventManager
            , &*_state
            , reinterpret_cast< FgStateEventBackgroundProc >( _procPtr )
        );
    }

    template< typename EVENT_DATA_T >
    void StateEventRegisterManager< EVENT_DATA_T >::destroy(
        StateEventRegisterManager< EVENT_DATA_T > * _this
    )
    {
        fgStateEventRegisterManagerDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_EVENTREGISTERMANAGER_H
