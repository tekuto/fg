﻿#ifndef FG_CORE_STATE_THREAD_H
#define FG_CORE_STATE_THREAD_H

#include "fg/def/core/state/thread.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgStateThreadGetState(
            FgStateThread *
        )
    )

    FG_FUNCTION_PTR(
        void
        , fgStateThreadGetData(
            FgStateThread *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    State< STATE_DATA_T > & StateThread< STATE_DATA_T, USER_DATA_T >::getState(
    )
    {
        return *reinterpret_cast< State< STATE_DATA_T > * >( fgStateThreadGetState( &**this ) );
    }

    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    USER_DATA_T & StateThread< STATE_DATA_T, USER_DATA_T >::getData(
    )
    {
        return *static_cast< USER_DATA_T * >( fgStateThreadGetData( &**this ) );
    }

    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & StateThread< STATE_DATA_T, void >::getState(
    )
    {
        return *reinterpret_cast< State< STATE_DATA_T > * >( fgStateThreadGetState( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_THREAD_H
