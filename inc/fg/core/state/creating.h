﻿#ifndef FG_CORE_STATE_CREATING_H
#define FG_CORE_STATE_CREATING_H

#include "fg/def/core/state/creating.h"
#include "fg/def/core/state/state.h"
#include "fg/def/core/state/event.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgCreatingState
        , fgCreatingStateCreate(
            FgState *
        )
    )

    FG_FUNCTION_VOID(
        fgCreatingStateDestroy(
            FgCreatingState *
        )
    )

    FG_FUNCTION_VOID(
        fgCreatingStateSetData(
            FgCreatingState *
            , void *
            , FgCreatingStateDataDestroyProc
        )
    )

    FG_FUNCTION_VOID(
        fgCreatingStateSetEnterEventProc(
            FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_VOID(
        fgCreatingStateSetExitEventProc(
            FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_VOID(
        fgCreatingStateSetReenterEventProc(
            FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_VOID(
        fgCreatingStateSetLeaveEventProc(
            FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_PTR(
        void
        , fgCreatingStateGetBasesystem(
            FgCreatingState *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    template< typename PREV_STATE_DATA_T >
    typename CreatingState< STATE_DATA_T >::Unique CreatingState< STATE_DATA_T >::create(
        State< PREV_STATE_DATA_T > &    _prevState
        , const DataCreateProc &        _DATA_CREATE_PROC
        , DataDestroyProc               _dataDestroyProcPtr
    )
    {
        auto    thisUnique = typename CreatingState::Unique( fgCreatingStateCreate( &*_prevState ) );

        thisUnique->setData(
            _DATA_CREATE_PROC( *thisUnique )
            , _dataDestroyProcPtr
        );

        return thisUnique;
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::destroy(
        CreatingState * _this
    )
    {
        fgCreatingStateDestroy( &**_this );
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::setData(
        STATE_DATA_T *      _dataPtr
        , DataDestroyProc   _dataDestroyProcPtr
    )
    {
        fgCreatingStateSetData(
            &**this
            , _dataPtr
            , reinterpret_cast< FgCreatingStateDataDestroyProc >( _dataDestroyProcPtr )
        );
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::setEventProcs(
    )
    {
    }

    template< typename STATE_DATA_T >
    template<
        typename EVENT_PROC_T
        , typename ... EVENT_PROCS_T
    >
    void CreatingState< STATE_DATA_T >::setEventProcs(
        EVENT_PROC_T        _procPtr
        , EVENT_PROCS_T ... _procPtrs
    )
    {
        this->setEventProc( _procPtr );

        this->setEventProcs( _procPtrs ... );
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::setEventProc(
        typename StateEnterEvent< STATE_DATA_T >::Proc  _procPtr
    )
    {
        fgCreatingStateSetEnterEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::setEventProc(
        typename StateExitEvent< STATE_DATA_T >::Proc   _procPtr
    )
    {
        fgCreatingStateSetExitEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::setEventProc(
        typename StateReenterEvent< STATE_DATA_T >::Proc    _procPtr
    )
    {
        fgCreatingStateSetReenterEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void CreatingState< STATE_DATA_T >::setEventProc(
        typename StateLeaveEvent< STATE_DATA_T >::Proc  _procPtr
    )
    {
        fgCreatingStateSetLeaveEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    template< typename BASESYSTEM_T >
    BASESYSTEM_T & CreatingState< STATE_DATA_T >::getBasesystem_(
    )
    {
        return *static_cast< BASESYSTEM_T * >( fgCreatingStateGetBasesystem( &**this ) );
    }

    template< typename PREV_STATE_DATA_T >
    CreatingState<>::Unique CreatingState<>::create(
        State< PREV_STATE_DATA_T > &    _prevState
        , const InitializeProc &        _INITIALIZE_PROC
    )
    {
        auto    thisUnique = Unique( fgCreatingStateCreate( &*_prevState ) );

        _INITIALIZE_PROC( *thisUnique );

        return thisUnique;
    }

    inline void CreatingState<>::destroy(
        CreatingState * _this
    )
    {
        fgCreatingStateDestroy( &**_this );
    }

    inline void CreatingState<>::setEventProcs(
    )
    {
    }

    template<
        typename EVENT_PROC_T
        , typename ... EVENT_PROCS_T
    >
    void CreatingState<>::setEventProcs(
        EVENT_PROC_T        _procPtr
        , EVENT_PROCS_T ... _procPtrs
    )
    {
        this->setEventProc( _procPtr );

        this->setEventProcs( _procPtrs ... );
    }

    inline void CreatingState<>::setEventProc(
        typename StateEnterEvent<>::Proc    _procPtr
    )
    {
        fgCreatingStateSetEnterEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    inline void CreatingState<>::setEventProc(
        typename StateExitEvent<>::Proc _procPtr
    )
    {
        fgCreatingStateSetExitEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    inline void CreatingState<>::setEventProc(
        typename StateReenterEvent<>::Proc  _procPtr
    )
    {
        fgCreatingStateSetReenterEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    inline void CreatingState<>::setEventProc(
        typename StateLeaveEvent<>::Proc    _procPtr
    )
    {
        fgCreatingStateSetLeaveEventProc(
            &**this
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename BASESYSTEM_T >
    BASESYSTEM_T & CreatingState<>::getBasesystem_(
    )
    {
        return *static_cast< BASESYSTEM_T * >( fgCreatingStateGetBasesystem( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_CREATING_H
