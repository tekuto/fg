﻿#ifndef FG_CORE_STATE_STATE_H
#define FG_CORE_STATE_STATE_H

#include "fg/def/core/state/state.h"
#include "fg/def/core/state/thread.h"
#include "fg/def/core/state/threadsingleton.h"
#include "fg/def/core/state/threadbackground.h"
#include "fg/def/core/state/threadbackgroundsingleton.h"
#include "fg/def/core/state/joiner.h"
#include "fg/def/core/state/creating.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        void
        , fgStateGetData(
            FgState *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecute(
            FgState *
            , FgStateThreadProc
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteWithJoiner(
            FgState *
            , FgStateThreadProc
            , FgStateJoiner *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteWithJoinerAndUserData(
            FgState *
            , FgStateThreadProc
            , FgStateJoiner *
            , void *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteSingleton(
            FgState *
            , FgStateThreadSingletonProc
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteSingletonWithJoiner(
            FgState *
            , FgStateThreadSingletonProc
            , FgStateJoiner *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteSingletonWithJoinerAndUserData(
            FgState *
            , FgStateThreadSingletonProc
            , FgStateJoiner *
            , void *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteBackground(
            FgState *
            , FgStateThreadBackgroundProc
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteBackgroundWithJoiner(
            FgState *
            , FgStateThreadBackgroundProc
            , FgStateJoiner *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteBackgroundWithJoinerAndUserData(
            FgState *
            , FgStateThreadBackgroundProc
            , FgStateJoiner *
            , void *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteBackgroundSingleton(
            FgState *
            , FgStateThreadBackgroundSingletonProc
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteBackgroundSingletonWithJoiner(
            FgState *
            , FgStateThreadBackgroundSingletonProc
            , FgStateJoiner *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExecuteBackgroundSingletonWithJoinerAndUserData(
            FgState *
            , FgStateThreadBackgroundSingletonProc
            , FgStateJoiner *
            , void *
        )
    )

    FG_FUNCTION_VOID(
        fgStateEnter(
            FgState *
            , FgCreatingState *
        )
    )

    FG_FUNCTION_VOID(
        fgStateExit(
            FgState *
        )
    )

    FG_FUNCTION_PTR(
        void
        , fgStateGetBasesystem(
            FgState *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    STATE_DATA_T & State< STATE_DATA_T >::getData(
    )
    {
        return *static_cast< STATE_DATA_T * >( fgStateGetData( &**this ) );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThread< STATE_DATA_T >::Proc  _procPtr
    )
    {
        fgStateExecute(
            &**this
            , reinterpret_cast< FgStateThreadProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThread< STATE_DATA_T >::Proc  _procPtr
        , StateJoiner &                             _joiner
    )
    {
        fgStateExecuteWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename STATE_DATA_T >
    template< typename USER_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThread< STATE_DATA_T, USER_DATA_T >::Proc _procPtr
        , StateJoiner &                                         _joiner
        , USER_DATA_T &                                         _userData
    )
    {
        fgStateExecuteWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadSingleton< STATE_DATA_T >::Proc _procPtr
    )
    {
        fgStateExecuteSingleton(
            &**this
            , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadSingleton< STATE_DATA_T >::Proc _procPtr
        , StateJoiner &                                     _joiner
    )
    {
        fgStateExecuteSingletonWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename STATE_DATA_T >
    template< typename USER_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadSingleton< STATE_DATA_T, USER_DATA_T >::Proc    _procPtr
        , StateJoiner &                                                     _joiner
        , USER_DATA_T &                                                     _userData
    )
    {
        fgStateExecuteSingletonWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadBackground< STATE_DATA_T >::Proc    _procPtr
    )
    {
        fgStateExecuteBackground(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadBackground< STATE_DATA_T >::Proc    _procPtr
        , StateJoiner &                                         _joiner
    )
    {
        fgStateExecuteBackgroundWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename STATE_DATA_T >
    template< typename USER_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadBackground< STATE_DATA_T, USER_DATA_T >::Proc   _procPtr
        , StateJoiner &                                                     _joiner
        , USER_DATA_T &                                                     _userData
    )
    {
        fgStateExecuteBackgroundWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadBackgroundSingleton< STATE_DATA_T >::Proc   _procPtr
    )
    {
        fgStateExecuteBackgroundSingleton(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundSingletonProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadBackgroundSingleton< STATE_DATA_T >::Proc   _procPtr
        , StateJoiner &                                                 _joiner
    )
    {
        fgStateExecuteBackgroundSingletonWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundSingletonProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename STATE_DATA_T >
    template< typename USER_DATA_T >
    void State< STATE_DATA_T >::execute(
        typename StateThreadBackgroundSingleton< STATE_DATA_T, USER_DATA_T >::Proc  _procPtr
        , StateJoiner &                                                             _joiner
        , USER_DATA_T &                                                             _userData
    )
    {
        fgStateExecuteBackgroundSingletonWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundSingletonProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    template< typename STATE_DATA_T >
    template< typename DATA_DESTROY_PROC_PTR_T >
    void State< STATE_DATA_T >::enter(
        const typename CreatingState< decltype( getArgType_( static_cast< DATA_DESTROY_PROC_PTR_T >( nullptr ) ) ) >::DataCreateProc &  _DATA_CREATE_PROC
        , DATA_DESTROY_PROC_PTR_T                                                                                                       _dataDestroyProcPtr
    )
    {
        fgStateEnter(
            &**this
            , &**(
                CreatingState< decltype( getArgType_( _dataDestroyProcPtr ) ) >::create(
                    *this
                    , _DATA_CREATE_PROC
                    , _dataDestroyProcPtr
                ).release()
            )
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::enter(
        const typename CreatingState<>::InitializeProc &    _INITIALIZE_PROC
    )
    {
        fgStateEnter(
            &**this
            , &**(
                CreatingState<>::create(
                    *this
                    , _INITIALIZE_PROC
                ).release()
            )
        );
    }

    template< typename STATE_DATA_T >
    void State< STATE_DATA_T >::exit(
    )
    {
        fgStateExit( &**this );
    }

    template< typename STATE_DATA_T >
    template< typename BASESYSTEM_T >
    BASESYSTEM_T & State< STATE_DATA_T >::getBasesystem_(
    )
    {
        return *static_cast< BASESYSTEM_T * >( fgStateGetBasesystem( &**this ) );
    }

    inline void State<>::execute(
        typename StateThread<>::Proc    _procPtr
    )
    {
        fgStateExecute(
            &**this
            , reinterpret_cast< FgStateThreadProc >( _procPtr )
        );
    }

    inline void State<>::execute(
        typename StateThread<>::Proc    _procPtr
        , StateJoiner &                 _joiner
    )
    {
        fgStateExecuteWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename USER_DATA_T >
    void State<>::execute(
        typename StateThread< void, USER_DATA_T >::Proc _procPtr
        , StateJoiner &                                 _joiner
        , USER_DATA_T &                                 _userData
    )
    {
        fgStateExecuteWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    inline void State<>::execute(
        typename StateThreadSingleton<>::Proc   _procPtr
    )
    {
        fgStateExecuteSingleton(
            &**this
            , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
        );
    }

    inline void State<>::execute(
        typename StateThreadSingleton<>::Proc   _procPtr
        , StateJoiner &                         _joiner
    )
    {
        fgStateExecuteSingletonWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename USER_DATA_T >
    void State<>::execute(
        typename StateThreadSingleton< void, USER_DATA_T >::Proc    _procPtr
        , StateJoiner &                                             _joiner
        , USER_DATA_T &                                             _userData
    )
    {
        fgStateExecuteSingletonWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadSingletonProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    inline void State<>::execute(
        typename StateThreadBackground<>::Proc  _procPtr
    )
    {
        fgStateExecuteBackground(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundProc >( _procPtr )
        );
    }

    inline void State<>::execute(
        typename StateThreadBackground<>::Proc  _procPtr
        , StateJoiner &                         _joiner
    )
    {
        fgStateExecuteBackgroundWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename USER_DATA_T >
    void State<>::execute(
        typename StateThreadBackground< void, USER_DATA_T >::Proc   _procPtr
        , StateJoiner &                                             _joiner
        , USER_DATA_T &                                             _userData
    )
    {
        fgStateExecuteBackgroundWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    inline void State<>::execute(
        typename StateThreadBackgroundSingleton<>::Proc _procPtr
    )
    {
        fgStateExecuteBackgroundSingleton(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundSingletonProc >( _procPtr )
        );
    }

    inline void State<>::execute(
        typename StateThreadBackgroundSingleton<>::Proc _procPtr
        , StateJoiner &                                 _joiner
    )
    {
        fgStateExecuteBackgroundSingletonWithJoiner(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundSingletonProc >( _procPtr )
            , &*_joiner
        );
    }

    template< typename USER_DATA_T >
    void State<>::execute(
        typename StateThreadBackgroundSingleton< void, USER_DATA_T >::Proc  _procPtr
        , StateJoiner &                                                     _joiner
        , USER_DATA_T &                                                     _userData
    )
    {
        fgStateExecuteBackgroundSingletonWithJoinerAndUserData(
            &**this
            , reinterpret_cast< FgStateThreadBackgroundSingletonProc >( _procPtr )
            , &*_joiner
            , &_userData
        );
    }

    template< typename DATA_DESTROY_PROC_PTR_T >
    void State<>::enter(
        const typename CreatingState< decltype( getArgType_( static_cast< DATA_DESTROY_PROC_PTR_T >( nullptr ) ) ) >::DataCreateProc &  _DATA_CREATE_PROC
        , DATA_DESTROY_PROC_PTR_T                                                                                                       _dataDestroyProcPtr
    )
    {
        fgStateEnter(
            &**this
            , &**(
                CreatingState< decltype( getArgType_( _dataDestroyProcPtr ) ) >::create(
                    *this
                    , _DATA_CREATE_PROC
                    , _dataDestroyProcPtr
                ).release()
            )
        );
    }

    inline void State<>::enter(
        const typename CreatingState<>::InitializeProc &    _INITIALIZE_PROC
    )
    {
        fgStateEnter(
            &**this
            , &**(
                CreatingState<>::create(
                    *this
                    , _INITIALIZE_PROC
                ).release()
            )
        );
    }

    inline void State<>::exit(
    )
    {
        fgStateExit( &**this );
    }

    template< typename BASESYSTEM_T >
    BASESYSTEM_T & State<>::getBasesystem_(
    )
    {
        return *static_cast< BASESYSTEM_T * >( fgStateGetBasesystem( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_STATE_H
