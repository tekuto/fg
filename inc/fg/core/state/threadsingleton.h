﻿#ifndef FG_CORE_STATE_THREADSINGLETON_H
#define FG_CORE_STATE_THREADSINGLETON_H

#include "fg/def/core/state/threadsingleton.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgStateThreadSingletonGetState(
            FgStateThreadSingleton *
        )
    )

    FG_FUNCTION_PTR(
        void
        , fgStateThreadSingletonGetData(
            FgStateThreadSingleton *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    State< STATE_DATA_T > & StateThreadSingleton< STATE_DATA_T, USER_DATA_T >::getState(
    )
    {
        return *reinterpret_cast< State< STATE_DATA_T > * >( fgStateThreadSingletonGetState( &**this ) );
    }

    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    USER_DATA_T & StateThreadSingleton< STATE_DATA_T, USER_DATA_T >::getData(
    )
    {
        return *static_cast< USER_DATA_T * >( fgStateThreadSingletonGetData( &**this ) );
    }

    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & StateThreadSingleton< STATE_DATA_T, void >::getState(
    )
    {
        return *reinterpret_cast< State< STATE_DATA_T > * >( fgStateThreadSingletonGetState( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CORE_STATE_THREADSINGLETON_H
