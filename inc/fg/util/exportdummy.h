﻿#ifndef FG_UTIL_EXPORT_H
#define FG_UTIL_EXPORT_H

#include "fg/util/exportcommon.h"

#undef FG_DEFINE_FUNCTION
#undef FG_FUNCTION_VOID
#undef FG_FUNCTION_BOOL
#undef FG_FUNCTION_NUM
#undef FG_FUNCTION_PTR
#undef FG_FUNCTION_REF
#undef FG_FUNCTION_ENUM

#define FG_DEFINE_FUNCTION( _returnType, _func, _dummyProc ) \
    FGEXPORT _returnType _func { _dummyProc }
#define FG_FUNCTION_VOID( _func ) \
    FG_DEFINE_FUNCTION( void, _func, )
#define FG_FUNCTION_BOOL( _func ) \
    FG_DEFINE_FUNCTION( bool, _func, return false; )
#define FG_FUNCTION_NUM( _returnType, _func ) \
    FG_DEFINE_FUNCTION( _returnType, _func, return 0; )
#define FG_FUNCTION_PTR( _returnType, _func ) \
    FG_DEFINE_FUNCTION( _returnType *, _func, return nullptr; )
#define FG_FUNCTION_REF( _returnType, _func ) \
    FG_DEFINE_FUNCTION( _returnType &, _func, return *static_cast< _returnType * >( nullptr ); )
#define FG_FUNCTION_ENUM( _returnType, _func, _dummyValue ) \
    FG_DEFINE_FUNCTION( _returnType, _func, return _dummyValue; )

#endif  // FG_UTIL_EXPORT_H
