﻿#ifndef FG_UTIL_DECLAREFUNCTION_H
#define FG_UTIL_DECLAREFUNCTION_H

#define FG_DECLARE_FUNCTION( _returnType, _func ) \
    FGEXPORT _returnType _func;
#define FG_FUNCTION_VOID( _func ) \
    FG_DECLARE_FUNCTION( void, _func )
#define FG_FUNCTION_BOOL( _func ) \
    FG_DECLARE_FUNCTION( bool, _func )
#define FG_FUNCTION_NUM( _returnType, _func ) \
    FG_DECLARE_FUNCTION( _returnType, _func )
#define FG_FUNCTION_PTR( _returnType, _func ) \
    FG_DECLARE_FUNCTION( _returnType *, _func )
#define FG_FUNCTION_REF( _returnType, _func ) \
    FG_DECLARE_FUNCTION( _returnType &, _func )
#define FG_FUNCTION_ENUM( _returnType, _func, _dummyValue ) \
    FG_DECLARE_FUNCTION( _returnType, _func )

#endif  // FG_UTIL_DECLAREFUNCTION_H
