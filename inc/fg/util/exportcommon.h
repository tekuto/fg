﻿#ifndef FG_UTIL_EXPORTCOMMON_H
#define FG_UTIL_EXPORTCOMMON_H

#undef  FGEXPORT

#if defined COMPILER_TYPE_GCC   // COMPILER_TYPE
#   define FGEXPORT __attribute__( ( visibility( "default" ) ) )
#elif defined COMPILER_TYPE_MSVC    // COMPILER_TYPE
#   define FGEXPORT __declspec( dllexport )
#else   // COMPILER_TYPE
#   error 未対応のコンパイラタイプ
#endif  // COMPILER_TYPE

#endif  // FG_UTIL_EXPORTCOMMON_H
