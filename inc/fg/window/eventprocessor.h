﻿#ifndef FG_WINDOW_EVENTPROCESSOR_H
#define FG_WINDOW_EVENTPROCESSOR_H

#include "fg/def/window/eventprocessor.h"
#include "fg/def/window/window.h"
#include "fg/def/window/eventmanagers.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgWindowEventProcessor
        , fgWindowEventProcessorCreate(
            FgState *
            , FgWindow *
            , FgWindowEventManagers *
        )
    )

    FG_FUNCTION_VOID(
        fgWindowEventProcessorDestroy(
            FgWindowEventProcessor *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    WindowEventProcessor::Unique WindowEventProcessor::create(
        State< STATE_DATA_T > & _state
        , Window &              _window
        , WindowEventManagers & _eventManagers
    )
    {
        return fgWindowEventProcessorCreate(
            &*_state
            , &*_window
            , &*_eventManagers
        );
    }

    inline void WindowEventProcessor::destroy(
        WindowEventProcessor *  _this
    )
    {
        fgWindowEventProcessorDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_WINDOW_EVENTPROCESSOR_H
