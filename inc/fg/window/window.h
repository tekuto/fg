﻿#ifndef FG_WINDOW_WINDOW_H
#define FG_WINDOW_WINDOW_H

#include "fg/def/window/window.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgWindow
        , fgWindowCreate(
            const char *
            , int
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgWindowDestroy(
            FgWindow *
        )
    )

    FG_FUNCTION_VOID(
        fgWindowRepaint(
            FgWindow *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline Window::Unique Window::create(
        const char *    _TITLE
        , int           _width
        , int           _height
    )
    {
        return fgWindowCreate(
            _TITLE
            , _width
            , _height
        );
    }

    inline void Window::destroy(
        Window *    _this
    )
    {
        fgWindowDestroy( &**_this );
    }

    inline void Window::repaint(
    )
    {
        fgWindowRepaint( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_WINDOW_WINDOW_H
