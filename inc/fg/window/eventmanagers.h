﻿#ifndef FG_WINDOW_EVENTMANAGERS_H
#define FG_WINDOW_EVENTMANAGERS_H

#include "fg/def/window/eventmanagers.h"
#include "fg/def/core/state/eventmanager.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgWindowEventManagers
        , fgWindowEventManagersCreate(
        )
    )

    FG_FUNCTION_VOID(
        fgWindowEventManagersDestroy(
            FgWindowEventManagers *
        )
    )

    FG_FUNCTION_PTR(
        FgStateEventManager
        , fgWindowEventManagersGetPaintEventManager(
            FgWindowEventManagers *
        )
    )

    FG_FUNCTION_PTR(
        FgStateEventManager
        , fgWindowEventManagersGetCloseEventManager(
            FgWindowEventManagers *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline WindowEventManagers::Unique WindowEventManagers::create(
    )
    {
        return fgWindowEventManagersCreate();
    }

    inline void WindowEventManagers::destroy(
        WindowEventManagers *   _this
    )
    {
        fgWindowEventManagersDestroy( &**_this );
    }

    inline WindowPaintEventManager & WindowEventManagers::getPaintEventManager(
    )
    {
        return reinterpret_cast< WindowPaintEventManager & >( *fgWindowEventManagersGetPaintEventManager( &**this ) );
    }

    inline WindowCloseEventManager & WindowEventManagers::getCloseEventManager(
    )
    {
        return reinterpret_cast< WindowCloseEventManager & >( *fgWindowEventManagersGetCloseEventManager( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_WINDOW_EVENTMANAGERS_H
