﻿#ifndef FG_WINDOW_CLOSEEVENT_H
#define FG_WINDOW_CLOSEEVENT_H

#include "fg/core/state/event.h"
#include "fg/core/state/eventbackground.h"
#include "fg/core/state/eventmanager.h"
#include "fg/core/state/eventregistermanager.h"
#include "fg/def/window/closeevent.h"
#include "fg/def/window/window.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgWindow
        , fgWindowCloseEventDataGetWindow(
            FgWindowCloseEventData *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline Window & WindowCloseEventData::getWindow(
    )
    {
        return reinterpret_cast< Window & >( *fgWindowCloseEventDataGetWindow( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_WINDOW_CLOSEEVENT_H
