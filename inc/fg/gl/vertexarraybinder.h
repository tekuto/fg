﻿#ifndef FG_GL_VERTEXARRAYBINDER_H
#define FG_GL_VERTEXARRAYBINDER_H

#include "fg/def/gl/vertexarraybinder.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/def/gl/vertexarrays.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLVertexArrayBinder
        , fgGLVertexArrayBinderCreate(
            FgGLCurrentContext *
            , const FgGLVertexArrays *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLVertexArrayBinderDestroy(
            FgGLVertexArrayBinder *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLVertexArrayBinder::Unique GLVertexArrayBinder::create(
        GLCurrentContext &          _glCurrentContext
        , const GLVertexArrays &    _GL_VERTEX_ARRAYS
        , GLsizei                   _index
    )
    {
        return fgGLVertexArrayBinderCreate(
            &*_glCurrentContext
            , &*_GL_VERTEX_ARRAYS
            , _index
        );
    }

    inline void GLVertexArrayBinder::destroy(
        GLVertexArrayBinder *   _this
    )
    {
        fgGLVertexArrayBinderDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_VERTEXARRAYBINDER_H
