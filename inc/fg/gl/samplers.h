﻿#ifndef FG_GL_SAMPLERS_H
#define FG_GL_SAMPLERS_H

#include "fg/def/gl/samplers.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLSamplers
        , fgGLSamplersCreate(
            FgGLCurrentContext *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLSamplersDestroy(
            FgGLSamplers *
        )
    )

    FG_FUNCTION_NUM(
        FgGLsizei
        , fgGLSamplersGetSize(
            const FgGLSamplers *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLSamplersGetName(
            const FgGLSamplers *
            , FgGLsizei
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLSamplers::Unique GLSamplers::create(
        GLCurrentContext &  _glCurrentContext
        , GLsizei           _size
    )
    {
        return fgGLSamplersCreate(
            &*_glCurrentContext
            , _size
        );
    }

    inline void GLSamplers::destroy(
        GLSamplers *    _this
    )
    {
        fgGLSamplersDestroy( &**_this );
    }

    inline GLsizei GLSamplers::getSize(
    ) const
    {
        return fgGLSamplersGetSize( &**this );
    }

    inline GLuint GLSamplers::getName(
        GLsizei _index
    ) const
    {
        return fgGLSamplersGetName(
            &**this
            , _index
        );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_SAMPLERS_H
