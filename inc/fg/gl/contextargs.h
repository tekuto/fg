﻿#ifndef FG_GL_CONTEXTARGS_H
#define FG_GL_CONTEXTARGS_H

#include "fg/def/gl/contextargs.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLContextArgs
        , fgGLContextArgsCreate(
        )
    )

    FG_FUNCTION_PTR(
        FgGLContextArgs
        , fgGLContextArgsCreateBlank(
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsDestroy(
            FgGLContextArgs *
        )
    )

    FG_FUNCTION_BOOL(
        fgGLContextArgsIsExistsRedSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextArgsGetRedSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsSetRedSize(
            FgGLContextArgs *
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsClearRedSize(
            FgGLContextArgs *
        )
    )

    FG_FUNCTION_BOOL(
        fgGLContextArgsIsExistsGreenSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextArgsGetGreenSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsSetGreenSize(
            FgGLContextArgs *
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsClearGreenSize(
            FgGLContextArgs *
        )
    )

    FG_FUNCTION_BOOL(
        fgGLContextArgsIsExistsBlueSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextArgsGetBlueSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsSetBlueSize(
            FgGLContextArgs *
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsClearBlueSize(
            FgGLContextArgs *
        )
    )

    FG_FUNCTION_BOOL(
        fgGLContextArgsIsExistsAlphaSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextArgsGetAlphaSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsSetAlphaSize(
            FgGLContextArgs *
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsClearAlphaSize(
            FgGLContextArgs *
        )
    )

    FG_FUNCTION_BOOL(
        fgGLContextArgsIsExistsDepthSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextArgsGetDepthSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsSetDepthSize(
            FgGLContextArgs *
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsClearDepthSize(
            FgGLContextArgs *
        )
    )

    FG_FUNCTION_BOOL(
        fgGLContextArgsIsExistsStencilSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextArgsGetStencilSize(
            const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsSetStencilSize(
            FgGLContextArgs *
            , int
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextArgsClearStencilSize(
            FgGLContextArgs *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLContextArgs::Unique GLContextArgs::create(
    )
    {
        return fgGLContextArgsCreate();
    }

    inline GLContextArgs::Unique GLContextArgs::createBlank(
    )
    {
        return fgGLContextArgsCreateBlank();
    }

    inline void GLContextArgs::destroy(
        GLContextArgs * _this
    )
    {
        fgGLContextArgsDestroy( &**_this );
    }

    inline bool GLContextArgs::isExistsRedSize(
    ) const
    {
        return fgGLContextArgsIsExistsRedSize( &**this );
    }

    inline int GLContextArgs::getRedSize(
    ) const
    {
        return fgGLContextArgsGetRedSize( &**this );
    }

    inline void GLContextArgs::setRedSize(
        int _size
    )
    {
        fgGLContextArgsSetRedSize(
            &**this
            , _size
        );
    }

    inline void GLContextArgs::clearRedSize(
    )
    {
        fgGLContextArgsClearRedSize( &**this );
    }

    inline bool GLContextArgs::isExistsGreenSize(
    ) const
    {
        return fgGLContextArgsIsExistsGreenSize( &**this );
    }

    inline int GLContextArgs::getGreenSize(
    ) const
    {
        return fgGLContextArgsGetGreenSize( &**this );
    }

    inline void GLContextArgs::setGreenSize(
        int _size
    )
    {
        fgGLContextArgsSetGreenSize(
            &**this
            , _size
        );
    }

    inline void GLContextArgs::clearGreenSize(
    )
    {
        fgGLContextArgsClearGreenSize( &**this );
    }

    inline bool GLContextArgs::isExistsBlueSize(
    ) const
    {
        return fgGLContextArgsIsExistsBlueSize( &**this );
    }

    inline int GLContextArgs::getBlueSize(
    ) const
    {
        return fgGLContextArgsGetBlueSize( &**this );
    }

    inline void GLContextArgs::setBlueSize(
        int _size
    )
    {
        fgGLContextArgsSetBlueSize(
            &**this
            , _size
        );
    }

    inline void GLContextArgs::clearBlueSize(
    )
    {
        fgGLContextArgsClearBlueSize( &**this );
    }

    inline bool GLContextArgs::isExistsAlphaSize(
    ) const
    {
        return fgGLContextArgsIsExistsAlphaSize( &**this );
    }

    inline int GLContextArgs::getAlphaSize(
    ) const
    {
        return fgGLContextArgsGetAlphaSize( &**this );
    }

    inline void GLContextArgs::setAlphaSize(
        int _size
    )
    {
        fgGLContextArgsSetAlphaSize(
            &**this
            , _size
        );
    }

    inline void GLContextArgs::clearAlphaSize(
    )
    {
        fgGLContextArgsClearAlphaSize( &**this );
    }

    inline bool GLContextArgs::isExistsDepthSize(
    ) const
    {
        return fgGLContextArgsIsExistsDepthSize( &**this );
    }

    inline int GLContextArgs::getDepthSize(
    ) const
    {
        return fgGLContextArgsGetDepthSize( &**this );
    }

    inline void GLContextArgs::setDepthSize(
        int _size
    )
    {
        fgGLContextArgsSetDepthSize(
            &**this
            , _size
        );
    }

    inline void GLContextArgs::clearDepthSize(
    )
    {
        fgGLContextArgsClearDepthSize( &**this );
    }

    inline bool GLContextArgs::isExistsStencilSize(
    ) const
    {
        return fgGLContextArgsIsExistsStencilSize( &**this );
    }

    inline int GLContextArgs::getStencilSize(
    ) const
    {
        return fgGLContextArgsGetStencilSize( &**this );
    }

    inline void GLContextArgs::setStencilSize(
        int _size
    )
    {
        fgGLContextArgsSetStencilSize(
            &**this
            , _size
        );
    }

    inline void GLContextArgs::clearStencilSize(
    )
    {
        fgGLContextArgsClearStencilSize( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_CONTEXTARGS_H
