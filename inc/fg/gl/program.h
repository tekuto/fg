﻿#ifndef FG_GL_PROGRAM_H
#define FG_GL_PROGRAM_H

#include "fg/def/gl/program.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLProgram
        , fgGLProgramCreate(
            FgGLCurrentContext *
        )
    )

    FG_FUNCTION_VOID(
        fgGLProgramDestroy(
            FgGLProgram *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLProgramGetObject(
            const FgGLProgram *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLProgram::Unique GLProgram::create(
        GLCurrentContext &  _glCurrentContext
    )
    {
        return fgGLProgramCreate( &*_glCurrentContext );
    }

    inline void GLProgram::destroy(
        GLProgram * _this
    )
    {
        fgGLProgramDestroy( &**_this );
    }

    inline GLuint GLProgram::getObject(
    ) const
    {
        return fgGLProgramGetObject( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_PROGRAM_H
