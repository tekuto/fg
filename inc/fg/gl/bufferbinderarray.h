﻿#ifndef FG_GL_BUFFERBINDERARRAY_H
#define FG_GL_BUFFERBINDERARRAY_H

#include "fg/def/gl/bufferbinderarray.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/def/gl/buffers.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLBufferBinderArray
        , fgGLBufferBinderArrayCreate(
            FgGLCurrentContext *
            , const FgGLBuffers *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLBufferBinderArrayDestroy(
            FgGLBufferBinderArray *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLBufferBinderArray::Unique GLBufferBinderArray::create(
        GLCurrentContext &  _glCurrentContext
        , const GLBuffers & _GL_BUFFERS
        , GLsizei           _index
    )
    {
        return fgGLBufferBinderArrayCreate(
            &*_glCurrentContext
            , &*_GL_BUFFERS
            , _index
        );
    }

    inline void GLBufferBinderArray::destroy(
        GLBufferBinderArray *   _this
    )
    {
        fgGLBufferBinderArrayDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_BUFFERBINDERARRAY_H
