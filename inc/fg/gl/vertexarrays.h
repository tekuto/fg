﻿#ifndef FG_GL_VERTEXARRAYS_H
#define FG_GL_VERTEXARRAYS_H

#include "fg/def/gl/vertexarrays.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLVertexArrays
        , fgGLVertexArraysCreate(
            FgGLCurrentContext *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLVertexArraysDestroy(
            FgGLVertexArrays *
        )
    )

    FG_FUNCTION_NUM(
        FgGLsizei
        , fgGLVertexArraysGetSize(
            const FgGLVertexArrays *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLVertexArraysGetName(
            const FgGLVertexArrays *
            , FgGLsizei
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLVertexArrays::Unique GLVertexArrays::create(
        GLCurrentContext &  _glCurrentContext
        , GLsizei           _size
    )
    {
        return fgGLVertexArraysCreate(
            &*_glCurrentContext
            , _size
        );
    }

    inline void GLVertexArrays::destroy(
        GLVertexArrays *    _this
    )
    {
        fgGLVertexArraysDestroy( &**_this );
    }

    inline GLsizei GLVertexArrays::getSize(
    ) const
    {
        return fgGLVertexArraysGetSize( &**this );
    }

    inline GLuint GLVertexArrays::getName(
        GLsizei _index
    ) const
    {
        return fgGLVertexArraysGetName(
            &**this
            , _index
        );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_VERTEXARRAYS_H
