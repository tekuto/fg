﻿#ifndef FG_GL_VERTEXSHADER_H
#define FG_GL_VERTEXSHADER_H

#include "fg/def/gl/vertexshader.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLVertexShader
        , fgGLVertexShaderCreate(
            FgGLCurrentContext *
        )
    )

    FG_FUNCTION_VOID(
        fgGLVertexShaderDestroy(
            FgGLVertexShader *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLVertexShaderGetObject(
            const FgGLVertexShader *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLVertexShader::Unique GLVertexShader::create(
        GLCurrentContext &  _glCurrentContext
    )
    {
        return fgGLVertexShaderCreate( &*_glCurrentContext );
    }

    inline void GLVertexShader::destroy(
        GLVertexShader *    _this
    )
    {
        fgGLVertexShaderDestroy( &**_this );
    }

    inline GLuint GLVertexShader::getObject(
    ) const
    {
        return fgGLVertexShaderGetObject( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_VERTEXSHADER_H
