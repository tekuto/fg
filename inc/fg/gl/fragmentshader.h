﻿#ifndef FG_GL_FRAGMENTSHADER_H
#define FG_GL_FRAGMENTSHADER_H

#include "fg/def/gl/fragmentshader.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLFragmentShader
        , fgGLFragmentShaderCreate(
            FgGLCurrentContext *
        )
    )

    FG_FUNCTION_VOID(
        fgGLFragmentShaderDestroy(
            FgGLFragmentShader *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLFragmentShaderGetObject(
            const FgGLFragmentShader *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLFragmentShader::Unique GLFragmentShader::create(
        GLCurrentContext &  _glCurrentContext
    )
    {
        return fgGLFragmentShaderCreate( &*_glCurrentContext );
    }

    inline void GLFragmentShader::destroy(
        GLFragmentShader *  _this
    )
    {
        fgGLFragmentShaderDestroy( &**_this );
    }

    inline GLuint GLFragmentShader::getObject(
    ) const
    {
        return fgGLFragmentShaderGetObject( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_FRAGMENTSHADER_H
