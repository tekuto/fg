﻿#ifndef FG_GL_CONTEXT_H
#define FG_GL_CONTEXT_H

#include "fg/def/gl/context.h"
#include "fg/def/gl/contextargs.h"
#include "fg/def/window/window.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLContext
        , fgGLContextCreate(
            FgWindow *
        )
    )

    FG_FUNCTION_PTR(
        FgGLContext
        , fgGLContextCreateWithShareList(
            FgWindow *
            , FgGLContext *
        )
    )

    FG_FUNCTION_PTR(
        FgGLContext
        , fgGLContextCreateWithArgs(
            FgWindow *
            , const FgGLContextArgs *
        )
    )

    FG_FUNCTION_PTR(
        FgGLContext
        , fgGLContextCreateWithShareListAndArgs(
            FgWindow *
            , FgGLContext *
            , const FgGLContextArgs *
        )
    )

    FG_FUNCTION_VOID(
        fgGLContextDestroy(
            FgGLContext *
        )
    )

    //TODO fgGLContextGetDisplay()（仮）に変更する
    FG_FUNCTION_PTR(
        FgWindow
        , fgGLContextGetWindow(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetBufferSize(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetRedSize(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetGreenSize(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetBlueSize(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetAlphaSize(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetDepthSize(
            FgGLContext *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgGLContextGetStencilSize(
            FgGLContext *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLContext::Unique GLContext::create(
        Window &    _window
    )
    {
        return fgGLContextCreate( &*_window );
    }

    inline GLContext::Unique GLContext::create(
        Window &        _window
        , GLContext &   _glContext
    )
    {
        return fgGLContextCreateWithShareList(
            &*_window
            , &*_glContext
        );
    }

    inline GLContext::Unique GLContext::create(
        Window &                _window
        , const GLContextArgs & _ARGS
    )
    {
        return fgGLContextCreateWithArgs(
            &*_window
            , &*_ARGS
        );
    }

    inline GLContext::Unique GLContext::create(
        Window &                _window
        , GLContext &           _glContext
        , const GLContextArgs & _ARGS
    )
    {
        return fgGLContextCreateWithShareListAndArgs(
            &*_window
            , &*_glContext
            , &*_ARGS
        );
    }

    inline void GLContext::destroy(
        GLContext * _this
    )
    {
        fgGLContextDestroy( &**_this );
    }

    inline Window & GLContext::getWindow(
    )
    {
        return reinterpret_cast< Window & >( *fgGLContextGetWindow( &**this ) );
    }

    inline int GLContext::getBufferSize(
    )
    {
        return fgGLContextGetBufferSize( &**this );
    }

    inline int GLContext::getRedSize(
    )
    {
        return fgGLContextGetRedSize( &**this );
    }

    inline int GLContext::getGreenSize(
    )
    {
        return fgGLContextGetGreenSize( &**this );
    }

    inline int GLContext::getBlueSize(
    )
    {
        return fgGLContextGetBlueSize( &**this );
    }

    inline int GLContext::getAlphaSize(
    )
    {
        return fgGLContextGetAlphaSize( &**this );
    }

    inline int GLContext::getDepthSize(
    )
    {
        return fgGLContextGetDepthSize( &**this );
    }

    inline int GLContext::getStencilSize(
    )
    {
        return fgGLContextGetStencilSize( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_CONTEXT_H
