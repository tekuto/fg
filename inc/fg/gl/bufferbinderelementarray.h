﻿#ifndef FG_GL_BUFFERBINDERELEMENTARRAY_H
#define FG_GL_BUFFERBINDERELEMENTARRAY_H

#include "fg/def/gl/bufferbinderelementarray.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/def/gl/buffers.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLBufferBinderElementArray
        , fgGLBufferBinderElementArrayCreate(
            FgGLCurrentContext *
            , const FgGLBuffers *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLBufferBinderElementArrayDestroy(
            FgGLBufferBinderElementArray *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLBufferBinderElementArray::Unique GLBufferBinderElementArray::create(
        GLCurrentContext &  _glCurrentContext
        , const GLBuffers & _GL_BUFFERS
        , GLsizei           _index
    )
    {
        return fgGLBufferBinderElementArrayCreate(
            &*_glCurrentContext
            , &*_GL_BUFFERS
            , _index
        );
    }

    inline void GLBufferBinderElementArray::destroy(
        GLBufferBinderElementArray *    _this
    )
    {
        fgGLBufferBinderElementArrayDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_BUFFERBINDERELEMENTARRAY_H
