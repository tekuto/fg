﻿#ifndef FG_GL_TEXTURES_H
#define FG_GL_TEXTURES_H

#include "fg/def/gl/textures.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLTextures
        , fgGLTexturesCreate(
            FgGLCurrentContext *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLTexturesDestroy(
            FgGLTextures *
        )
    )

    FG_FUNCTION_NUM(
        FgGLsizei
        , fgGLTexturesGetSize(
            const FgGLTextures *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLTexturesGetName(
            const FgGLTextures *
            , FgGLsizei
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLTextures::Unique GLTextures::create(
        GLCurrentContext &  _glCurrentContext
        , GLsizei           _size
    )
    {
        return fgGLTexturesCreate(
            &*_glCurrentContext
            , _size
        );
    }

    inline void GLTextures::destroy(
        GLTextures *    _this
    )
    {
        fgGLTexturesDestroy( &**_this );
    }

    inline GLsizei GLTextures::getSize(
    ) const
    {
        return fgGLTexturesGetSize( &**this );
    }

    inline GLuint GLTextures::getName(
        GLsizei _index
    ) const
    {
        return fgGLTexturesGetName(
            &**this
            , _index
        );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_TEXTURES_H
