﻿#ifndef FG_GL_THREADEXECUTOR_H
#define FG_GL_THREADEXECUTOR_H

#include "fg/def/gl/threadexecutor.h"
#include "fg/def/gl/thread.h"
#include "fg/def/gl/context.h"
#include "fg/def/core/state/creating.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLThreadExecutor
        , fgGLThreadExecutorCreateForCreatingState(
            FgCreatingState *
            , FgGLContext *
            , FgGLThreadProc
        )
    )

    FG_FUNCTION_PTR(
        FgGLThreadExecutor
        , fgGLThreadExecutorCreateForCreatingStateWithUserData(
            FgCreatingState *
            , FgGLContext *
            , FgGLThreadProc
            , void *
        )
    )

    FG_FUNCTION_PTR(
        FgGLThreadExecutor
        , fgGLThreadExecutorCreateForState(
            FgState *
            , FgGLContext *
            , FgGLThreadProc
        )
    )

    FG_FUNCTION_PTR(
        FgGLThreadExecutor
        , fgGLThreadExecutorCreateForStateWithUserData(
            FgState *
            , FgGLContext *
            , FgGLThreadProc
            , void *
        )
    )

    FG_FUNCTION_VOID(
        fgGLThreadExecutorDestroy(
            FgGLThreadExecutor *
        )
    )

    FG_FUNCTION_VOID(
        fgGLThreadExecutorExecute(
            FgGLThreadExecutor *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    GLThreadExecutor::Unique GLThreadExecutor::create(
        CreatingState< STATE_DATA_T > &             _state
        , GLContext &                               _glContext
        , typename GLThread< STATE_DATA_T >::Proc   _procPtr
    )
    {
        return fgGLThreadExecutorCreateForCreatingState(
            &*_state
            , &*_glContext
            , reinterpret_cast< FgGLThreadProc >( _procPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    GLThreadExecutor::Unique GLThreadExecutor::create(
        CreatingState< STATE_DATA_T > &                         _state
        , GLContext &                                           _glContext
        , typename GLThread< STATE_DATA_T, USER_DATA_T >::Proc  _procPtr
        , USER_DATA_T &                                         _userData
    )
    {
        return fgGLThreadExecutorCreateForCreatingStateWithUserData(
            &*_state
            , &*_glContext
            , reinterpret_cast< FgGLThreadProc >( _procPtr )
            , &_userData
        );
    }

    template< typename STATE_DATA_T >
    GLThreadExecutor::Unique GLThreadExecutor::create(
        State< STATE_DATA_T > &                     _state
        , GLContext &                               _glContext
        , typename GLThread< STATE_DATA_T >::Proc   _procPtr
    )
    {
        return fgGLThreadExecutorCreateForState(
            &*_state
            , &*_glContext
            , reinterpret_cast< FgGLThreadProc >( _procPtr )
        );
    }

    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    GLThreadExecutor::Unique GLThreadExecutor::create(
        State< STATE_DATA_T > &                                 _state
        , GLContext &                                           _glContext
        , typename GLThread< STATE_DATA_T, USER_DATA_T >::Proc  _procPtr
        , USER_DATA_T &                                         _userData
    )
    {
        return fgGLThreadExecutorCreateForStateWithUserData(
            &*_state
            , &*_glContext
            , reinterpret_cast< FgGLThreadProc >( _procPtr )
            , &_userData
        );
    }

    inline void GLThreadExecutor::destroy(
        GLThreadExecutor *  _this
    )
    {
        fgGLThreadExecutorDestroy( &**_this );
    }

    inline void GLThreadExecutor::execute(
    )
    {
        fgGLThreadExecutorExecute( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_THREADEXECUTOR_H
