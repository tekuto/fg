﻿#ifndef FG_GL_TEXTUREBINDER2D_H
#define FG_GL_TEXTUREBINDER2D_H

#include "fg/def/gl/texturebinder2d.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/def/gl/textures.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLTextureBinder2D
        , fgGLTextureBinder2DCreate(
            FgGLCurrentContext *
            , const FgGLTextures *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLTextureBinder2DDestroy(
            FgGLTextureBinder2D *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLTextureBinder2D::Unique GLTextureBinder2D::create(
        GLCurrentContext &      _glCurrentContext
        , const GLTextures &    _GL_TEXTURES
        , GLsizei               _index
    )
    {
        return fgGLTextureBinder2DCreate(
            &*_glCurrentContext
            , &*_GL_TEXTURES
            , _index
        );
    }

    inline void GLTextureBinder2D::destroy(
        GLTextureBinder2D * _this
    )
    {
        fgGLTextureBinder2DDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_TEXTUREBINDER2D_H
