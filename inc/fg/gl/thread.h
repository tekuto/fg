﻿#ifndef FG_GL_THREAD_H
#define FG_GL_THREAD_H

#include "fg/def/gl/thread.h"
#include "fg/def/gl/context.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgState
        , fgGLThreadGetState(
            FgGLThread *
        )
    )

    FG_FUNCTION_PTR(
        FgGLContext
        , fgGLThreadGetGLContext(
            FgGLThread *
        )
    )

    FG_FUNCTION_PTR(
        void
        , fgGLThreadGetData(
            FgGLThread *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    State< STATE_DATA_T > & GLThread< STATE_DATA_T, USER_DATA_T >::getState(
    )
    {
        return reinterpret_cast< State< STATE_DATA_T > & >( *fgGLThreadGetState( &**this ) );
    }

    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    GLContext & GLThread< STATE_DATA_T, USER_DATA_T >::getGLContext(
    )
    {
        return reinterpret_cast< GLContext & >( *fgGLThreadGetGLContext( &**this ) );
    }

    template<
        typename STATE_DATA_T
        , typename USER_DATA_T
    >
    USER_DATA_T & GLThread< STATE_DATA_T, USER_DATA_T >::getData(
    )
    {
        return *static_cast< USER_DATA_T * >( fgGLThreadGetData( &**this ) );
    }

    template< typename STATE_DATA_T >
    State< STATE_DATA_T > & GLThread< STATE_DATA_T, void >::getState(
    )
    {
        return reinterpret_cast< State< STATE_DATA_T > & >( *fgGLThreadGetState( &**this ) );
    }

    template< typename STATE_DATA_T >
    GLContext & GLThread< STATE_DATA_T, void >::getGLContext(
    )
    {
        return reinterpret_cast< GLContext & >( *fgGLThreadGetGLContext( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_THREAD_H
