﻿#ifndef FG_GL_PICTURE_H
#define FG_GL_PICTURE_H

#include "fg/def/gl/picture.h"
#include "fg/def/gl/types.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLPicture
        , fgGLPictureCreate(
            const void *
            , size_t
            , FgGLsizei
            , FgGLsizei
            , FgGLenum
            , FgGLenum
        )
    )

    FG_FUNCTION_VOID(
        fgGLPictureDestroy(
            FgGLPicture *
        )
    )

    FG_FUNCTION_NUM(
        size_t
        , fgGLPictureGetSize(
            const FgGLPicture *
        )
    )

    FG_FUNCTION_PTR(
        const void
        , fgGLPictureGetData(
            const FgGLPicture *
        )
    )

    FG_FUNCTION_NUM(
        FgGLsizei
        , fgGLPictureGetWidth(
            const FgGLPicture *
        )
    )

    FG_FUNCTION_NUM(
        FgGLsizei
        , fgGLPictureGetHeight(
            const FgGLPicture *
        )
    )

    FG_FUNCTION_NUM(
        FgGLenum
        , fgGLPictureGetFormat(
            const FgGLPicture *
        )
    )

    FG_FUNCTION_NUM(
        FgGLenum
        , fgGLPictureGetDataType(
            const FgGLPicture *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLPicture::Unique GLPicture::create(
        const void *    _DATA_PTR
        , std::size_t   _size
        , GLsizei       _width
        , GLsizei       _height
        , GLenum        _format
        , GLenum        _dataType
    )
    {
        return fgGLPictureCreate(
            _DATA_PTR
            , _size
            , _width
            , _height
            , _format
            , _dataType
        );
    }

    inline void GLPicture::destroy(
        GLPicture * _this
    )
    {
        fgGLPictureDestroy( &**_this );
    }

    inline std::size_t GLPicture::getSize(
    ) const
    {
        return fgGLPictureGetSize( &**this );
    }

    inline const void * GLPicture::getData(
    ) const
    {
        return fgGLPictureGetData( &**this );
    }

    inline GLsizei GLPicture::getWidth(
    ) const
    {
        return fgGLPictureGetWidth( &**this );
    }

    inline GLsizei GLPicture::getHeight(
    ) const
    {
        return fgGLPictureGetHeight( &**this );
    }

    inline GLenum GLPicture::getFormat(
    ) const
    {
        return fgGLPictureGetFormat( &**this );
    }

    inline GLenum GLPicture::getDataType(
    ) const
    {
        return fgGLPictureGetDataType( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_PICTURE_H
