﻿#ifndef FG_GL_BUFFERS_H
#define FG_GL_BUFFERS_H

#include "fg/def/gl/buffers.h"
#include "fg/def/gl/types.h"
#include "fg/def/gl/currentcontext.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgGLBuffers
        , fgGLBuffersCreate(
            FgGLCurrentContext *
            , FgGLsizei
        )
    )

    FG_FUNCTION_VOID(
        fgGLBuffersDestroy(
            FgGLBuffers *
        )
    )

    FG_FUNCTION_NUM(
        FgGLsizei
        , fgGLBuffersGetSize(
            const FgGLBuffers *
        )
    )

    FG_FUNCTION_NUM(
        FgGLuint
        , fgGLBuffersGetName(
            const FgGLBuffers *
            , FgGLsizei
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline GLBuffers::Unique GLBuffers::create(
        GLCurrentContext &  _glCurrentContext
        , GLsizei           _size
    )
    {
        return fgGLBuffersCreate(
            &*_glCurrentContext
            , _size
        );
    }

    inline void GLBuffers::destroy(
        GLBuffers * _this
    )
    {
        fgGLBuffersDestroy( &**_this );
    }

    inline GLsizei GLBuffers::getSize(
    ) const
    {
        return fgGLBuffersGetSize( &**this );
    }

    inline GLuint GLBuffers::getName(
        GLsizei _index
    ) const
    {
        return fgGLBuffersGetName(
            &**this
            , _index
        );
    }
}

#endif  // __cplusplus

#endif  // FG_GL_BUFFERS_H
