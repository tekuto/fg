﻿#ifndef FG_CONTROLLER_RAW_ID_H
#define FG_CONTROLLER_RAW_ID_H

#include "fg/def/controller/raw/id.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgRawControllerID
        , fgRawControllerIDCreate(
            const char *
            , const char *
            , const char *
        )
    )

    FG_FUNCTION_VOID(
        fgRawControllerIDDestroy(
            FgRawControllerID *
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerID
        , fgRawControllerIDClone(
            const FgRawControllerID *
        )
    )

    FG_FUNCTION_PTR(
        const char
        , fgRawControllerIDGetModule(
            const FgRawControllerID *
        )
    )

    FG_FUNCTION_PTR(
        const char
        , fgRawControllerIDGetPort(
            const FgRawControllerID *
        )
    )

    FG_FUNCTION_PTR(
        const char
        , fgRawControllerIDGetDevice(
            const FgRawControllerID *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerIDEquals(
            const FgRawControllerID *
            , const FgRawControllerID *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerIDLess(
            const FgRawControllerID *
            , const FgRawControllerID *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline RawControllerID::ConstUnique RawControllerID::create(
        const char *    _MODULE
        , const char *  _PORT
        , const char *  _DEVICE
    )
    {
        return fgRawControllerIDCreate(
            _MODULE
            , _PORT
            , _DEVICE
        );
    }

    inline void RawControllerID::destroy(
        RawControllerID *   _this
    )
    {
        fgRawControllerIDDestroy( &**_this );
    }

    inline RawControllerID::ConstUnique RawControllerID::clone(
    ) const
    {
        return fgRawControllerIDClone( &**this );
    }

    inline const char * RawControllerID::getModule(
    ) const
    {
        return fgRawControllerIDGetModule( &**this );
    }

    inline const char * RawControllerID::getPort(
    ) const
    {
        return fgRawControllerIDGetPort( &**this );
    }

    inline const char * RawControllerID::getDevice(
    ) const
    {
        return fgRawControllerIDGetDevice( &**this );
    }

    inline bool RawControllerID::operator==(
        const RawControllerID & _OTHER
    ) const
    {
        return fgRawControllerIDEquals(
            &**this
            , &*_OTHER
        );
    }

    inline bool RawControllerID::operator<(
        const RawControllerID & _OTHER
    ) const
    {
        return fgRawControllerIDLess(
            &**this
            , &*_OTHER
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_ID_H
