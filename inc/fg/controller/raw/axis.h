﻿#ifndef FG_CONTROLLER_RAW_AXIS_H
#define FG_CONTROLLER_RAW_AXIS_H

#include "fg/def/controller/raw/axis.h"
#include "fg/def/controller/raw/id.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgRawControllerAxis
        , fgRawControllerAxisCreate(
            const FgRawControllerID *
            , size_t
        )
    )

    FG_FUNCTION_VOID(
        fgRawControllerAxisDestroy(
            FgRawControllerAxis *
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerAxis
        , fgRawControllerAxisClone(
            const FgRawControllerAxis *
        )
    )

    FG_FUNCTION_PTR(
        const FgRawControllerID
        , fgRawControllerAxisGetID(
            const FgRawControllerAxis *
        )
    )

    FG_FUNCTION_NUM(
        size_t
        , fgRawControllerAxisGetIndex(
            const FgRawControllerAxis *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerAxisEquals(
            const FgRawControllerAxis *
            , const FgRawControllerAxis *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerAxisLess(
            const FgRawControllerAxis *
            , const FgRawControllerAxis *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline RawControllerAxis::ConstUnique RawControllerAxis::create(
        const RawControllerID & _ID
        , std::size_t           _index
    )
    {
        return fgRawControllerAxisCreate(
            &*_ID
            , _index
        );
    }

    inline void RawControllerAxis::destroy(
        RawControllerAxis * _this
    )
    {
        fgRawControllerAxisDestroy( &**_this );
    }

    inline RawControllerAxis::ConstUnique RawControllerAxis::clone(
    ) const
    {
        return fgRawControllerAxisClone( &**this );
    }

    inline const RawControllerID & RawControllerAxis::getID(
    ) const
    {
        return reinterpret_cast< const RawControllerID & >( *fgRawControllerAxisGetID( &**this ) );
    }

    inline std::size_t RawControllerAxis::getIndex(
    ) const
    {
        return fgRawControllerAxisGetIndex( &**this );
    }

    inline bool RawControllerAxis::operator==(
        const RawControllerAxis &   _OTHER
    ) const
    {
        return fgRawControllerAxisEquals(
            &**this
            , &*_OTHER
        );
    }

    inline bool RawControllerAxis::operator<(
        const RawControllerAxis &   _OTHER
    ) const
    {
        return fgRawControllerAxisLess(
            &**this
            , &*_OTHER
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_AXIS_H
