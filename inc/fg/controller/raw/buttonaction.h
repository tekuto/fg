﻿#ifndef FG_CONTROLLER_RAW_BUTTONACTION_H
#define FG_CONTROLLER_RAW_BUTTONACTION_H

#include "fg/def/controller/raw/buttonaction.h"
#include "fg/def/controller/raw/button.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const FgRawControllerButton
        , fgRawControllerButtonGetButton(
            const FgRawControllerButtonAction *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerButtonIsPressedAction(
            const FgRawControllerButtonAction *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const RawControllerButton & RawControllerButtonAction::getButton(
    ) const
    {
        return reinterpret_cast< const RawControllerButton & >( *fgRawControllerButtonGetButton( &**this ) );
    }

    inline bool RawControllerButtonAction::isPressedAction(
    ) const
    {
        return fgRawControllerButtonIsPressedAction( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_BUTTONACTION_H
