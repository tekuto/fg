﻿#ifndef FG_CONTROLLER_RAW_CONNECTACTION_H
#define FG_CONTROLLER_RAW_CONNECTACTION_H

#include "fg/def/controller/raw/connectaction.h"
#include "fg/def/controller/raw/id.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const FgRawControllerID
        , fgRawControllerConnectActionGetID(
            const FgRawControllerConnectAction *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerConnectActionIsConnectedAction(
            const FgRawControllerConnectAction *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const RawControllerID & RawControllerConnectAction::getID(
    ) const
    {
        return reinterpret_cast< const RawControllerID & >( *fgRawControllerConnectActionGetID( &**this ) );
    }

    inline bool RawControllerConnectAction::isConnectedAction(
    ) const
    {
        return fgRawControllerConnectActionIsConnectedAction( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_CONNECTACTION_H
