﻿#ifndef FG_CONTROLLER_RAW_AXISACTION_H
#define FG_CONTROLLER_RAW_AXISACTION_H

#include "fg/def/controller/raw/axisaction.h"
#include "fg/def/controller/raw/axis.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const FgRawControllerAxis
        , fgRawControllerAxisGetAxis(
            const FgRawControllerAxisAction *
        )
    )

    FG_FUNCTION_NUM(
        int
        , fgRawControllerAxisGetValue(
            const FgRawControllerAxisAction *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const RawControllerAxis & RawControllerAxisAction::getAxis(
    ) const
    {
        return reinterpret_cast< const RawControllerAxis & >( *fgRawControllerAxisGetAxis( &**this ) );
    }

    inline int RawControllerAxisAction::getValue(
    ) const
    {
        return fgRawControllerAxisGetValue( &**this );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_AXISACTION_H
