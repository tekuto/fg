﻿#ifndef FG_CONTROLLER_RAW_ACTION_H
#define FG_CONTROLLER_RAW_ACTION_H

#include "fg/def/controller/raw/action.h"
#include "fg/def/controller/raw/connectaction.h"
#include "fg/def/controller/raw/buttonaction.h"
#include "fg/def/controller/raw/axisaction.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const FgRawControllerConnectAction
        , fgRawControllerActionGetConnectAction(
            const FgRawControllerAction *
        )
    )

    FG_FUNCTION_PTR(
        const FgRawControllerButtonAction
        , fgRawControllerActionGetButtonAction(
            const FgRawControllerAction *
        )
    )

    FG_FUNCTION_PTR(
        const FgRawControllerAxisAction
        , fgRawControllerActionGetAxisAction(
            const FgRawControllerAction *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const RawControllerConnectAction * RawControllerAction::getConnectAction(
    ) const
    {
        return reinterpret_cast< const RawControllerConnectAction * >( fgRawControllerActionGetConnectAction( &**this ) );
    }

    inline const RawControllerButtonAction * RawControllerAction::getButtonAction(
    ) const
    {
        return reinterpret_cast< const RawControllerButtonAction * >( fgRawControllerActionGetButtonAction( &**this ) );
    }

    inline const RawControllerAxisAction * RawControllerAction::getAxisAction(
    ) const
    {
        return reinterpret_cast< const RawControllerAxisAction * >( fgRawControllerActionGetAxisAction( &**this ) );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_ACTION_H
