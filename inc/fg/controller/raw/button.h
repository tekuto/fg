﻿#ifndef FG_CONTROLLER_RAW_BUTTON_H
#define FG_CONTROLLER_RAW_BUTTON_H

#include "fg/def/controller/raw/button.h"
#include "fg/def/controller/raw/id.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgRawControllerButton
        , fgRawControllerButtonCreate(
            const FgRawControllerID *
            , size_t
        )
    )

    FG_FUNCTION_VOID(
        fgRawControllerButtonDestroy(
            FgRawControllerButton *
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerButton
        , fgRawControllerButtonClone(
            const FgRawControllerButton *
        )
    )

    FG_FUNCTION_PTR(
        const FgRawControllerID
        , fgRawControllerButtonGetID(
            const FgRawControllerButton *
        )
    )

    FG_FUNCTION_NUM(
        size_t
        , fgRawControllerButtonGetIndex(
            const FgRawControllerButton *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerButtonEquals(
            const FgRawControllerButton *
            , const FgRawControllerButton *
        )
    )

    FG_FUNCTION_BOOL(
        fgRawControllerButtonLess(
            const FgRawControllerButton *
            , const FgRawControllerButton *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline RawControllerButton::ConstUnique RawControllerButton::create(
        const RawControllerID & _ID
        , std::size_t           _index
    )
    {
        return fgRawControllerButtonCreate(
            &*_ID
            , _index
        );
    }

    inline void RawControllerButton::destroy(
        RawControllerButton *   _this
    )
    {
        fgRawControllerButtonDestroy( &**_this );
    }

    inline RawControllerButton::ConstUnique RawControllerButton::clone(
    ) const
    {
        return fgRawControllerButtonClone( &**this );
    }

    inline const RawControllerID & RawControllerButton::getID(
    ) const
    {
        return reinterpret_cast< const RawControllerID & >( *fgRawControllerButtonGetID( &**this ) );
    }

    inline std::size_t RawControllerButton::getIndex(
    ) const
    {
        return fgRawControllerButtonGetIndex( &**this );
    }

    inline bool RawControllerButton::operator==(
        const RawControllerButton & _OTHER
    ) const
    {
        return fgRawControllerButtonEquals(
            &**this
            , &*_OTHER
        );
    }

    inline bool RawControllerButton::operator<(
        const RawControllerButton & _OTHER
    ) const
    {
        return fgRawControllerButtonLess(
            &**this
            , &*_OTHER
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_BUTTON_H
