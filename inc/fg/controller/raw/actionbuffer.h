﻿#ifndef FG_CONTROLLER_RAW_ACTIONBUFFER_H
#define FG_CONTROLLER_RAW_ACTIONBUFFER_H

#include "fg/def/controller/raw/actionbuffer.h"
#include "fg/def/controller/raw/action.h"
#include "fg/util/import.h"

#include <stddef.h>

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_NUM(
        size_t
        , fgRawControllerActionBufferGetSize(
            const FgRawControllerActionBuffer *
        )
    )

    FG_FUNCTION_PTR(
        const FgRawControllerAction
        , fgRawControllerActionBufferGetAction(
            const FgRawControllerActionBuffer *
            , size_t
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline std::size_t RawControllerActionBuffer::getSize(
    ) const
    {
        return fgRawControllerActionBufferGetSize( &**this );
    }

    inline const RawControllerAction & RawControllerActionBuffer::getAction(
        std::size_t _index
    ) const
    {
        return reinterpret_cast< const RawControllerAction & >(
            *fgRawControllerActionBufferGetAction(
                &**this
                , _index
            )
        );
    }
}

#endif  // __cplusplus

#endif  // FG_CONTROLLER_RAW_ACTIONBUFFER_H
