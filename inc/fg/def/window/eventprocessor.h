﻿#ifndef FG_DEF_WINDOW_EVENTPROCESSOR_H
#define FG_DEF_WINDOW_EVENTPROCESSOR_H

struct FgWindowEventProcessor;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    class Window;
    class WindowEventManagers;

    class WindowEventProcessor : public UniqueWrapper< WindowEventProcessor, FgWindowEventProcessor >
    {
    public:
        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , Window &
            , WindowEventManagers &
        );

        static void destroy(
            WindowEventProcessor *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_WINDOW_EVENTPROCESSOR_H
