﻿#ifndef FG_DEF_WINDOW_PAINTEVENT_H
#define FG_DEF_WINDOW_PAINTEVENT_H

struct FgWindowPaintEventData;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/core/state/eventbackground.h"
#include "fg/def/common/unique.h"

namespace fg {
    class Window;

    template< typename >
    class StateEventManager;

    template< typename >
    class StateEventRegisterManager;

    class WindowPaintEventData : public UniqueWrapper< WindowPaintEventData, FgWindowPaintEventData >
    {
    public:
        Window & getWindow(
        );
    };

    template< typename STATE_DATA_T = void >
    using WindowPaintEvent = StateEvent<
        STATE_DATA_T
        , WindowPaintEventData
    >;

    template< typename STATE_DATA_T = void >
    using WindowPaintEventBackground = StateEventBackground<
        STATE_DATA_T
        , WindowPaintEventData
    >;

    using WindowPaintEventManager = StateEventManager< WindowPaintEventData >;

    using WindowPaintEventRegisterManager = StateEventRegisterManager< WindowPaintEventData >;
}

#endif  // __cplusplus

#endif  // FG_DEF_WINDOW_PAINTEVENT_H
