﻿#ifndef FG_DEF_WINDOW_WINDOW_H
#define FG_DEF_WINDOW_WINDOW_H

struct FgWindow;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class Window : public UniqueWrapper< Window, FgWindow >
    {
    public:
        static Unique create(
            const char *
            , int
            , int
        );

        static void destroy(
            Window *
        );

        void repaint(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_WINDOW_WINDOW_H
