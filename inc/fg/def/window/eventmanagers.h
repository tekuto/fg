﻿#ifndef FG_DEF_WINDOW_EVENTMANAGERS_H
#define FG_DEF_WINDOW_EVENTMANAGERS_H

struct FgWindowEventManagers;

#ifdef  __cplusplus

#include "fg/def/window/paintevent.h"
#include "fg/def/window/closeevent.h"
#include "fg/def/common/unique.h"

namespace fg {
    class WindowEventManagers : public UniqueWrapper< WindowEventManagers, FgWindowEventManagers >
    {
    public:
        static Unique create(
        );

        static void destroy(
            WindowEventManagers *
        );

        WindowPaintEventManager & getPaintEventManager(
        );

        WindowCloseEventManager & getCloseEventManager(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_WINDOW_EVENTMANAGERS_H
