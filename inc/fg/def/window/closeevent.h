﻿#ifndef FG_DEF_WINDOW_CLOSEEVENT_H
#define FG_DEF_WINDOW_CLOSEEVENT_H

struct FgWindowCloseEventData;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/core/state/eventbackground.h"
#include "fg/def/common/unique.h"

namespace fg {
    class Window;

    template< typename >
    class StateEventManager;

    template< typename >
    class StateEventRegisterManager;

    class WindowCloseEventData : public UniqueWrapper< WindowCloseEventData, FgWindowCloseEventData >
    {
    public:
        Window & getWindow(
        );
    };

    template< typename STATE_DATA_T = void >
    using WindowCloseEvent = StateEvent<
        STATE_DATA_T
        , WindowCloseEventData
    >;

    template< typename STATE_DATA_T = void >
    using WindowCloseEventBackground = StateEventBackground<
        STATE_DATA_T
        , WindowCloseEventData
    >;

    using WindowCloseEventManager = StateEventManager< WindowCloseEventData >;

    using WindowCloseEventRegisterManager = StateEventRegisterManager< WindowCloseEventData >;
}

#endif  // __cplusplus

#endif  // FG_DEF_WINDOW_CLOSEEVENT_H
