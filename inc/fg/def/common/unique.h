﻿#ifndef FG_DEF_COMMON_UNIQUE_H
#define FG_DEF_COMMON_UNIQUE_H

#ifdef  __cplusplus

#include <memory>

namespace fg {
    template< typename UNIQUE_WRAPPER_T >
    class ConstUnique
    {
    public:
        using Interface = typename UNIQUE_WRAPPER_T::Interface;
        using Impl = typename UNIQUE_WRAPPER_T::Impl;

    private:
        struct Free
        {
            void operator()(
                Interface * _interfacePtr
            ) const
            {
                Interface::destroy( _interfacePtr );
            }
        };

        using UniquePtr = std::unique_ptr<
            Interface
            , Free
        >;

        UniquePtr   uniquePtr;

    public:
        ConstUnique(
        ) = default;

        ConstUnique(
            Impl *  _implPtr
        )
            : uniquePtr( reinterpret_cast< Interface * >( _implPtr ) )
        {
        }

        void destroy(
        )
        {
            this->uniquePtr.reset();
        }

        const Interface * get(
        ) const
        {
            return this->uniquePtr.get();
        }

        const Interface & operator*(
        ) const
        {
            return *( this->get() );
        }

        const Interface * operator->(
        ) const
        {
            return this->get();
        }

        const Interface * release(
        )
        {
            return this->uniquePtr.release();
        }
    };

    template< typename UNIQUE_WRAPPER_T >
    class Unique : public ConstUnique< UNIQUE_WRAPPER_T >
    {
    public:
        using Interface = typename UNIQUE_WRAPPER_T::Interface;
        using Impl = typename UNIQUE_WRAPPER_T::Impl;

        Unique(
        ) = default;

        Unique(
            Impl *  _implPtr
        )
            : ConstUnique< UNIQUE_WRAPPER_T >( _implPtr )
        {
        }

        Interface * get(
        ) const
        {
            return const_cast< Interface * >( this->ConstUnique< UNIQUE_WRAPPER_T >::get() );
        }

        Interface & operator*(
        ) const
        {
            return const_cast< Interface & >( this->ConstUnique< UNIQUE_WRAPPER_T >::operator*() );
        }

        Interface * operator->(
        ) const
        {
            return const_cast< Interface * >( this->ConstUnique< UNIQUE_WRAPPER_T >::operator->() );
        }

        Interface * release(
        )
        {
            return const_cast< Interface * >( this->ConstUnique< UNIQUE_WRAPPER_T >::release() );
        }
    };

    template<
        typename INTERFACE_T
        , typename IMPL_T = INTERFACE_T
    >
    class UniqueWrapper
    {
    public:
        using Interface = INTERFACE_T;
        using Impl = IMPL_T;

        using ConstUnique = fg::ConstUnique< Interface >;
        using Unique = fg::Unique< Interface >;

        static void destroy(
            Interface * _interfacePtr
        )
        {
            delete reinterpret_cast< Impl * >( _interfacePtr );
        }

        auto & operator*(
        ) const
        {
            return reinterpret_cast< const Impl & >( *this );
        }

        auto & operator*(
        )
        {
            return reinterpret_cast< Impl & >( *this );
        }

        auto operator->(
        ) const
        {
            return reinterpret_cast< const Impl * >( this );
        }

        auto operator->(
        )
        {
            return reinterpret_cast< Impl * >( this );
        }
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_COMMON_UNIQUE_H
