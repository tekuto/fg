﻿#ifndef FG_DEF_COMMON_MOTION_H
#define FG_DEF_COMMON_MOTION_H

#ifdef  __cplusplus

#include <vector>
#include <stdexcept>
#include <cstddef>

namespace fg {
    template< typename DURATION_T >
    class MotionBase_
    {
    public:
        using Duration = DURATION_T;
    };

    template< typename DURATION_T >
    class Motion_ : public MotionBase_< DURATION_T >
    {
    public:
        using Duration = typename MotionBase_< DURATION_T >::Duration;

    protected:
        const Duration  DURATION;
        const Duration  BEGIN_TIME;
        const Duration  END_TIME;

        Duration    currentTime;

        Motion_(
            const DURATION_T &      _DURATION
            , const DURATION_T &    _BEGIN_TIME
        )
            : DURATION( _DURATION )
            , BEGIN_TIME( _BEGIN_TIME )
            , END_TIME( _BEGIN_TIME + _DURATION )
            , currentTime( Duration() )
        {
            if( _DURATION < Duration( 0 ) ) {
                throw std::runtime_error( "DURATIONが0未満" );
            }
        }

    public:
        void initialize(
        )
        {
            this->currentTime = Duration();
        }

        const Duration & getBeginTime(
        ) const
        {
            return this->BEGIN_TIME;
        }

        const Duration & getDuration(
        ) const
        {
            return this->DURATION;
        }

        Duration getCurrentTime(
        ) const
        {
            return this->currentTime;
        }

        void setCurrentTime(
            const Duration &    _TIME
        )
        {
            this->currentTime = _TIME;
        }

        void update(
            const Duration &    _UPDATE_TIME
        )
        {
            if( this->currentTime >= this->END_TIME ) {
                return;
            }

            this->currentTime += _UPDATE_TIME;
        }
    };

    template<
        typename RESULT_T
        , typename DURATION_T
    >
    struct MotionDurationCast
    {
        RESULT_T operator()(
            const DURATION_T &  _DURATION
        ) const
        {
            return static_cast< RESULT_T >( _DURATION );
        }
    };

    template<
        typename DURATION_T
        , typename VALUE_T
        , typename DURATION_CAST_T = MotionDurationCast< float, DURATION_T >
    >
    class Motion : public Motion_< DURATION_T >
    {
    public:
        using Duration = typename Motion_< DURATION_T >::Duration;

    private:
        const VALUE_T   BEGIN_VALUE;
        const VALUE_T   END_VALUE;
        const VALUE_T   DIFF_VALUE;

    public:
        Motion(
            const VALUE_T &         _BEGIN_VALUE
            , const VALUE_T &       _END_VALUE
            , const DURATION_T &    _DURATION
        )
            : Motion(
                _BEGIN_VALUE
                , _END_VALUE
                , _DURATION
                , DURATION_T()
            )
        {
        }

        Motion(
            const VALUE_T &         _BEGIN_VALUE
            , const VALUE_T &       _END_VALUE
            , const DURATION_T &    _DURATION
            , const DURATION_T &    _BEGIN_TIME
        )
            : Motion_< DURATION_T >(
                _DURATION
                , _BEGIN_TIME
            )
            , BEGIN_VALUE( _BEGIN_VALUE )
            , END_VALUE( _END_VALUE )
            , DIFF_VALUE( _END_VALUE - _BEGIN_VALUE )
        {
        }

        const VALUE_T & getBeginValue(
        ) const
        {
            return this->BEGIN_VALUE;
        }

        const VALUE_T & getEndValue(
        ) const
        {
            return this->END_VALUE;
        }

        VALUE_T calc(
        ) const
        {
            if( this->currentTime >= this->END_TIME ) {
                return this->END_VALUE;
            }

            if( this->currentTime <= this->BEGIN_TIME ) {
                return this->BEGIN_VALUE;
            }

            const auto  ELAPSED_TIME = this->currentTime - this->BEGIN_TIME;
            const auto  PROGRESS = DURATION_CAST_T()( ELAPSED_TIME ) / DURATION_CAST_T()( this->DURATION );

            return this->BEGIN_VALUE + this->DIFF_VALUE * PROGRESS;
        }
    };

    template< typename DURATION_T >
    class MotionLoop_ : public MotionBase_< DURATION_T >
    {
    public:
        using Duration = typename MotionBase_< DURATION_T >::Duration;

    protected:
        const Duration  DURATION;
        const Duration  INITIALIZE_TIME;

        Duration    currentTime;

        MotionLoop_(
            const DURATION_T &      _DURATION
            , const DURATION_T &    _INITIALIZE_TIME
        )
            : DURATION( _DURATION )
            , INITIALIZE_TIME( _INITIALIZE_TIME )
            , currentTime( _INITIALIZE_TIME )
        {
            if( _DURATION <= Duration( 0 ) ) {
                throw std::runtime_error( "DURATIONが0以下" );
            }
        }

    public:
        void initialize(
        )
        {
            this->currentTime = this->INITIALIZE_TIME;
        }

        const Duration & getDuration(
        ) const
        {
            return this->DURATION;
        }

        Duration getCurrentTime(
        ) const
        {
            return this->currentTime;
        }

        void setCurrentTime(
            const Duration &    _TIME
        )
        {
            this->currentTime = _TIME;
        }

        void update(
            const Duration &    _UPDATE_TIME
        )
        {
            auto    currentTime = _UPDATE_TIME;
            while( currentTime >= this->DURATION ) {
                currentTime -= this->DURATION;
            }

            currentTime += this->currentTime;
            if( currentTime >= this->DURATION ) {
                currentTime -= this->DURATION;
            }

            this->currentTime = currentTime;
        }
    };

    template<
        typename DURATION_T
        , typename VALUE_T
        , typename DURATION_CAST_T = MotionDurationCast< float, DURATION_T >
    >
    class MotionLoop : public MotionLoop_< DURATION_T >
    {
    public:
        using Duration = typename MotionLoop_< DURATION_T >::Duration;

    private:
        const VALUE_T   BEGIN_VALUE;
        const VALUE_T   END_VALUE;
        const VALUE_T   DIFF_VALUE;

    public:
        MotionLoop(
            const VALUE_T &         _BEGIN_VALUE
            , const VALUE_T &       _END_VALUE
            , const DURATION_T &    _DURATION
        )
            : MotionLoop(
                _BEGIN_VALUE
                , _END_VALUE
                , _DURATION
                , Duration()
            )
        {
        }

        MotionLoop(
            const VALUE_T &         _BEGIN_VALUE
            , const VALUE_T &       _END_VALUE
            , const DURATION_T &    _DURATION
            , const DURATION_T &    _INITIALIZE_TIME
        )
            : MotionLoop_< DURATION_T >(
                _DURATION
                , _INITIALIZE_TIME
            )
            , BEGIN_VALUE( _BEGIN_VALUE )
            , END_VALUE( _END_VALUE )
            , DIFF_VALUE( _END_VALUE - _BEGIN_VALUE )
        {
        }

        const VALUE_T & getBeginValue(
        ) const
        {
            return this->BEGIN_VALUE;
        }

        const VALUE_T & getEndValue(
        ) const
        {
            return this->END_VALUE;
        }

        VALUE_T calc(
        ) const
        {
            const auto  PROGRESS = DURATION_CAST_T()( this->currentTime ) / DURATION_CAST_T()( this->DURATION );

            return this->BEGIN_VALUE + this->DIFF_VALUE * PROGRESS;
        }
    };

    template< typename DURATION_T >
    class MotionRoundTrip_ : public MotionBase_< DURATION_T >
    {
    public:
        using Duration = typename MotionBase_< DURATION_T >::Duration;

    protected:
        const Duration  DURATION;
        const Duration  INITIALIZE_TIME;
        const bool      INITIALIZE_DIRECTION;

        Duration    currentTime;
        bool        direction;

        MotionRoundTrip_(
            const DURATION_T &      _DURATION
            , const DURATION_T &    _INITIALIZE_TIME
            , bool                  _initializeDirection
        )
            : DURATION( _DURATION )
            , INITIALIZE_TIME(
                _initializeDirection == true
                    ? _INITIALIZE_TIME
                    : _DURATION - _INITIALIZE_TIME
            )
            , INITIALIZE_DIRECTION( _initializeDirection )
            , currentTime( this->INITIALIZE_TIME )
            , direction( this->INITIALIZE_DIRECTION )
        {
            if( _DURATION <= Duration( 0 ) ) {
                throw std::runtime_error( "DURATIONが0以下" );
            }
        }

    public:
        void initialize(
        )
        {
            this->currentTime = this->INITIALIZE_TIME;
            this->direction = this->INITIALIZE_DIRECTION;
        }

        const Duration & getDuration(
        ) const
        {
            return this->DURATION;
        }

        Duration getCurrentTime(
        ) const
        {
            return this->direction == true
                ? this->currentTime
                : this->DURATION - this->currentTime
            ;
        }

        bool getDirection(
        ) const
        {
            return this->direction;
        }

        void setCurrentTime(
            const Duration &    _TIME
            , bool              _direction
        )
        {
            this->currentTime = _direction == true
                ? _TIME
                : this->DURATION - _TIME
            ;
            this->direction = _direction;
        }

        void update(
            const Duration &    _UPDATE_TIME
        )
        {
            auto    currentTime = _UPDATE_TIME;
            auto    direction = this->direction;
            while( currentTime >= this->DURATION ) {
                currentTime -= this->DURATION;
                direction = !direction;
            }

            currentTime += this->currentTime;
            if( currentTime >= this->DURATION ) {
                currentTime -= this->DURATION;
                direction = !direction;
            }

            this->currentTime = currentTime;
            this->direction = direction;
        }
    };

    template<
        typename DURATION_T
        , typename VALUE_T
        , typename DURATION_CAST_T = MotionDurationCast< float, DURATION_T >
    >
    class MotionRoundTrip : public MotionRoundTrip_< DURATION_T >
    {
    public:
        using Duration = typename MotionRoundTrip_< DURATION_T >::Duration;

    private:
        const VALUE_T   BEGIN_VALUE;
        const VALUE_T   END_VALUE;
        const VALUE_T   DIFF_VALUE;

    public:
        MotionRoundTrip(
            const VALUE_T &         _BEGIN_VALUE
            , const VALUE_T &       _END_VALUE
            , const DURATION_T &    _DURATION
        )
            : MotionRoundTrip(
                _BEGIN_VALUE
                , _END_VALUE
                , _DURATION
                , Duration()
                , true
            )
        {
        }

        MotionRoundTrip(
            const VALUE_T &         _BEGIN_VALUE
            , const VALUE_T &       _END_VALUE
            , const DURATION_T &    _DURATION
            , const DURATION_T &    _INITIALIZE_TIME
            , bool                  _initializeDirection
        )
            : MotionRoundTrip_< DURATION_T >(
                _DURATION
                , _INITIALIZE_TIME
                , _initializeDirection
            )
            , BEGIN_VALUE( _BEGIN_VALUE )
            , END_VALUE( _END_VALUE )
            , DIFF_VALUE( _END_VALUE - _BEGIN_VALUE )
        {
        }

        const VALUE_T & getBeginValue(
        ) const
        {
            return this->BEGIN_VALUE;
        }

        const VALUE_T & getEndValue(
        ) const
        {
            return this->END_VALUE;
        }

        VALUE_T calc(
        ) const
        {
            const auto  ELAPSED_TIME = this->direction == true
                ? this->currentTime
                : this->DURATION - this->currentTime
            ;
            const auto  PROGRESS = DURATION_CAST_T()( ELAPSED_TIME ) / DURATION_CAST_T()( this->DURATION );

            return this->BEGIN_VALUE + this->DIFF_VALUE * PROGRESS;
        }
    };

    template< typename DURATION_T >
    class MotionGroup : public MotionBase_< DURATION_T >
    {
    public:
        using Duration = typename MotionBase_< DURATION_T >::Duration;

    private:
        using MotionPtrs = std::vector< Motion_< DURATION_T > * >;
        using MotionLoopPtrs = std::vector< MotionLoop_< DURATION_T > * >;
        using MotionRoundTripPtrs = std::vector< MotionRoundTrip_< DURATION_T > * >;
        using MotionGroupPtrs = std::vector< MotionGroup< DURATION_T > * >;

        MotionPtrs          motionPtrs;
        MotionLoopPtrs      motionLoopPtrs;
        MotionRoundTripPtrs motionRoundTripPtrs;
        MotionGroupPtrs     motionGroupPtrs;

        template< template< typename > typename MOTION_T >
        constexpr static std::size_t getMotionPtrsSize(
        )
        {
            return 0;
        }

        template<
            template< typename > typename MOTION_T
            , typename ... ARGS_T
        >
        constexpr static std::size_t getMotionPtrsSize(
            const MotionBase_< DURATION_T > &
            , const ARGS_T & ...                _ARGS
        )
        {
            return getMotionPtrsSize< MOTION_T >( _ARGS ... );
        }

        template<
            template< typename > typename MOTION_T
            , typename ... ARGS_T
        >
        constexpr static std::size_t getMotionPtrsSize(
            const MOTION_T< DURATION_T > &
            , const ARGS_T & ...            _ARGS
        )
        {
            return getMotionPtrsSize< MOTION_T >( _ARGS ... ) + 1;
        }

        template<
            template< typename > typename MOTION_T
            , typename MOTION_PTRS
        >
        static void appendMotion(
            const MOTION_PTRS &
        )
        {
        }

        template<
            template< typename > typename MOTION_T
            , typename MOTION_PTRS
            , typename ... ARGS_T
        >
        static void appendMotion(
            MOTION_PTRS &                       _motionPtrs
            , const MotionBase_< DURATION_T > &
            , ARGS_T & ...                      _args
        )
        {
            appendMotion< MOTION_T >(
                _motionPtrs
                , _args ...
            );
        }

        template<
            template< typename > typename MOTION_T
            , typename MOTION_PTRS
            , typename ... ARGS_T
        >
        static void appendMotion(
            MOTION_PTRS &               _motionPtrs
            , MOTION_T< DURATION_T > &  _motion
            , ARGS_T & ...              _args
        )
        {
            _motionPtrs.push_back( &_motion );

            appendMotion< MOTION_T >(
                _motionPtrs
                , _args ...
            );
        }

        template<
            template< typename > typename MOTION_T
            , typename MOTION_PTRS
            , typename ... ARGS_T
        >
        static MOTION_PTRS createMotionPtrs(
            ARGS_T & ...    _args
        )
        {
            auto    motionPtrs = MOTION_PTRS();

            motionPtrs.reserve( getMotionPtrsSize< MOTION_T >( _args ... ) );

            appendMotion< MOTION_T >(
                motionPtrs
                , _args ...
            );

            return motionPtrs;
        }

        template< typename MOTION_PTRS_T >
        static void initialize(
            MOTION_PTRS_T & _motionPtrs
        )
        {
            for( auto & motionPtr : _motionPtrs ) {
                motionPtr->initialize();
            }
        }

        template< typename MOTION_PTRS_T >
        static void update(
            MOTION_PTRS_T &     _motionPtrs
            , const Duration &  _UPDATE_TIME
        )
        {
            for( auto & motionPtr : _motionPtrs ) {
                motionPtr->update( _UPDATE_TIME );
            }
        }

    public:
        template< typename ... ARGS_T >
        MotionGroup(
            ARGS_T & ...    _args
        )
            : motionPtrs( createMotionPtrs< Motion_, MotionPtrs >( _args ... ) )
            , motionLoopPtrs( createMotionPtrs< MotionLoop_, MotionLoopPtrs >( _args ... ) )
            , motionRoundTripPtrs( createMotionPtrs< MotionRoundTrip_, MotionRoundTripPtrs >( _args ... ) )
            , motionGroupPtrs( createMotionPtrs< MotionGroup, MotionGroupPtrs >( _args ... ) )
        {
        }

        void initialize(
        )
        {
            initialize( this->motionPtrs );
            initialize( this->motionLoopPtrs );
            initialize( this->motionRoundTripPtrs );
            initialize( this->motionGroupPtrs );
        }

        void update(
            const Duration &    _UPDATE_TIME
        )
        {
            update(
                this->motionPtrs
                , _UPDATE_TIME
            );

            update(
                this->motionLoopPtrs
                , _UPDATE_TIME
            );

            update(
                this->motionRoundTripPtrs
                , _UPDATE_TIME
            );

            update(
                this->motionGroupPtrs
                , _UPDATE_TIME
            );
        }
    };

    template< typename TIME_POINT_T >
    class MotionManager
    {
    public:
        using TimePoint = TIME_POINT_T;

        using Duration = decltype( *static_cast< const TimePoint * >( nullptr ) - *static_cast< const TimePoint * >( nullptr ) );

    private:
        MotionGroup< Duration > motionGroup;

        TIME_POINT_T    prevTimePoint;

    public:
        template< typename ... ARGS_T >
        MotionManager(
            ARGS_T & ...    _args
        )
            : MotionManager(
                TIME_POINT_T()
                , _args ...
            )
        {
        }

        template< typename ... ARGS_T >
        MotionManager(
            const TIME_POINT_T &    _INITIALIZE_TIME_POINT
            , ARGS_T & ...          _args
        )
            : motionGroup( _args ... )
            , prevTimePoint( _INITIALIZE_TIME_POINT )
        {
        }

        void initialize(
            const TIME_POINT_T &    _INITIALIZE_TIME_POINT
        )
        {
            this->prevTimePoint = _INITIALIZE_TIME_POINT;
        }

        void initializeAll(
            const TIME_POINT_T &    _INITIALIZE_TIME_POINT
        )
        {
            this->initialize( _INITIALIZE_TIME_POINT );

            this->motionGroup.initialize();
        }

        const TIME_POINT_T & getPrevTimePoint(
        ) const
        {
            return this->prevTimePoint;
        }

        void update(
            const TIME_POINT_T &    _CURRENT_TIME_POINT
        )
        {
            this->motionGroup.update( _CURRENT_TIME_POINT - this->prevTimePoint );

            this->prevTimePoint = _CURRENT_TIME_POINT;
        }
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_COMMON_MOTION_H
