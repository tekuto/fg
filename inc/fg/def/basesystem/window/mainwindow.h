﻿#ifndef FG_DEF_BASESYSTEM_WINDOW_MAINWINDOW_H
#define FG_DEF_BASESYSTEM_WINDOW_MAINWINDOW_H

#ifdef  __cplusplus

namespace fg {
    class Window;

    template< typename >
    class CreatingState;

    template< typename >
    class State;

    template< typename STATE_DATA_T >
    Window & getMainWindow(
        CreatingState< STATE_DATA_T > &
    );

    template< typename STATE_DATA_T >
    Window & getMainWindow(
        State< STATE_DATA_T > &
    );
}

#endif  // __cplusplus

#endif  // FG_DEF_BASESYSTEM_WINDOW_MAINWINDOW_H
