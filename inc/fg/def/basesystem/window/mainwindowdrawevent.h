﻿#ifndef FG_DEF_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H
#define FG_DEF_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H

struct FgMainWindowDrawEventData;

struct FgMainWindowDrawEventRegisterManager;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/common/unique.h"

namespace fg {
    class Window;

    template< typename >
    class CreatingState;

    template< typename >
    class State;

    class MainWindowDrawEventData : public UniqueWrapper< MainWindowDrawEventData, FgMainWindowDrawEventData >
    {
    public:
        Window & getWindow(
        );
    };

    template< typename STATE_DATA_T = void >
    using MainWindowDrawEvent = StateEvent<
        STATE_DATA_T
        , MainWindowDrawEventData
    >;

    class MainWindowDrawEventRegisterManager : public UniqueWrapper< MainWindowDrawEventRegisterManager, FgMainWindowDrawEventRegisterManager >
    {
    public:
        template< typename STATE_DATA_T >
        static Unique create(
            CreatingState< STATE_DATA_T > &
            , typename MainWindowDrawEvent< STATE_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , typename MainWindowDrawEvent< STATE_DATA_T >::Proc
        );

        static void destroy(
            MainWindowDrawEventRegisterManager *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H
