﻿#ifndef FG_DEF_BASESYSTEM_CONTROLLER_RAW_EVENT_H
#define FG_DEF_BASESYSTEM_CONTROLLER_RAW_EVENT_H

struct FgRawControllerEventData;

struct FgRawControllerEventRegisterManager;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/core/state/eventbackground.h"
#include "fg/def/common/unique.h"

namespace fg {
    class RawControllerActionBuffer;

    template< typename >
    class CreatingState;

    template< typename >
    class State;

    class RawControllerEventData : public UniqueWrapper< RawControllerEventData, FgRawControllerEventData >
    {
    public:
        const RawControllerActionBuffer & getActionBuffer(
        ) const;
    };

    template< typename STATE_DATA_T = void >
    using RawControllerEvent = StateEvent<
        STATE_DATA_T
        , RawControllerEventData
    >;

    template< typename STATE_DATA_T = void >
    using RawControllerEventBackground = StateEventBackground<
        STATE_DATA_T
        , RawControllerEventData
    >;

    class RawControllerEventRegisterManager : public UniqueWrapper< RawControllerEventRegisterManager, FgRawControllerEventRegisterManager >
    {
    public:
        template< typename STATE_DATA_T >
        static Unique create(
            CreatingState< STATE_DATA_T > &
            , typename RawControllerEvent< STATE_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , typename RawControllerEvent< STATE_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static Unique create(
            CreatingState< STATE_DATA_T > &
            , typename RawControllerEventBackground< STATE_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , typename RawControllerEventBackground< STATE_DATA_T >::Proc
        );

        static void destroy(
            RawControllerEventRegisterManager *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_BASESYSTEM_CONTROLLER_RAW_EVENT_H
