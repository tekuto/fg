﻿#ifndef FG_DEF_GL_CONTEXTARGS_H
#define FG_DEF_GL_CONTEXTARGS_H

struct FgGLContextArgs;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class GLContextArgs : public UniqueWrapper< GLContextArgs, FgGLContextArgs >
    {
    public:
        static Unique create(
        );

        static Unique createBlank(
        );

        static void destroy(
            GLContextArgs *
        );

        bool isExistsRedSize(
        ) const;

        int getRedSize(
        ) const;

        void setRedSize(
            int
        );

        void clearRedSize(
        );

        bool isExistsGreenSize(
        ) const;

        int getGreenSize(
        ) const;

        void setGreenSize(
            int
        );

        void clearGreenSize(
        );

        bool isExistsBlueSize(
        ) const;

        int getBlueSize(
        ) const;

        void setBlueSize(
            int
        );

        void clearBlueSize(
        );

        bool isExistsAlphaSize(
        ) const;

        int getAlphaSize(
        ) const;

        void setAlphaSize(
            int
        );

        void clearAlphaSize(
        );

        bool isExistsDepthSize(
        ) const;

        int getDepthSize(
        ) const;

        void setDepthSize(
            int
        );

        void clearDepthSize(
        );

        bool isExistsStencilSize(
        ) const;

        int getStencilSize(
        ) const;

        void setStencilSize(
            int
        );

        void clearStencilSize(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_CONTEXTARGS_H
