﻿#ifndef FG_DEF_GL_BUFFERS_H
#define FG_DEF_GL_BUFFERS_H

struct FgGLBuffers;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLBuffers : public UniqueWrapper< GLBuffers, FgGLBuffers >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , GLsizei
        );

        static void destroy(
            GLBuffers *
        );

        GLsizei getSize(
        ) const;

        GLuint getName(
            GLsizei
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_BUFFERS_H
