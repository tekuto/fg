﻿#ifndef FG_DEF_GL_THREADEXECUTOR_H
#define FG_DEF_GL_THREADEXECUTOR_H

struct FgGLThreadExecutor;

#ifdef  __cplusplus

#include "fg/def/gl/thread.h"
#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class CreatingState;

    template< typename >
    class State;

    class GLContext;

    class GLThreadExecutor : public UniqueWrapper< GLThreadExecutor, FgGLThreadExecutor >
    {
    public:
        template< typename STATE_DATA_T >
        static Unique create(
            CreatingState< STATE_DATA_T > &
            , GLContext &
            , typename GLThread< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename USER_DATA_T
        >
        static Unique create(
            CreatingState< STATE_DATA_T > &
            , GLContext &
            , typename GLThread< STATE_DATA_T, USER_DATA_T >::Proc
            , USER_DATA_T &
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , GLContext &
            , typename GLThread< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename USER_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , GLContext &
            , typename GLThread< STATE_DATA_T, USER_DATA_T >::Proc
            , USER_DATA_T &
        );

        static void destroy(
            GLThreadExecutor *
        );

        void execute(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_THREADEXECUTOR_H
