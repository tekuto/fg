﻿#ifndef FG_DEF_GL_BUFFERBINDERARRAY_H
#define FG_DEF_GL_BUFFERBINDERARRAY_H

struct FgGLBufferBinderArray;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;
    class GLBuffers;

    class GLBufferBinderArray : public UniqueWrapper< GLBufferBinderArray, FgGLBufferBinderArray >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , const GLBuffers &
            , GLsizei
        );

        static void destroy(
            GLBufferBinderArray *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_BUFFERBINDERARRAY_H
