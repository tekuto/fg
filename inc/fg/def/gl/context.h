﻿#ifndef FG_DEF_GL_CONTEXT_H
#define FG_DEF_GL_CONTEXT_H

struct FgGLContext;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class Window;
    class GLContextArgs;

    class GLContext : public UniqueWrapper< GLContext, FgGLContext >
    {
    public:
        static Unique create(
            Window &
        );

        static Unique create(
            Window &
            , GLContext &
        );

        static Unique create(
            Window &
            , const GLContextArgs &
        );

        static Unique create(
            Window &
            , GLContext &
            , const GLContextArgs &
        );

        static void destroy(
            GLContext *
        );

        //TODO getDisplay()（仮）に変更する
        Window & getWindow(
        );

        int getBufferSize(
        );

        int getRedSize(
        );

        int getGreenSize(
        );

        int getBlueSize(
        );

        int getAlphaSize(
        );

        int getDepthSize(
        );

        int getStencilSize(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_CONTEXT_H
