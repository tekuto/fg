﻿#ifndef FG_DEF_GL_VERTEXARRAYBINDER_H
#define FG_DEF_GL_VERTEXARRAYBINDER_H

struct FgGLVertexArrayBinder;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;
    class GLVertexArrays;

    class GLVertexArrayBinder : public UniqueWrapper< GLVertexArrayBinder, FgGLVertexArrayBinder >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , const GLVertexArrays &
            , GLsizei
        );

        static void destroy(
            GLVertexArrayBinder *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_VERTEXARRAYBINDER_H
