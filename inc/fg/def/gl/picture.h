﻿#ifndef FG_DEF_GL_PICTURE_H
#define FG_DEF_GL_PICTURE_H

struct FgGLPicture;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class GLPicture : public UniqueWrapper< GLPicture, FgGLPicture >
    {
    public:
        static Unique create(
            const void *
            , std::size_t
            , GLsizei
            , GLsizei
            , GLenum
            , GLenum
        );

        static void destroy(
            GLPicture *
        );

        std::size_t getSize(
        ) const;

        const void * getData(
        ) const;

        GLsizei getWidth(
        ) const;

        GLsizei getHeight(
        ) const;

        GLenum getFormat(
        ) const;

        GLenum getDataType(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_PICTURE_H
