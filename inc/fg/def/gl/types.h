﻿#ifndef FG_DEF_GL_TYPES_H
#define FG_DEF_GL_TYPES_H

#include <stddef.h>
#include <stdint.h>

// GL_VERSION_1_0
typedef unsigned int FgGLenum;
typedef float FgGLfloat;
typedef int FgGLint;
typedef int FgGLsizei;
typedef void FgGLvoid;
typedef unsigned int FgGLbitfield;
typedef double FgGLdouble;
typedef unsigned int FgGLuint;
typedef unsigned char FgGLboolean;
typedef unsigned char FgGLubyte;

// GL_VERSION_1_1
typedef float FgGLclampf;
typedef double FgGLclampd;

// GL_VERSION_1_5
typedef ptrdiff_t FgGLsizeiptr;
typedef ptrdiff_t FgGLintptr;

// GL_VERSION_2_0
typedef char FgGLchar;
typedef short FgGLshort;
typedef signed char FgGLbyte;
typedef unsigned short FgGLushort;

// GL_VERSION_3_0
typedef unsigned short FgGLhalf;

// GL_VERSION_3_2
struct __FgGLsync;
typedef __FgGLsync * FgGLsync;
typedef uint64_t FgGLuint64;
typedef int64_t FgGLint64;

// GL_VERSION_4_3
typedef void ( * FgGLDebugProc )(
    FgGLenum
    , FgGLenum
    , FgGLuint
    , FgGLenum
    , FgGLsizei
    , const FgGLchar *
    , const void *
);

// GL_ARB_bindless_texture
typedef uint64_t FgGLuint64EXT;

// GL_ARB_cl_event
struct __FgGLclcontext;
typedef __FgGLclcontext * FgGLclcontext;
struct __FgGLclevent;
typedef __FgGLclevent * FgGLclevent;

// GL_ARB_debug_output
typedef void ( * FgGLDebugProcARB )(
    FgGLenum
    , FgGLenum
    , FgGLuint
    , FgGLenum
    , FgGLsizei
    , const FgGLchar *
    , const void *
);

// GL_ARB_half_float_pixel
typedef unsigned short FgGLhalfARB;

// GL_ARB_shader_objects
typedef unsigned int FgGLhandleARB; //FIXME __APPLE__の場合void *
typedef char FgGLcharARB;

// GL_ARB_vertex_buffer_object
typedef ptrdiff_t FgGLsizeiptrARB;
typedef ptrdiff_t FgGLintptrARB;

// GL_OES_fixed_point
typedef FgGLint FgGLfixed;

// GL_AMD_debug_output
typedef void ( * FgGLDebugProcAMD )(
    FgGLuint
    , FgGLenum
    , FgGLenum
    , FgGLsizei
    , const FgGLchar *
    , void *
);

// GL_NV_gpu_shader5
typedef int64_t FgGLint64EXT;

// GL_NV_half_float
typedef unsigned short FgGLhalfNV;

// GL_NV_vdpau_interop
typedef FgGLintptr FgGLvdpauSurfaceNV;

#ifdef  __cplusplus

namespace fg {
    // GL_VERSION_1_0
    using GLenum = FgGLenum;
    using GLfloat = FgGLfloat;
    using GLint = FgGLint;
    using GLsizei = FgGLsizei;
    using GLvoid = FgGLvoid;
    using GLbitfield = FgGLbitfield;
    using GLdouble = FgGLdouble;
    using GLuint = FgGLuint;
    using GLboolean = FgGLboolean;
    using GLubyte = FgGLubyte;

    // GL_VERSION_1_1
    using GLclampf = FgGLclampf;
    using GLclampd = FgGLclampd;

    // GL_VERSION_1_5
    using GLsizeiptr = FgGLsizeiptr;
    using GLintptr = FgGLintptr;

    // GL_VERSION_2_0
    using GLchar = FgGLchar;
    using GLshort = FgGLshort;
    using GLbyte = FgGLbyte;
    using GLushort = FgGLushort;

    // GL_VERSION_3_0
    using GLhalf = FgGLhalf;

    // GL_VERSION_3_2
    using __GLsync = __FgGLsync;
    using GLsync = FgGLsync;
    using GLuint64 = FgGLuint64;
    using GLint64 = FgGLint64;

    // GL_VERSION_4_3
    using GLDebugProc = FgGLDebugProc;

    // GL_ARB_bindless_texture
    using GLuint64EXT = FgGLuint64EXT;

    // GL_ARB_cl_event
    using __GLclcontext = __FgGLclcontext;
    using GLclcontext = FgGLclcontext;
    using __GLclevent = __FgGLclevent;
    using GLclevent = FgGLclevent;

    // GL_ARB_debug_output
    using GLDebugProcARB = FgGLDebugProcARB;

    // GL_ARB_half_float_pixel
    using GLhalfARB = FgGLhalfARB;

    // GL_ARB_shader_objects
    using GLhandleARB = FgGLhandleARB;  //FIXME __APPLE__の場合void *
    using GLcharARB = FgGLcharARB;

    // GL_ARB_vertex_buffer_object
    using GLsizeiptrARB = FgGLsizeiptrARB;
    using GLintptrARB = FgGLintptrARB;

    // GL_OES_fixed_point
    using GLfixed = FgGLfixed;

    // GL_AMD_debug_output
    using GLDebugProcAMD = FgGLDebugProcAMD;

    // GL_NV_gpu_shader5
    using GLint64EXT = FgGLint64EXT;

    // GL_NV_half_float
    using GLhalfNV = FgGLhalfNV;

    // GL_NV_vdpau_interop
    using GLvdpauSurfaceNV = FgGLvdpauSurfaceNV;
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_TYPES_H
