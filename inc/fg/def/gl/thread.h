﻿#ifndef FG_DEF_GL_THREAD_H
#define FG_DEF_GL_THREAD_H

struct FgGLThread;

typedef void ( * FgGLThreadProc )(
    FgGLThread *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    class GLContext;

    template<
        typename STATE_DATA_T = void
        , typename USER_DATA_T = void
    >
    class GLThread : public UniqueWrapper< GLThread< STATE_DATA_T, USER_DATA_T >, FgGLThread >
    {
    public:
        using Proc = void ( * )(
            GLThread &
        );

        State< STATE_DATA_T > & getState(
        );

        GLContext & getGLContext(
        );

        USER_DATA_T & getData(
        );
    };

    template< typename STATE_DATA_T >
    class GLThread< STATE_DATA_T, void > : public UniqueWrapper< GLThread< STATE_DATA_T, void >, FgGLThread >
    {
    public:
        using Proc = void ( * )(
            GLThread &
        );

        State< STATE_DATA_T > & getState(
        );

        GLContext & getGLContext(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_THREAD_H
