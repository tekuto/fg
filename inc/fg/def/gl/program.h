﻿#ifndef FG_DEF_GL_PROGRAM_H
#define FG_DEF_GL_PROGRAM_H

struct FgGLProgram;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLProgram : public UniqueWrapper< GLProgram, FgGLProgram >
    {
    public:
        static Unique create(
            GLCurrentContext &
        );

        static void destroy(
            GLProgram *
        );

        GLuint getObject(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_PROGRAM_H
