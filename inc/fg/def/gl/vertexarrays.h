﻿#ifndef FG_DEF_GL_VERTEXARRAYS_H
#define FG_DEF_GL_VERTEXARRAYS_H

struct FgGLVertexArrays;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLVertexArrays : public UniqueWrapper< GLVertexArrays, FgGLVertexArrays >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , GLsizei
        );

        static void destroy(
            GLVertexArrays *
        );

        GLsizei getSize(
        ) const;

        GLuint getName(
            GLsizei
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_VERTEXARRAYS_H
