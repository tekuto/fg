﻿#ifndef FG_DEF_GL_FRAGMENTSHADER_H
#define FG_DEF_GL_FRAGMENTSHADER_H

struct FgGLFragmentShader;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLFragmentShader : public UniqueWrapper< GLFragmentShader, FgGLFragmentShader >
    {
    public:
        static Unique create(
            GLCurrentContext &
        );

        static void destroy(
            GLFragmentShader *
        );

        GLuint getObject(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_FRAGMENTSHADER_H
