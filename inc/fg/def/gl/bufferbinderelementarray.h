﻿#ifndef FG_DEF_GL_BUFFERBINDERELEMENTARRAY_H
#define FG_DEF_GL_BUFFERBINDERELEMENTARRAY_H

struct FgGLBufferBinderElementArray;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;
    class GLBuffers;

    class GLBufferBinderElementArray : public UniqueWrapper< GLBufferBinderElementArray, FgGLBufferBinderElementArray >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , const GLBuffers &
            , GLsizei
        );

        static void destroy(
            GLBufferBinderElementArray *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_BUFFERBINDERELEMENTARRAY_H
