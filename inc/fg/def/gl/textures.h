﻿#ifndef FG_DEF_GL_TEXTURES_H
#define FG_DEF_GL_TEXTURES_H

struct FgGLTextures;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLTextures : public UniqueWrapper< GLTextures, FgGLTextures >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , GLsizei
        );

        static void destroy(
            GLTextures *
        );

        GLsizei getSize(
        ) const;

        GLuint getName(
            GLsizei
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_TEXTURES_H
