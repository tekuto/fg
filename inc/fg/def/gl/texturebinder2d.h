﻿#ifndef FG_DEF_GL_TEXTUREBINDER2D_H
#define FG_DEF_GL_TEXTUREBINDER2D_H

struct FgGLTextureBinder2D;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;
    class GLTextures;

    class GLTextureBinder2D : public UniqueWrapper< GLTextureBinder2D, FgGLTextureBinder2D >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , const GLTextures &
            , GLsizei
        );

        static void destroy(
            GLTextureBinder2D *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_TEXTUREBINDER2D_H
