﻿#ifndef FG_DEF_GL_SAMPLERS_H
#define FG_DEF_GL_SAMPLERS_H

struct FgGLSamplers;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLSamplers : public UniqueWrapper< GLSamplers, FgGLSamplers >
    {
    public:
        static Unique create(
            GLCurrentContext &
            , GLsizei
        );

        static void destroy(
            GLSamplers *
        );

        GLsizei getSize(
        ) const;

        GLuint getName(
            GLsizei
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_SAMPLERS_H
