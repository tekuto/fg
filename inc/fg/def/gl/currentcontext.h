﻿#ifndef FG_DEF_GL_CURRENTCONTEXT_H
#define FG_DEF_GL_CURRENTCONTEXT_H

struct FgGLCurrentContext;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/gl/functions.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLContext;

    class GLCurrentContext : public UniqueWrapper< GLCurrentContext, FgGLCurrentContext >
    {
    public:
        static Unique create(
            GLContext &
        );

        static void destroy(
            GLCurrentContext *
        );

        // GL_VERSION_1_0
#define FgGLenum GLenum
#define FgGLfloat GLfloat
#define FgGLint GLint
#define FgGLsizei GLsizei
#define FgGLvoid GLvoid
#define FgGLbitfield GLbitfield
#define FgGLdouble GLdouble
#define FgGLuint GLuint
#define FgGLboolean GLboolean
#define FgGLubyte GLubyte

        // GL_VERSION_1_1
#define FgGLclampf GLclampf
#define FgGLclampd GLclampd

        // GL_VERSION_1_5
#define FgGLsizeiptr GLsizeiptr
#define FgGLintptr GLintptr

        // GL_VERSION_2_0
#define FgGLchar GLchar
#define FgGLshort GLshort
#define FgGLbyte GLbyte
#define FgGLushort GLushort

        // GL_VERSION_3_0
#define FgGLhalf GLhalf

        // GL_VERSION_3_2
#define __FgGLsync __GLsync
#define FgGLsync GLsync
#define FgGLuint64 GLuint64
#define FgGLint64 GLint64

        // GL_VERSION_4_3
#define FgGLDebugProc GLDebugProc

        // GL_ARB_bindless_texture
#define FgGLuint64EXT GLuint64EXT

        // GL_ARB_cl_event
#define __FgGLclcontext __GLclcontext
#define FgGLclcontext GLclcontext
#define __FgGLclevent __GLclevent
#define FgGLclevent GLclevent

        // GL_ARB_debug_output
#define FgGLDebugProcARB GLDebugProcARB

        // GL_ARB_half_float_pixel
#define FgGLhalfARB GLhalfARB

        // GL_ARB_shader_objects
#define FgGLhandleARB GLhandleARB
#define FgGLcharARB GLcharARB

        // GL_ARB_vertex_buffer_object
#define FgGLsizeiptrARB GLsizeiptrARB
#define FgGLintptrARB GLintptrARB

        // GL_OES_fixed_point
#define FgGLfixed GLfixed

        // GL_AMD_debug_output
#define FgGLDebugProcAMD GLDebugProcAMD

        // GL_NV_gpu_shader5
#define FgGLint64EXT GLint64EXT

        // GL_NV_half_float
#define FgGLhalfNV GLhalfNV

        // GL_NV_vdpau_interop
#define FgGLvdpauSurfaceNV GLvdpauSurfaceNV


#define GL_ARGS( _dummy , ... ) \
        __VA_ARGS__
#define GL_DEFINE_FUNCTION( _name, _returnType, _args ) \
        _returnType gl##_name( GL_ARGS _args );
#define FG_GL_FUNCTION_NUM( _name, _returnType, _args, _values ) \
        GL_DEFINE_FUNCTION( _name, _returnType, _args )
#define FG_GL_FUNCTION_PTR( _name, _returnType, _args, _values ) \
        GL_DEFINE_FUNCTION( _name, _returnType *, _args )
#define FG_GL_FUNCTION_VOID( _name, _args, _values ) \
        GL_DEFINE_FUNCTION( _name, void, _args )

        FG_GL_FUNCTIONS

#undef  FG_GL_FUNCTION_VOID
#undef  FG_GL_FUNCTION_PTR
#undef  FG_GL_FUNCTION_NUM
#undef  GL_DEFINE_FUNCTION
#undef  GL_ARGS


        // GL_VERSION_1_0
#undef  FgGLenum
#undef  FgGLfloat
#undef  FgGLint
#undef  FgGLsizei
#undef  FgGLvoid
#undef  FgGLbitfield
#undef  FgGLdouble
#undef  FgGLuint
#undef  FgGLboolean
#undef  FgGLubyte

        // GL_VERSION_1_1
#undef  FgGLclampf
#undef  FgGLclampd

        // GL_VERSION_1_5
#undef  FgGLsizeiptr
#undef  FgGLintptr

        // GL_VERSION_2_0
#undef  FgGLchar
#undef  FgGLshort
#undef  FgGLbyte
#undef  FgGLushort

        // GL_VERSION_3_0
#undef  FgGLhalf

        // GL_VERSION_3_2
#undef  __FgGLsync
#undef  FgGLsync
#undef  FgGLuint64
#undef  FgGLint64

        // GL_VERSION_4_3
#undef  FgGLDebugProc

        // GL_ARB_bindless_texture
#undef  FgGLuint64EXT

        // GL_ARB_cl_event
#undef  __FgGLclcontext
#undef  FgGLclcontext
#undef  __FgGLclevent
#undef  FgGLclevent

        // GL_ARB_debug_output
#undef  FgGLDebugProcARB

        // GL_ARB_half_float_pixel
#undef  FgGLhalfARB

        // GL_ARB_shader_objects
#undef  FgGLhandleARB
#undef  FgGLcharARB

        // GL_ARB_vertex_buffer_object
#undef  FgGLsizeiptrARB
#undef  FgGLintptrARB

        // GL_OES_fixed_point
#undef  FgGLfixed

        // GL_AMD_debug_output
#undef  FgGLDebugProcAMD

        // GL_NV_gpu_shader5
#undef  FgGLint64EXT

        // GL_NV_half_float
#undef  FgGLhalfNV

        // GL_NV_vdpau_interop
#undef  FgGLvdpauSurfaceNV
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_CURRENTCONTEXT_H
