﻿#ifndef FG_DEF_GL_VERTEXSHADER_H
#define FG_DEF_GL_VERTEXSHADER_H

struct FgGLVertexShader;

#ifdef  __cplusplus

#include "fg/def/gl/types.h"
#include "fg/def/common/unique.h"

namespace fg {
    class GLCurrentContext;

    class GLVertexShader : public UniqueWrapper< GLVertexShader, FgGLVertexShader >
    {
    public:
        static Unique create(
            GLCurrentContext &
        );

        static void destroy(
            GLVertexShader *
        );

        GLuint getObject(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_GL_VERTEXSHADER_H
