﻿#ifndef FG_DEF_CORE_BASESYSTEM_CONTEXT_H
#define FG_DEF_CORE_BASESYSTEM_CONTEXT_H

struct FgBasesystemContext;

typedef void ( * FgBasesystemContextInitializeProc )(
    FgBasesystemContext *
);

typedef void ( * FgBasesystemContextStateDataDestroyProc )(
    void *
);

typedef void ( * FgBasesystemContextWaitEndProc )(
    void *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template< typename STATE_DATA_T >
    class BasesystemContext : public UniqueWrapper< BasesystemContext< STATE_DATA_T >, FgBasesystemContext >
    {
    public:
        using StateDataDestroyProc = void ( * )(
            STATE_DATA_T *
        );

        template< typename DATA_T >
        using WaitEndProc = void ( * )(
            DATA_T &
        );

        void setStateData(
            STATE_DATA_T *
            , StateDataDestroyProc
        );

        template< typename DATA_T >
        void setWaitEndProc(
            WaitEndProc< DATA_T >
            , DATA_T &
        );

        State< STATE_DATA_T > & getState(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_BASESYSTEM_CONTEXT_H
