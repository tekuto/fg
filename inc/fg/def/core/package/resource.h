﻿#ifndef FG_DEF_CORE_PACKAGE_RESOURCE_H
#define FG_DEF_CORE_PACKAGE_RESOURCE_H

struct FgPackageResource;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class ModuleContext;

    template< typename >
    class BasesystemContext;

    template< typename >
    class GameContext;

    class PackageResource : public UniqueWrapper< PackageResource, FgPackageResource >
    {
    public:
        static ConstUnique create(
            const ModuleContext &
            , const char *
        );

        template< typename STATE_DATA_T >
        static ConstUnique create(
            const BasesystemContext< STATE_DATA_T > &
            , const char *
        );

        template< typename STATE_DATA_T >
        static ConstUnique create(
            const GameContext< STATE_DATA_T > &
            , const char *
        );

        static void destroy(
            PackageResource *
        );

        std::size_t getSize(
        ) const;

        const void * getData(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_PACKAGE_RESOURCE_H
