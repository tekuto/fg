﻿#ifndef FG_DEF_CORE_STATE_THREAD_H
#define FG_DEF_CORE_STATE_THREAD_H

struct FgStateThread;

typedef void ( * FgStateThreadProc )(
    FgStateThread *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template<
        typename STATE_DATA_T = void
        , typename USER_DATA_T = void
    >
    class StateThread : public UniqueWrapper< StateThread< STATE_DATA_T, USER_DATA_T >, FgStateThread >
    {
    public:
        using Proc = void ( * )(
            StateThread &
        );

        State< STATE_DATA_T > & getState(
        );

        USER_DATA_T & getData(
        );
    };

    template< typename STATE_DATA_T >
    class StateThread< STATE_DATA_T, void > : public UniqueWrapper< StateThread< STATE_DATA_T, void >, FgStateThread >
    {
    public:
        using Proc = void ( * )(
            StateThread &
        );

        State< STATE_DATA_T > & getState(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_THREAD_H
