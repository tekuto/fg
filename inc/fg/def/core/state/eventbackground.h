﻿#ifndef FG_DEF_CORE_STATE_EVENTBACKGROUND_H
#define FG_DEF_CORE_STATE_EVENTBACKGROUND_H

struct FgStateEventBackground;

typedef void ( * FgStateEventBackgroundProc )(
    FgStateEventBackground *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template<
        typename STATE_DATA_T
        , typename EVENT_DATA_T
    >
    class StateEventBackground : public UniqueWrapper< StateEventBackground< STATE_DATA_T, EVENT_DATA_T >, FgStateEventBackground >
    {
    public:
        using Proc = void ( * )(
            StateEventBackground &
        );

        State< STATE_DATA_T > & getState(
        );

        EVENT_DATA_T & getData(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_EVENTBACKGROUND_H
