﻿#ifndef FG_DEF_CORE_STATE_EVENTMANAGER_H
#define FG_DEF_CORE_STATE_EVENTMANAGER_H

struct FgStateEventManager;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class StateJoiner;

    template< typename EVENT_DATA_T >
    class StateEventManager : public UniqueWrapper< StateEventManager< EVENT_DATA_T >, FgStateEventManager >
    {
    public:
        static typename StateEventManager::Unique create(
        );

        static void destroy(
            StateEventManager< EVENT_DATA_T > *
        );

        void execute(
            StateJoiner &
            , EVENT_DATA_T &
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_EVENTMANAGER_H
