﻿#ifndef FG_DEF_CORE_STATE_REENTEREVENT_H
#define FG_DEF_CORE_STATE_REENTEREVENT_H

struct FgStateReenterEventData;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/common/unique.h"

namespace fg {
    class StateReenterEventData : public UniqueWrapper< StateReenterEventData, FgStateReenterEventData >
    {
    public:
        const void * getPrevDataDestroyProcPtr(
        ) const;

        template< typename STATE_DATA_T >
        bool isPrevState(
        ) const;
    };

    template< typename STATE_DATA_T = void >
    using StateReenterEvent = StateEvent<
        STATE_DATA_T
        , StateReenterEventData
    >;
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_REENTEREVENT_H
