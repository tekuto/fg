﻿#ifndef FG_DEF_CORE_STATE_ENTEREVENT_H
#define FG_DEF_CORE_STATE_ENTEREVENT_H

struct FgStateEnterEventData;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/common/unique.h"

namespace fg {
    class StateEnterEventData : public UniqueWrapper< StateEnterEventData, FgStateEnterEventData >
    {
    };

    template< typename STATE_DATA_T = void >
    using StateEnterEvent = StateEvent<
        STATE_DATA_T
        , StateEnterEventData
    >;
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_ENTEREVENT_H
