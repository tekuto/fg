﻿#ifndef FG_DEF_CORE_STATE_EVENT_H
#define FG_DEF_CORE_STATE_EVENT_H

struct FgStateEvent;

typedef void ( * FgStateEventProc )(
    FgStateEvent *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template<
        typename STATE_DATA_T
        , typename EVENT_DATA_T
    >
    class StateEvent : public UniqueWrapper< StateEvent< STATE_DATA_T, EVENT_DATA_T >, FgStateEvent >
    {
    public:
        using Proc = void ( * )(
            StateEvent &
        );

        State< STATE_DATA_T > & getState(
        );

        EVENT_DATA_T & getData(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_EVENT_H
