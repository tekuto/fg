﻿#ifndef FG_DEF_CORE_STATE_CREATING_H
#define FG_DEF_CORE_STATE_CREATING_H

struct FgCreatingState;

typedef void ( * FgCreatingStateDataDestroyProc )(
    void *
);

#ifdef  __cplusplus

#include "fg/def/core/state/enterevent.h"
#include "fg/def/core/state/exitevent.h"
#include "fg/def/core/state/reenterevent.h"
#include "fg/def/core/state/leaveevent.h"
#include "fg/def/common/unique.h"

#include <functional>

namespace fg {
    template< typename >
    class State;

    template< typename STATE_DATA_T = void >
    class CreatingState : public UniqueWrapper< CreatingState< STATE_DATA_T >, FgCreatingState >
    {
        template< typename >
        friend class State;

        using DataCreateProc = std::function< STATE_DATA_T * ( CreatingState< STATE_DATA_T > & ) >;

        using DataDestroyProc = void ( * )(
            STATE_DATA_T *
        );

        template< typename PREV_STATE_DATA_T >
        static typename CreatingState::Unique create(
            State< PREV_STATE_DATA_T > &
            , const DataCreateProc &
            , DataDestroyProc
        );

        void setData(
            STATE_DATA_T *
            , DataDestroyProc
        );

    public:
        static void destroy(
            CreatingState *
        );

        void setEventProcs(
        );

        template<
            typename EVENT_PROC_T
            , typename ... EVENT_PROCS_T
        >
        void setEventProcs(
            EVENT_PROC_T
            , EVENT_PROCS_T ...
        );

        void setEventProc(
            typename StateEnterEvent< STATE_DATA_T >::Proc
        );

        void setEventProc(
            typename StateExitEvent< STATE_DATA_T >::Proc
        );

        void setEventProc(
            typename StateReenterEvent< STATE_DATA_T >::Proc
        );

        void setEventProc(
            typename StateLeaveEvent< STATE_DATA_T >::Proc
        );

        template< typename BASESYSTEM_T >
        BASESYSTEM_T & getBasesystem_(
        );
    };

    template<>
    class CreatingState<> : public UniqueWrapper< CreatingState<>, FgCreatingState >
    {
        template< typename >
        friend class State;

        using InitializeProc = std::function< void ( CreatingState<> & ) >;

        template< typename PREV_STATE_DATA_T >
        static Unique create(
            State< PREV_STATE_DATA_T > &
            , const InitializeProc &
        );

    public:
        static void destroy(
            CreatingState *
        );

        void setEventProcs(
        );

        template<
            typename EVENT_PROC_T
            , typename ... EVENT_PROCS_T
        >
        void setEventProcs(
            EVENT_PROC_T
            , EVENT_PROCS_T ...
        );

        void setEventProc(
            typename StateEnterEvent<>::Proc
        );

        void setEventProc(
            typename StateExitEvent<>::Proc
        );

        void setEventProc(
            typename StateReenterEvent<>::Proc
        );

        void setEventProc(
            typename StateLeaveEvent<>::Proc
        );

        template< typename BASESYSTEM_T >
        BASESYSTEM_T & getBasesystem_(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_CREATING_H
