﻿#ifndef FG_DEF_CORE_STATE_EVENTREGISTERMANAGER_H
#define FG_DEF_CORE_STATE_EVENTREGISTERMANAGER_H

struct FgStateEventRegisterManager;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/core/state/eventbackground.h"
#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class CreatingState;

    template< typename >
    class State;

    template< typename >
    class StateEventManager;

    template< typename EVENT_DATA_T >
    class StateEventRegisterManager : public UniqueWrapper< StateEventRegisterManager< EVENT_DATA_T >, FgStateEventRegisterManager >
    {
    public:
        template< typename STATE_DATA_T >
        static typename StateEventRegisterManager::Unique create(
            StateEventManager< EVENT_DATA_T > &
            , CreatingState< STATE_DATA_T > &
            , typename StateEvent< STATE_DATA_T, EVENT_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static typename StateEventRegisterManager::Unique create(
            StateEventManager< EVENT_DATA_T > &
            , State< STATE_DATA_T > &
            , typename StateEvent< STATE_DATA_T, EVENT_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static typename StateEventRegisterManager::Unique create(
            StateEventManager< EVENT_DATA_T > &
            , CreatingState< STATE_DATA_T > &
            , typename StateEventBackground< STATE_DATA_T, EVENT_DATA_T >::Proc
        );

        template< typename STATE_DATA_T >
        static typename StateEventRegisterManager::Unique create(
            StateEventManager< EVENT_DATA_T > &
            , State< STATE_DATA_T > &
            , typename StateEventBackground< STATE_DATA_T, EVENT_DATA_T >::Proc
        );

        static void destroy(
            StateEventRegisterManager< EVENT_DATA_T > *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_EVENTREGISTERMANAGER_H
