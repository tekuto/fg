﻿#ifndef FG_DEF_CORE_STATE_STATE_H
#define FG_DEF_CORE_STATE_STATE_H

struct FgState;

#ifdef  __cplusplus

#include "fg/def/core/state/thread.h"
#include "fg/def/core/state/threadsingleton.h"
#include "fg/def/core/state/threadbackground.h"
#include "fg/def/core/state/threadbackgroundsingleton.h"
#include "fg/def/core/state/creating.h"
#include "fg/def/common/unique.h"

namespace fg {
    class StateJoiner;

    template< typename STATE_DATA_T = void >
    class State : public UniqueWrapper< State< STATE_DATA_T >, FgState >
    {
        template< typename ARG_T >
        static ARG_T getArgType_( void ( * )( ARG_T * ) );

    public:
        STATE_DATA_T & getData(
        );

        void execute(
            typename StateThread< STATE_DATA_T >::Proc
        );

        void execute(
            typename StateThread< STATE_DATA_T >::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThread< STATE_DATA_T, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        void execute(
            typename StateThreadSingleton< STATE_DATA_T >::Proc
        );

        void execute(
            typename StateThreadSingleton< STATE_DATA_T >::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThreadSingleton< STATE_DATA_T, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        void execute(
            typename StateThreadBackground< STATE_DATA_T >::Proc
        );

        void execute(
            typename StateThreadBackground< STATE_DATA_T >::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThreadBackground< STATE_DATA_T, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        void execute(
            typename StateThreadBackgroundSingleton< STATE_DATA_T >::Proc
        );

        void execute(
            typename StateThreadBackgroundSingleton< STATE_DATA_T >::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThreadBackgroundSingleton< STATE_DATA_T, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        template< typename DATA_DESTROY_PROC_PTR_T >
        void enter(
            const typename CreatingState< decltype( getArgType_( static_cast< DATA_DESTROY_PROC_PTR_T >( nullptr ) ) ) >::DataCreateProc &
            , DATA_DESTROY_PROC_PTR_T
        );

        void enter(
            const typename CreatingState<>::InitializeProc &
        );

        void exit(
        );

        //TODO 子プロセス起動関数追加

        template< typename BASESYSTEM_T >
        BASESYSTEM_T & getBasesystem_(
        );
    };

    template<>
    class State<> : public UniqueWrapper< State<>, FgState >
    {
        template< typename ARG_T >
        static ARG_T getArgType_( void ( * )( ARG_T * ) );

    public:
        void execute(
            typename StateThread<>::Proc
        );

        void execute(
            typename StateThread<>::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThread< void, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        void execute(
            typename StateThreadSingleton<>::Proc
        );

        void execute(
            typename StateThreadSingleton<>::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThreadSingleton< void, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        void execute(
            typename StateThreadBackground<>::Proc
        );

        void execute(
            typename StateThreadBackground<>::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThreadBackground< void, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        void execute(
            typename StateThreadBackgroundSingleton<>::Proc
        );

        void execute(
            typename StateThreadBackgroundSingleton<>::Proc
            , StateJoiner &
        );

        template< typename USER_DATA_T >
        void execute(
            typename StateThreadBackgroundSingleton< void, USER_DATA_T >::Proc
            , StateJoiner &
            , USER_DATA_T &
        );

        template< typename DATA_DESTROY_PROC_PTR_T >
        void enter(
            const typename CreatingState< decltype( getArgType_( static_cast< DATA_DESTROY_PROC_PTR_T >( nullptr ) ) ) >::DataCreateProc &
            , DATA_DESTROY_PROC_PTR_T
        );

        void enter(
            const typename CreatingState<>::InitializeProc &
        );

        void exit(
        );

        //TODO 子プロセス起動関数追加

        template< typename BASESYSTEM_T >
        BASESYSTEM_T & getBasesystem_(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_STATE_H
