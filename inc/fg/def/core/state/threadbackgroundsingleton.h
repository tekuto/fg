﻿#ifndef FG_DEF_CORE_STATE_THREADBACKGROUNDSINGLETON_H
#define FG_DEF_CORE_STATE_THREADBACKGROUNDSINGLETON_H

struct FgStateThreadBackgroundSingleton;

typedef void ( * FgStateThreadBackgroundSingletonProc )(
    FgStateThreadBackgroundSingleton *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template<
        typename STATE_DATA_T = void
        , typename USER_DATA_T = void
    >
    class StateThreadBackgroundSingleton : public UniqueWrapper< StateThreadBackgroundSingleton< STATE_DATA_T, USER_DATA_T >, FgStateThreadBackgroundSingleton >
    {
    public:
        using Proc = void ( * )(
            StateThreadBackgroundSingleton &
        );

        State< STATE_DATA_T > & getState(
        );

        USER_DATA_T & getData(
        );
    };

    template< typename STATE_DATA_T >
    class StateThreadBackgroundSingleton< STATE_DATA_T, void > : public UniqueWrapper< StateThreadBackgroundSingleton< STATE_DATA_T, void >, FgStateThreadBackgroundSingleton >
    {
    public:
        using Proc = void ( * )(
            StateThreadBackgroundSingleton &
        );

        State< STATE_DATA_T > & getState(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_THREADBACKGROUNDSINGLETON_H
