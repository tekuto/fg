﻿#ifndef FG_DEF_CORE_STATE_JOINER_H
#define FG_DEF_CORE_STATE_JOINER_H

struct FgStateJoiner;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class StateJoiner : public UniqueWrapper< StateJoiner, FgStateJoiner >
    {
    public:
        static Unique create(
        );

        static void destroy(
            StateJoiner *
        );

        void join(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_JOINER_H
