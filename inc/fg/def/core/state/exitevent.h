﻿#ifndef FG_DEF_CORE_STATE_EXITEVENT_H
#define FG_DEF_CORE_STATE_EXITEVENT_H

struct FgStateExitEventData;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/common/unique.h"

namespace fg {
    class StateExitEventData : public UniqueWrapper< StateExitEventData, FgStateExitEventData >
    {
    };

    template< typename STATE_DATA_T = void >
    using StateExitEvent = StateEvent<
        STATE_DATA_T
        , StateExitEventData
    >;
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_EXITEVENT_H
