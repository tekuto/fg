﻿#ifndef FG_DEF_CORE_STATE_THREADSINGLETON_H
#define FG_DEF_CORE_STATE_THREADSINGLETON_H

struct FgStateThreadSingleton;

typedef void ( * FgStateThreadSingletonProc )(
    FgStateThreadSingleton *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template<
        typename STATE_DATA_T = void
        , typename USER_DATA_T = void
    >
    class StateThreadSingleton : public UniqueWrapper< StateThreadSingleton< STATE_DATA_T, USER_DATA_T >, FgStateThreadSingleton >
    {
    public:
        using Proc = void ( * )(
            StateThreadSingleton &
        );

        State< STATE_DATA_T > & getState(
        );

        USER_DATA_T & getData(
        );
    };

    template< typename STATE_DATA_T >
    class StateThreadSingleton< STATE_DATA_T, void > : public UniqueWrapper< StateThreadSingleton< STATE_DATA_T, void >, FgStateThreadSingleton >
    {
    public:
        using Proc = void ( * )(
            StateThreadSingleton &
        );

        State< STATE_DATA_T > & getState(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_THREADSINGLETON_H
