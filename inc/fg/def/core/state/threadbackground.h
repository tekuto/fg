﻿#ifndef FG_DEF_CORE_STATE_THREADBACKGROUND_H
#define FG_DEF_CORE_STATE_THREADBACKGROUND_H

struct FgStateThreadBackground;

typedef void ( * FgStateThreadBackgroundProc )(
    FgStateThreadBackground *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template<
        typename STATE_DATA_T = void
        , typename USER_DATA_T = void
    >
    class StateThreadBackground : public UniqueWrapper< StateThreadBackground< STATE_DATA_T, USER_DATA_T >, FgStateThreadBackground >
    {
    public:
        using Proc = void ( * )(
            StateThreadBackground &
        );

        State< STATE_DATA_T > & getState(
        );

        USER_DATA_T & getData(
        );
    };

    template< typename STATE_DATA_T >
    class StateThreadBackground< STATE_DATA_T, void > : public UniqueWrapper< StateThreadBackground< STATE_DATA_T, void >, FgStateThreadBackground >
    {
    public:
        using Proc = void ( * )(
            StateThreadBackground &
        );

        State< STATE_DATA_T > & getState(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_THREADBACKGROUND_H
