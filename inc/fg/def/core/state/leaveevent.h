﻿#ifndef FG_DEF_CORE_STATE_LEAVEEVENT_H
#define FG_DEF_CORE_STATE_LEAVEEVENT_H

struct FgStateLeaveEventData;

#ifdef  __cplusplus

#include "fg/def/core/state/event.h"
#include "fg/def/common/unique.h"

namespace fg {
    class StateLeaveEventData : public UniqueWrapper< StateLeaveEventData, FgStateLeaveEventData >
    {
    };

    template< typename STATE_DATA_T = void >
    using StateLeaveEvent = StateEvent<
        STATE_DATA_T
        , StateLeaveEventData
    >;
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_STATE_LEAVEEVENT_H
