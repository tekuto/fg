﻿#ifndef FG_DEF_CORE_SAVE_REMOVENOTIFY_H
#define FG_DEF_CORE_SAVE_REMOVENOTIFY_H

struct FgSaveRemoveNotify;

typedef void ( * FgSaveRemoveNotifyProc )(
    FgSaveRemoveNotify *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template< typename STATE_DATA_T = void >
    class SaveRemoveNotify : public UniqueWrapper< SaveRemoveNotify< STATE_DATA_T >, FgSaveRemoveNotify >
    {
    public:
        using Proc = void ( * )(
            SaveRemoveNotify< STATE_DATA_T > &
        );

        State< STATE_DATA_T > & getState(
        );

        bool removed(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_REMOVENOTIFY_H
