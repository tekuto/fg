﻿#ifndef FG_DEF_CORE_SAVE_READER_H
#define FG_DEF_CORE_SAVE_READER_H

struct FgSaveReader;

#ifdef  __cplusplus

#include "fg/def/core/save/save.h"
#include "fg/def/core/save/readnotify.h"
#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    class ModuleContext;

    template< typename >
    class BasesystemContext;

    template< typename >
    class GameContext;

    class SaveReader : public UniqueWrapper< SaveReader, FgSaveReader >
    {
    public:
        static Save::ConstUnique read(
            const ModuleContext &
            , const char *
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , const ModuleContext &
            , const char *
            , typename SaveReadNotify< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename BASESYSTEM_CONTEXT_STATE_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , const BasesystemContext< BASESYSTEM_CONTEXT_STATE_DATA_T > &
            , const char *
            , typename SaveReadNotify< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename GAME_CONTEXT_STATE_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , const GameContext< GAME_CONTEXT_STATE_DATA_T > &
            , const char *
            , typename SaveReadNotify< STATE_DATA_T >::Proc
        );

        static void destroy(
            SaveReader *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_READER_H
