﻿#ifndef FG_DEF_CORE_SAVE_WRITENOTIFY_H
#define FG_DEF_CORE_SAVE_WRITENOTIFY_H

struct FgSaveWriteNotify;

typedef void ( * FgSaveWriteNotifyProc )(
    FgSaveWriteNotify *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    template< typename STATE_DATA_T = void >
    class SaveWriteNotify : public UniqueWrapper< SaveWriteNotify< STATE_DATA_T >, FgSaveWriteNotify >
    {
    public:
        using Proc = void ( * )(
            SaveWriteNotify< STATE_DATA_T > &
        );

        State< STATE_DATA_T > & getState(
        );

        bool wrote(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_WRITENOTIFY_H
