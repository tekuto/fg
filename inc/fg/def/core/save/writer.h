﻿#ifndef FG_DEF_CORE_SAVE_WRITER_H
#define FG_DEF_CORE_SAVE_WRITER_H

struct FgSaveWriter;

#ifdef  __cplusplus

#include "fg/def/core/save/writenotify.h"
#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    class ModuleContext;

    template< typename >
    class BasesystemContext;

    template< typename >
    class GameContext;

    class Save;

    class SaveWriter : public UniqueWrapper< SaveWriter, FgSaveWriter >
    {
    public:
        static bool write(
            const ModuleContext &
            , const char *
            , const Save &
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , const ModuleContext &
            , const char *
            , const Save &
            , typename SaveWriteNotify< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename BASESYSTEM_CONTEXT_STATE_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , const BasesystemContext< BASESYSTEM_CONTEXT_STATE_DATA_T > &
            , const char *
            , const Save &
            , typename SaveWriteNotify< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename GAME_CONTEXT_STATE_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , const GameContext< GAME_CONTEXT_STATE_DATA_T > &
            , const char *
            , const Save &
            , typename SaveWriteNotify< STATE_DATA_T >::Proc
        );

        static void destroy(
            SaveWriter *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_WRITER_H
