﻿#ifndef FG_DEF_CORE_SAVE_SAVE_H
#define FG_DEF_CORE_SAVE_SAVE_H

struct FgSave;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class DataElementString;
    class DataElementList;
    class DataElementMap;
    class DataElement;

    class Save : public UniqueWrapper< Save, FgSave >
    {
    public:
        static ConstUnique create(
            const DataElementString &
        );

        static ConstUnique create(
            const DataElementList &
        );

        static ConstUnique create(
            const DataElementMap &
        );

        static void destroy(
            Save *
        );

        ConstUnique clone(
        ) const;

        const DataElement & getDataElement(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_SAVE_H
