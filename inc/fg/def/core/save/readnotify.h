﻿#ifndef FG_DEF_CORE_SAVE_READNOTIFY_H
#define FG_DEF_CORE_SAVE_READNOTIFY_H

struct FgSaveReadNotify;

typedef void ( * FgSaveReadNotifyProc )(
    FgSaveReadNotify *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    class Save;

    template< typename STATE_DATA_T = void >
    class SaveReadNotify : public UniqueWrapper< SaveReadNotify< STATE_DATA_T >, FgSaveReadNotify >
    {
    public:
        using Proc = void ( * )(
            SaveReadNotify< STATE_DATA_T > &
        );

        State< STATE_DATA_T > & getState(
        );

        const Save * getSave(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_READNOTIFY_H
