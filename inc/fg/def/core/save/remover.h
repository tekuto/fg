﻿#ifndef FG_DEF_CORE_SAVE_REMOVER_H
#define FG_DEF_CORE_SAVE_REMOVER_H

struct FgSaveRemover;

#ifdef  __cplusplus

#include "fg/def/core/save/removenotify.h"
#include "fg/def/common/unique.h"

namespace fg {
    template< typename >
    class State;

    class ModuleContext;

    template< typename >
    class BasesystemContext;

    template< typename >
    class GameContext;

    class SaveRemover : public UniqueWrapper< SaveRemover, FgSaveRemover >
    {
    public:
        static bool remove(
            const ModuleContext &
            , const char *
        );

        template< typename STATE_DATA_T >
        static Unique create(
            State< STATE_DATA_T > &
            , const ModuleContext &
            , const char *
            , typename SaveRemoveNotify< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename BASESYSTEM_CONTEXT_STATE_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , const BasesystemContext< BASESYSTEM_CONTEXT_STATE_DATA_T > &
            , const char *
            , typename SaveRemoveNotify< STATE_DATA_T >::Proc
        );

        template<
            typename STATE_DATA_T
            , typename GAME_CONTEXT_STATE_DATA_T
        >
        static Unique create(
            State< STATE_DATA_T > &
            , const GameContext< GAME_CONTEXT_STATE_DATA_T > &
            , const char *
            , typename SaveRemoveNotify< STATE_DATA_T >::Proc
        );

        static void destroy(
            SaveRemover *
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_SAVE_REMOVER_H
