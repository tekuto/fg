﻿#ifndef FG_DEF_CORE_MODULE_CONTEXT_H
#define FG_DEF_CORE_MODULE_CONTEXT_H

struct FgModuleContext;

typedef void ( * FgModuleContextInitializeProc )(
    FgModuleContext *
);

typedef void ( * FgModuleContextDataDestroyProc )(
    void *
);

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class ModuleContext : public UniqueWrapper< ModuleContext, FgModuleContext >
    {
    public:
        template< typename DATA_T >
        using DataDestroyProc = void ( * )(
            DATA_T *
        );

        template< typename DATA_T >
        DATA_T & getData(
        );

        template< typename DATA_T >
        void setData(
            DATA_T *
            , DataDestroyProc< DATA_T >
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_MODULE_CONTEXT_H
