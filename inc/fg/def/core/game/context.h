﻿#ifndef FG_DEF_CORE_GAME_CONTEXT_H
#define FG_DEF_CORE_GAME_CONTEXT_H

struct FgGameContext;

typedef void ( * FgGameContextInitializeProc )(
    FgGameContext *
);

typedef void ( * FgGameContextStateDataDestroyProc )(
    void *
);

#ifdef  __cplusplus

#include "fg/def/core/state/creating.h"
#include "fg/def/common/unique.h"

namespace fg {
    template< typename STATE_DATA_T = void >
    class GameContext : public UniqueWrapper< GameContext< STATE_DATA_T >, FgGameContext >
    {
    public:
        using StateDataDestroyProc = void ( * )(
            STATE_DATA_T *
        );

        void setStateData(
            STATE_DATA_T *
            , StateDataDestroyProc
        );

        CreatingState< STATE_DATA_T > & getCreatingState(
        );
    };

    template<>
    class GameContext<> : public UniqueWrapper< GameContext<>, FgGameContext >
    {
    public:
        CreatingState<> & getCreatingState(
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_GAME_CONTEXT_H
