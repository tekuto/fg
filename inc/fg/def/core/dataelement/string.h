﻿#ifndef FG_DEF_CORE_DATAELEMENT_STRING_H
#define FG_DEF_CORE_DATAELEMENT_STRING_H

struct FgDataElementString;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class DataElementString : public UniqueWrapper< DataElementString, FgDataElementString >
    {
    public:
        static ConstUnique create(
            const char *
        );

        static void destroy(
            DataElementString *
        );

        const char * getString(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_DATAELEMENT_STRING_H
