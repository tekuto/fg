﻿#ifndef FG_DEF_CORE_DATAELEMENT_DATAELEMENT_H
#define FG_DEF_CORE_DATAELEMENT_DATAELEMENT_H

struct FgDataElement;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class DataElementString;
    class DataElementList;
    class DataElementMap;

    class DataElement : public UniqueWrapper< DataElement, FgDataElement >
    {
    public:
        const DataElementString * getDataElementString(
        ) const;

        const DataElementList * getDataElementList(
        ) const;

        const DataElementMap * getDataElementMap(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_DATAELEMENT_DATAELEMENT_H
