﻿#ifndef FG_DEF_CORE_DATAELEMENT_LIST_H
#define FG_DEF_CORE_DATAELEMENT_LIST_H

struct FgDataElementList;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class DataElement;
    class DataElementString;
    class DataElementMap;

    class DataElementList : public UniqueWrapper< DataElementList, FgDataElementList >
    {
    public:
        static Unique create(
        );

        static void destroy(
            DataElementList *
        );

        std::size_t getSize(
        ) const;

        const DataElement & getDataElement(
            std::size_t
        ) const;

        const DataElementString * getDataElementString(
            std::size_t
        ) const;

        const DataElementList * getDataElementList(
            std::size_t
        ) const;

        const DataElementMap * getDataElementMap(
            std::size_t
        ) const;

        void addDataElement(
            const DataElementString &
        );

        void addDataElement(
            fg::ConstUnique< DataElementString > &&
        );

        void addDataElement(
            const DataElementList & _LIST
        );

        void addDataElement(
            fg::ConstUnique< DataElementList > &&
        );

        void addDataElement(
            const DataElementMap &
        );

        void addDataElement(
            fg::ConstUnique< DataElementMap > &&
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_DATAELEMENT_LIST_H
