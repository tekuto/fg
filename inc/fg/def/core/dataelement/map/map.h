﻿#ifndef FG_DEF_CORE_DATAELEMENT_MAP_MAP_H
#define FG_DEF_CORE_DATAELEMENT_MAP_MAP_H

struct FgDataElementMap;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class DataElement;
    class DataElementString;
    class DataElementList;

    class DataElementMap : public UniqueWrapper< DataElementMap, FgDataElementMap >
    {
    public:
        static Unique create(
        );

        static void destroy(
            DataElementMap *
        );

        const DataElement * getDataElement(
            const char *
        ) const;

        const DataElementString * getDataElementString(
            const char *
        ) const;

        const DataElementList * getDataElementList(
            const char *
        ) const;

        const DataElementMap * getDataElementMap(
            const char *
        ) const;

        void addDataElement(
            const char *
            , const DataElementString &
        );

        void addDataElement(
            const char *
            , fg::ConstUnique< DataElementString > &&
        );

        void addDataElement(
            const char *
            , const DataElementList &
        );

        void addDataElement(
            const char *
            , fg::ConstUnique< DataElementList > &&
        );

        void addDataElement(
            const char *
            , const DataElementMap &
        );

        void addDataElement(
            const char *
            , fg::ConstUnique< DataElementMap > &&
        );
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_DATAELEMENT_MAP_MAP_H
