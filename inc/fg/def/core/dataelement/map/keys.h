﻿#ifndef FG_DEF_CORE_DATAELEMENT_MAP_KEYS_H
#define FG_DEF_CORE_DATAELEMENT_MAP_KEYS_H

struct FgDataElementMapKeys;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class DataElementMap;

    class DataElementMapKeys : public UniqueWrapper< DataElementMapKeys, FgDataElementMapKeys >
    {
    public:
        static ConstUnique create(
            const DataElementMap &
        );

        static void destroy(
            DataElementMapKeys *
        );

        std::size_t getSize(
        ) const;

        const char * getKey(
            std::size_t
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CORE_DATAELEMENT_MAP_KEYS_H
