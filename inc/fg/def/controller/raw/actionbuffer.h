﻿#ifndef FG_DEF_CONTROLLER_RAW_ACTIONBUFFER_H
#define FG_DEF_CONTROLLER_RAW_ACTIONBUFFER_H

struct FgRawControllerActionBuffer;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class RawControllerAction;

    class RawControllerActionBuffer : public UniqueWrapper< RawControllerActionBuffer, FgRawControllerActionBuffer >
    {
    public:
        std::size_t getSize(
        ) const;

        const RawControllerAction & getAction(
            std::size_t
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_ACTIONBUFFER_H
