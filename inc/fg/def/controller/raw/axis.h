﻿#ifndef FG_DEF_CONTROLLER_RAW_AXIS_H
#define FG_DEF_CONTROLLER_RAW_AXIS_H

struct FgRawControllerAxis;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class RawControllerID;

    class RawControllerAxis : public UniqueWrapper< RawControllerAxis, FgRawControllerAxis >
    {
    public:
        static ConstUnique create(
            const RawControllerID &
            , std::size_t
        );

        static void destroy(
            RawControllerAxis *
        );

        ConstUnique clone(
        ) const;

        const RawControllerID & getID(
        ) const;

        std::size_t getIndex(
        ) const;

        bool operator==(
            const RawControllerAxis &
        ) const;

        bool operator<(
            const RawControllerAxis &
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_AXIS_H
