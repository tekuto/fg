﻿#ifndef FG_DEF_CONTROLLER_RAW_BUTTONACTION_H
#define FG_DEF_CONTROLLER_RAW_BUTTONACTION_H

struct FgRawControllerButtonAction;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class RawControllerButton;

    class RawControllerButtonAction : public UniqueWrapper< RawControllerButtonAction, FgRawControllerButtonAction >
    {
    public:
        const RawControllerButton & getButton(
        ) const;

        bool isPressedAction(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_BUTTONACTION_H
