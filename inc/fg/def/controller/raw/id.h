﻿#ifndef FG_DEF_CONTROLLER_RAW_ID_H
#define FG_DEF_CONTROLLER_RAW_ID_H

struct FgRawControllerID;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class RawControllerID : public UniqueWrapper< RawControllerID, FgRawControllerID >
    {
    public:
        static ConstUnique create(
            const char *
            , const char *
            , const char *
        );

        static void destroy(
            RawControllerID *
        );

        ConstUnique clone(
        ) const;

        const char * getModule(
        ) const;

        const char * getPort(
        ) const;

        const char * getDevice(
        ) const;

        bool operator==(
            const RawControllerID &
        ) const;

        bool operator<(
            const RawControllerID &
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_ID_H
