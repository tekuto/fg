﻿#ifndef FG_DEF_CONTROLLER_RAW_AXISACTION_H
#define FG_DEF_CONTROLLER_RAW_AXISACTION_H

struct FgRawControllerAxisAction;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class RawControllerAxis;

    class RawControllerAxisAction : public UniqueWrapper< RawControllerAxisAction, FgRawControllerAxisAction >
    {
    public:
        const RawControllerAxis & getAxis(
        ) const;

        int getValue(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_AXISACTION_H
