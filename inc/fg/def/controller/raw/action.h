﻿#ifndef FG_DEF_CONTROLLER_RAW_ACTION_H
#define FG_DEF_CONTROLLER_RAW_ACTION_H

struct FgRawControllerAction;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class RawControllerConnectAction;
    class RawControllerButtonAction;
    class RawControllerAxisAction;

    class RawControllerAction : public UniqueWrapper< RawControllerAction, FgRawControllerAction >
    {
    public:
        const RawControllerConnectAction * getConnectAction(
        ) const;

        const RawControllerButtonAction * getButtonAction(
        ) const;

        const RawControllerAxisAction * getAxisAction(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_ACTION_H
