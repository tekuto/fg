﻿#ifndef FG_DEF_CONTROLLER_RAW_BUTTON_H
#define FG_DEF_CONTROLLER_RAW_BUTTON_H

struct FgRawControllerButton;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

#include <cstddef>

namespace fg {
    class RawControllerID;

    class RawControllerButton : public UniqueWrapper< RawControllerButton, FgRawControllerButton >
    {
    public:
        static ConstUnique create(
            const RawControllerID &
            , std::size_t
        );

        static void destroy(
            RawControllerButton *
        );

        ConstUnique clone(
        ) const;

        const RawControllerID & getID(
        ) const;

        std::size_t getIndex(
        ) const;

        bool operator==(
            const RawControllerButton &
        ) const;

        bool operator<(
            const RawControllerButton &
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_BUTTON_H
