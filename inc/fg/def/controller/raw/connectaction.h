﻿#ifndef FG_DEF_CONTROLLER_RAW_CONNECTACTION_H
#define FG_DEF_CONTROLLER_RAW_CONNECTACTION_H

struct FgRawControllerConnectAction;

#ifdef  __cplusplus

#include "fg/def/common/unique.h"

namespace fg {
    class RawControllerID;

    class RawControllerConnectAction : public UniqueWrapper< RawControllerConnectAction, FgRawControllerConnectAction >
    {
    public:
        const RawControllerID & getID(
        ) const;

        bool isConnectedAction(
        ) const;
    };
}

#endif  // __cplusplus

#endif  // FG_DEF_CONTROLLER_RAW_CONNECTACTION_H
