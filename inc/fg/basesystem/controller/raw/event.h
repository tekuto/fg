﻿#ifndef FG_BASESYSTEM_CONTROLLER_RAW_EVENT_H
#define FG_BASESYSTEM_CONTROLLER_RAW_EVENT_H

#include "fg/def/basesystem/controller/raw/event.h"
#include "fg/def/controller/raw/actionbuffer.h"
#include "fg/def/core/state/creating.h"
#include "fg/def/core/state/state.h"
#include "fg/def/core/state/event.h"
#include "fg/def/core/state/eventbackground.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        const FgRawControllerActionBuffer
        , fgBasesystemRawControllerEventDataGetActionBuffer(
            const FgRawControllerEventData *
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerEventRegisterManager
        , fgBasesystemRawControllerEventRegisterManagerCreateForCreatingState(
            FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerEventRegisterManager
        , fgBasesystemRawControllerEventRegisterManagerCreateForState(
            FgState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerEventRegisterManager
        , fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForCreatingState(
            FgCreatingState *
            , FgStateEventBackgroundProc
        )
    )

    FG_FUNCTION_PTR(
        FgRawControllerEventRegisterManager
        , fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForState(
            FgState *
            , FgStateEventBackgroundProc
        )
    )

    FG_FUNCTION_VOID(
        fgBasesystemRawControllerEventRegisterManagerDestroy(
            FgRawControllerEventRegisterManager *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline const RawControllerActionBuffer & RawControllerEventData::getActionBuffer(
    ) const
    {
        return reinterpret_cast< const RawControllerActionBuffer & >( *fgBasesystemRawControllerEventDataGetActionBuffer( &**this ) );
    }

    template< typename STATE_DATA_T >
    RawControllerEventRegisterManager::Unique RawControllerEventRegisterManager::create(
        CreatingState< STATE_DATA_T > &                     _state
        , typename RawControllerEvent< STATE_DATA_T >::Proc _procPtr
    )
    {
        return fgBasesystemRawControllerEventRegisterManagerCreateForCreatingState(
            &*_state
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    RawControllerEventRegisterManager::Unique RawControllerEventRegisterManager::create(
        State< STATE_DATA_T > &                             _state
        , typename RawControllerEvent< STATE_DATA_T >::Proc _procPtr
    )
    {
        return fgBasesystemRawControllerEventRegisterManagerCreateForState(
            &*_state
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    RawControllerEventRegisterManager::Unique RawControllerEventRegisterManager::create(
        CreatingState< STATE_DATA_T > &                                 _state
        , typename RawControllerEventBackground< STATE_DATA_T >::Proc   _procPtr
    )
    {
        return fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForCreatingState(
            &*_state
            , reinterpret_cast< FgStateEventBackgroundProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    RawControllerEventRegisterManager::Unique RawControllerEventRegisterManager::create(
        State< STATE_DATA_T > &                                         _state
        , typename RawControllerEventBackground< STATE_DATA_T >::Proc   _procPtr
    )
    {
        return fgBasesystemRawControllerEventRegisterManagerCreateBackgroundForState(
            &*_state
            , reinterpret_cast< FgStateEventBackgroundProc >( _procPtr )
        );
    }

    inline void RawControllerEventRegisterManager::destroy(
        RawControllerEventRegisterManager * _this
    )
    {
        fgBasesystemRawControllerEventRegisterManagerDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_BASESYSTEM_CONTROLLER_RAW_EVENT_H
