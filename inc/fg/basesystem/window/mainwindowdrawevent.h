﻿#ifndef FG_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H
#define FG_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H

#include "fg/core/state/event.h"
#include "fg/def/basesystem/window/mainwindowdrawevent.h"
#include "fg/def/window/window.h"
#include "fg/def/core/state/creating.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgWindow
        , fgBasesystemMainWindowDrawEventDataGetWindow(
            FgMainWindowDrawEventData *
        )
    )

    FG_FUNCTION_PTR(
        FgMainWindowDrawEventRegisterManager
        , fgBasesystemMainWindowDrawEventRegisterManagerCreateForCreatingState(
            FgCreatingState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_PTR(
        FgMainWindowDrawEventRegisterManager
        , fgBasesystemMainWindowDrawEventRegisterManagerCreateForState(
            FgState *
            , FgStateEventProc
        )
    )

    FG_FUNCTION_VOID(
        fgBasesystemMainWindowDrawEventRegisterManagerDestroy(
            FgMainWindowDrawEventRegisterManager *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    inline Window & MainWindowDrawEventData::getWindow(
    )
    {
        return reinterpret_cast< Window & >( *fgBasesystemMainWindowDrawEventDataGetWindow( &**this ) );
    }

    template< typename STATE_DATA_T >
    MainWindowDrawEventRegisterManager::Unique MainWindowDrawEventRegisterManager::create(
        CreatingState< STATE_DATA_T > &                         _state
        , typename MainWindowDrawEvent< STATE_DATA_T >::Proc    _procPtr
    )
    {
        return fgBasesystemMainWindowDrawEventRegisterManagerCreateForCreatingState(
            &*_state
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    template< typename STATE_DATA_T >
    MainWindowDrawEventRegisterManager::Unique MainWindowDrawEventRegisterManager::create(
        State< STATE_DATA_T > &                                 _state
        , typename MainWindowDrawEvent< STATE_DATA_T >::Proc    _procPtr
    )
    {
        return fgBasesystemMainWindowDrawEventRegisterManagerCreateForState(
            &*_state
            , reinterpret_cast< FgStateEventProc >( _procPtr )
        );
    }

    inline void MainWindowDrawEventRegisterManager::destroy(
        MainWindowDrawEventRegisterManager *    _this
    )
    {
        fgBasesystemMainWindowDrawEventRegisterManagerDestroy( &**_this );
    }
}

#endif  // __cplusplus

#endif  // FG_BASESYSTEM_WINDOW_MAINWINDOWDRAWEVENT_H
