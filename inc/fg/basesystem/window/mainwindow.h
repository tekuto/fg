﻿#ifndef FG_BASESYSTEM_WINDOW_MAINWINDOW_H
#define FG_BASESYSTEM_WINDOW_MAINWINDOW_H

#include "fg/def/basesystem/window/mainwindow.h"
#include "fg/def/window/window.h"
#include "fg/def/core/state/creating.h"
#include "fg/def/core/state/state.h"
#include "fg/util/import.h"

#ifdef  __cplusplus
extern "C" {
#endif  // __cplusplus

    FG_FUNCTION_PTR(
        FgWindow
        , fgBasesystemGetMainWindowForCreatingState(
            FgCreatingState *
        )
    )

    FG_FUNCTION_PTR(
        FgWindow
        , fgBasesystemGetMainWindowForState(
            FgState *
        )
    )

#ifdef  __cplusplus
}
#endif  // __cplusplus

#ifdef  __cplusplus

namespace fg {
    template< typename STATE_DATA_T >
    Window & getMainWindow(
        CreatingState< STATE_DATA_T > & _state
    )
    {
        return reinterpret_cast< Window & >( *fgBasesystemGetMainWindowForCreatingState( &*_state ) );
    }

    template< typename STATE_DATA_T >
    Window & getMainWindow(
        State< STATE_DATA_T > & _state
    )
    {
        return reinterpret_cast< Window & >( *fgBasesystemGetMainWindowForState( &*_state ) );
    }
}

#endif  // __cplusplus

#endif  // FG_BASESYSTEM_WINDOW_MAINWINDOW_H
